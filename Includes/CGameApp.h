//-----------------------------------------------------------------------------
// File: CGameApp.h
//
// Desc: Game Application class, this is the central hub for all app processing
//
// Original design by Adam Hoult & Gary Simmons. Modified by Mihai Popescu.
//-----------------------------------------------------------------------------

#ifndef _CGAMEAPP_H_
#define _CGAMEAPP_H_

//-----------------------------------------------------------------------------
// CGameApp Specific Includes
//-----------------------------------------------------------------------------
#include "Main.h"
#include "CTimer.h"
#include "CPlayer.h"
#include "BackBuffer.h"
#include "ImageFile.h"
#include "Enemy.h"

#include <iostream>
#include <string>
#include <fstream>
#include "Player.h"
#include "Human.h"

#include "MiniMap.h"
#include "HUD.h"
#include "GroupOfHumans.h"
#include "GroupOfCars.h"
#include "Builders.h"
#include "CollisionManagerTest.h"
#include "GroupOfPlayers.h"

#define IDCONTINUEGAME 1

class GroupOfPlayers;

//-----------------------------------------------------------------------------
// Forward Declarations
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Main Class Declarations
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// Name : CGameApp (Class)
// Desc : Central game engine, initialises the game and handles core processes.
//-----------------------------------------------------------------------------
class CGameApp
{
public:
	//-------------------------------------------------------------------------
	// Constructors & Destructors for This Class.
	//-------------------------------------------------------------------------
			 CGameApp();
	virtual ~CGameApp();

	int getWeaponIDFromShopMenu();
	//-------------------------------------------------------------------------
	// Public Functions for This Class
	//-------------------------------------------------------------------------
	LRESULT		DisplayWndProc( HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam );
	bool		InitInstance( LPCTSTR lpCmdLine, int iCmdShow );
	int			BeginGame( );
	bool		ShutDown( );
	int			RestartGame(LPCTSTR lpCmdLine, int iCmdShow);
	void		Save();
	void		Load();


private:
	//-------------------------------------------------------------------------
	// Private Functions for This Class
	//-------------------------------------------------------------------------
	bool		BuildObjects	  ( );
	void		ReleaseObjects	( );
	int			FrameAdvance();
	bool		CreateDisplay	 ( );
	void		SetupGameState	( );
	void		AnimateObjects	( );
	void		CheckCollisions();
	void		DrawObjects	   ( );
	void		ProcessInput	  ( );
	
	string		save_file = "data/saveFile.txt";
	CollisionManagerTest* collisionManager;
	GroupOfPlayers* players;
	int MAX_WEAPONS = 4;
	GroupOfPoliceCars* policeCars;
	INature* nature;


	//CPlayer* m_pOponentPlayer;

	//-------------------------------------------------------------------------
	// Private Static Functions For This Class
	//-------------------------------------------------------------------------
	static LRESULT CALLBACK StaticWndProc(HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam);

	//-------------------------------------------------------------------------
	// Private Variables For This Class
	//-------------------------------------------------------------------------
	CTimer				  m_Timer;			// Game timer
	ULONG				   m_LastFrameRate;	// Used for making sure we update only when fps changes.
	
	HWND					m_hWnd;			 // Main window HWND
	HICON				   m_hIcon;			// Window Icon
	HMENU				   m_hMenu;			// Window Menu
	
	bool					m_bActive;		  // Is the application active ?

	ULONG				   m_nViewX;		   // X Position of render viewport
	ULONG				   m_nViewY;		   // Y Position of render viewport
	ULONG				   m_nViewWidth;	   // Width of render viewport
	ULONG				   m_nViewHeight;	  // Height of render viewport

	POINT				   m_OldCursorPos;	 // Old cursor position for tracking
	HINSTANCE				m_hInstance;

	CImageFile				*m_imgBackground;

	BackBuffer*				m_pBBuffer;
	Player*					player;
	MiniMap*				miniMap;
	HUD*					hud;

	GroupOfHumans*			humans;
	GroupOfCars*			cars;

	Builders*				builder;
	PoliceCar*				policeCar;

	ATOM					atom = 0;

	string					PLAYER_MINI_MAP_FILE_NAME = "Textures\\MiniMap\\Indicator.bmp";
	string					MINI_MAP_FILE_NAME = "Textures\\MiniMap\\MiniMap.bmp";
	string					BACKGROUND_FILE = "Textures\\Background\\bgr.bmp";
};

#endif // _CGAMEAPP_H_