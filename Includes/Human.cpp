﻿#include "Human.h"
#include "IHumanState.h"

Human::Human(const BackBuffer* pBackBuffer, const char* fileName, CImageFile* imgBackground) : ICollidable(){
	this->imgBackground = imgBackground;
	backBuffer = pBackBuffer;

	RECT r;
	r.left = 0;
	r.top = 0;
	r.right = 51;
	r.bottom = 50;

	m_pSprite = new AnimatedSprite(fileName, BACKGROUND_TRANSPARENT_COLOR, r, 1, 1);
	m_pSprite->setBackBuffer(pBackBuffer);
	m_pSprite->setBackgroundImage(imgBackground);
	m_eSpeedState = SPEED_STOP;
	m_fTimer = 0;
	m_pSprite->UpdatePixelBuffer();

	center.x = m_pSprite->width() / 2;
	center.y = m_pSprite->height() / 2;
}

Human::Human(AnimatedSprite* m_pSprite, IHumanState* humanState, INature* nature,const BackBuffer* backBuffer, CImageFile* imgBackground) {
	this->m_pSprite = m_pSprite;
	this->humanState = humanState;
	this->backBuffer = backBuffer;
	this->imgBackground = imgBackground;
	this->nature = nature;
}

Human::~Human() {
	delete humanState;
	//delete m_pSprite;
}

void Human::Update(float dt) {
	AnimateSprite();
	humanState->update(dt);
	m_pSprite->update(dt);

	// Get velocity
	double v = m_pSprite->mVelocity.Magnitude();

	// NOTE: for each async sound played Windows creates a thread for you
	// but only one, so you cannot play multiple sounds at once.
	// This creation/destruction of threads also leads to bad performance
	// so this method is not recommanded to be used in complex projects.

	// update internal time counter used in sound handling (not to overlap sounds)
	m_fTimer += dt;
}

void Human::Draw() {
	humanState->draw();
	//m_pSprite->drawByAbsolutePosition();
}

void Human::Move() {
	frameCounter++;
}

Vec2& Human::Position() {
	return m_pSprite->mPosition;
}

Vec2& Human::Velocity() {
	return m_pSprite->mVelocity;
}

Vec2& Human::absoluteCenterPosition() {
	return m_pSprite->mAbsoluteCenterPosition;
}

void Human::setAbsolutePosition(Vec2 position) {
	m_pSprite->mAbsoluteCenterPosition = position;
	humanState->setSprite(m_pSprite);
	m_pSprite->updateCollisionPointsRelative();
}

bool Human::isToDelete() {
	return humanState->isToDelete();
}

void Human::AnimateSprite() {
	int frameIndexToSet = frameCounter / CHANGING_FRAME_TIME_INTERVAL + 1;
	if (frameIndexToSet >= m_pSprite->GetFrameCount())
		frameCounter = 0;
	m_pSprite->SetFrame(frameIndexToSet);
}

bool Human::isCollision(ICollidable* other) {
	return other->isCollision(m_pSprite);
}

bool Human::isCollision(Sprite* otherSprite) {
	/*isHit();*/
	return m_pSprite->isCollisionWith(otherSprite);
}

void Human::isHit(ICollidable* other) {
	humanState = humanState->goNextState();
	humanState->setAbsolutePosition(absoluteCenterPosition());
	humanState->isHit(other);
}

void Human::isInDangerZone() {
	goLastPostion();
}

void Human::goLastPostion() {
	
}

bool Human::isCollisionWithNature() {
	if (nature->isBlockedZone(m_pSprite) || nature->isShopZone(m_pSprite)) {
		goLastPostion();
		return true;
	}
	if (nature->isDangerZone(m_pSprite)) {
		isInDangerZone();
		return true;
	}

	return false;
}

string Human::toString() {
	return m_pSprite->toString();
}


void Human::rotate(int rotationAngle) {
	m_pSprite->Rotate(rotationAngle);
}

void Human::isHit() {
	humanState = humanState->goNextState();
	humanState->setAbsolutePosition(absoluteCenterPosition());
}

