﻿#include "AliveHumanState.h"

AliveHumanState::~AliveHumanState() {
	delete m_pSprite;
}

void AliveHumanState::setNext(IHumanState* nextState) {
	this->nextState = nextState;
}

void AliveHumanState::draw() {
	m_pSprite->drawByAbsolutePosition();
}

IHumanState* AliveHumanState::goNextState() {
	return nextState;
}

void AliveHumanState::setAbsolutePosition(const Vec2& absolutePosition) {
	m_pSprite->mAbsoluteCenterPosition = absolutePosition;
}

void AliveHumanState::update(float dt) {
	m_pSprite->update(dt);
}

void AliveHumanState::setSprite(AnimatedSprite* sprite) {
	m_pSprite = sprite;
}

bool AliveHumanState::isToDelete() {
	return m_pSprite->isToDelete();
}

void AliveHumanState::isHit(ICollidable* other) {
}
