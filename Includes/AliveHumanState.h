﻿#pragma once
#include "IHumanState.h"
#include "Sprite.h"

class AliveHumanState : public IHumanState{
public:
	AliveHumanState(AnimatedSprite* sprite) : m_pSprite(sprite) {};
	virtual ~AliveHumanState();
	void setNext(IHumanState* nextState) override;
	void draw() override;
	IHumanState* goNextState() override;
	void setAbsolutePosition(const Vec2& absolutePosition) override;
	void update(float dt) override;

	void setSprite(AnimatedSprite* sprite) override;
	bool isToDelete() override;
	void isHit(ICollidable* other) override;
private:
	IHumanState* nextState;
	AnimatedSprite* m_pSprite;
};
