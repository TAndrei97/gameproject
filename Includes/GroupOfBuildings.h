﻿#pragma once
#include <vector>
#include "Main.h"
#include "Sprite.h"
#include <iostream>
#include <vector>
#include "ImageFile.h"
#include <fstream>
#include "Building.h"

using namespace std;
class GroupOfBuildings {
public:
	GroupOfBuildings(CImageFile* img_background, const BackBuffer* back_buffer)
		: imgBackground(img_background),
		  backBuffer(back_buffer) {
	}
	virtual ~GroupOfBuildings();

	void Draw();
	void LoadFromFile(const char* fileName);
private:
	vector<Building*>	buildings;
	COLORREF				BACKGROUND_TRANSPARENT_COLOR = RGB(255, 0, 255);
	CImageFile *imgBackground;
	const BackBuffer* backBuffer;
};
