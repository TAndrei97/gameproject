﻿#pragma once
#include "Sprite.h"
#include "Weapon.h"
#include "IMovingPlayer.h"
#include "IPlayerState.h"


class IPlayerState;

class PlayerWalkingState : public IPlayerState {
public:

	PlayerWalkingState(AnimatedSprite* animated_sprite, Weapon* weapon, IMovingPlayer* moving_player)
		: m_pSprite(animated_sprite),
		  weapon(weapon),
		movingPlayer(moving_player),
		  frameCounter(0){
	}

	void setSprite(Sprite* sprite) override;
	IPlayerState* goNextState(Sprite* animated_sprite) override;
	void draw() override;

	void setNext(IPlayerState* nextState) override;
	void setAbsoluteCenterPosition(const Vec2& absoluteCenterPosition) override;
	Sprite* getSprite() override;
	void animateSprite() override;
	void move(ULONG ul_direction) override;
	void setPosition(const Vec2& position) override;
	void shoot() override;
	bool isCollisionWith(Sprite* otherSprite) override;
private:

	int CHANGING_FRAME_TIME_INTERVAL = 10;
	int frameCounter;
	AnimatedSprite* m_pSprite;
	Weapon* weapon;
	IPlayerState* nextState;
	IMovingPlayer* movingPlayer;
	int SPEED = 5;

};
