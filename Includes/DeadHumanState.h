﻿#pragma once
#include "IHumanState.h"
#include "Sprite.h"

class DeadHumanState : public IHumanState {
public:
	DeadHumanState(AnimatedSprite* sprite) : m_pSprite(sprite){};
	virtual ~DeadHumanState();
	void setNext(IHumanState* nextState) override;
	void draw() override;
	IHumanState* goNextState() override;
	void setAbsolutePosition(const Vec2& absolutePosition) override;
	void update(float dt) override;

	void setSprite(AnimatedSprite* sprite) override;
	bool isToDelete() override;
	void isHit(ICollidable* other) override;
private:
	AnimatedSprite* m_pSprite;
	IHumanState* nextState;
	int money = 200;
};
