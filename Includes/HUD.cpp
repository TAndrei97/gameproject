﻿#include "HUD.h"

//HUD::HUD(Player* player, const BackBuffer* pBackBuffer, const char* moneyFileName, const char* weaponFileName, const char* lifeFileName) {
//	backBuffer = pBackBuffer;
//	this->player = player;
//	
//	RECT weaponRect;
//	weaponRect.left = 0;
//	weaponRect.top = 0;
//	weaponRect.right = 75;
//	weaponRect.bottom = 75;
//
//	RECT moneyRect;
//	moneyRect.left = 0;
//	moneyRect.top = 0;
//	moneyRect.right = 40;
//	moneyRect.bottom = 33;
//
//	weaponSprite = new AnimatedSprite(backBuffer, imgBackground, weaponFileName, BACKGROUND_TRANSPARENT_COLOR, weaponRect, 2, 4);
//	moneyAmountSprite = new CounterSprite(moneyFileName, BACKGROUND_TRANSPARENT_COLOR, moneyRect, 2, 5, 4);
//	lifeSprite = new ProgressBarSprite(lifeFileName, BACKGROUND_TRANSPARENT_COLOR, 100);
//	policeLevelSprite = new ProgressBarSprite(POLICE_LEVEL_FILE.c_str(), BACKGROUND_TRANSPARENT_COLOR, 0, 5);
//
//	weaponSprite->setBackBuffer(backBuffer);
//	moneyAmountSprite->setBackBuffer(backBuffer);
//	lifeSprite->setBackBuffer(backBuffer);
//	policeLevelSprite->setBackBuffer(backBuffer);
//}

HUD::HUD(Player* player, const BackBuffer* backBuffer, CImageFile* imgBackground, AnimatedSprite* weaponSprite, CounterSprite* moneyAmountSprite, CounterSprite* scoreSprite,ProgressBarSprite* lifeSprite, ProgressBarSprite* policeLevelSprite) {
	this->player = player;
	this->backBuffer = backBuffer;
	this->imgBackground = imgBackground;
	this->weaponSprite = weaponSprite;
	this->moneyAmountSprite = moneyAmountSprite;
	this->lifeSprite = lifeSprite;
	this->policeLevelSprite = policeLevelSprite;
	this->scoreSprite = scoreSprite;
}

HUD::~HUD() {
	/*if (weaponSprite != NULL) {
		delete weaponSprite;
		weaponSprite = NULL;
	}
	if (moneyAmountSprite != NULL) {
		delete moneyAmountSprite;
		moneyAmountSprite = NULL;
	}*/
	//if (policeLevelSprite != NULL) {
	//	delete policeLevelSprite;
	//	policeLevelSprite = NULL;
	//}
	//if (scoreSprite != NULL) {
	//	delete scoreSprite;
	//	scoreSprite = NULL;
	//}
}

void HUD::Draw() {
	weaponSprite->draw();
	moneyAmountSprite->Draw();
	lifeSprite->Draw();
	policeLevelSprite->Draw();
	scoreSprite->Draw();
}

void HUD::setPosition(Vec2 position) {
	//change position to lifeSprite!!!!!!!!!
	/*int weaponSpriteHeight = weaponSprite->height();
	int lifeSpriteHeight = lifeSprite->height();
	int moneySpriteHeight = moneyAmountSprite->height();
	int policeLevelSpriteHeight = policeLevelSprite->height();
	int scoreSpriteHeight = scoreSprite->height();
	Vec2 moneySpritePosition = position + Vec2(0, weaponSpriteHeight / 2 + moneySpriteHeight / 2);
	Vec2 lifeSpritePosition = moneySpritePosition + Vec2(0, moneySpriteHeight / 2 + lifeSpriteHeight / 2);
	
	Vec2 policeLevelSpritePosition = lifeSpritePosition + Vec2(0, lifeSpriteHeight / 2 + policeLevelSpriteHeight / 2);
	
	Vec2 scoreSpritePosition = lifeSpritePosition + Vec2(0, lifeSpriteHeight / 2 + scoreSpriteHeight / 2);*/
	int margin = 10;
	Vec2 lifeSpritePosition(backBuffer->width() - lifeSprite->width() / 2 - margin * 2 - weaponSprite->width(), margin + lifeSprite->height() / 2);
	Vec2 moneySpritePosition(backBuffer->width() - moneyAmountSprite->width() / 2 - margin * 3 - weaponSprite->width(), margin * 1.5 + lifeSprite->height() + moneyAmountSprite->height() / 2);
	Vec2 weaponPosition(backBuffer->width() - margin - weaponSprite->width() / 2.0, weaponSprite->height() / 2.0 + margin);
	Vec2 policePosition(policeLevelSprite->width() / 2 + margin, margin + policeLevelSprite->height() / 2);
	Vec2 scorePosition(scoreSprite->width() / 2 + margin, margin + policeLevelSprite->height() + margin * 2 + scoreSprite->height() / 2);
	weaponSprite->mPosition = weaponPosition;
	moneyAmountSprite->setPosition(moneySpritePosition);
	lifeSprite->setPosition(lifeSpritePosition);	
	policeLevelSprite->setPosition(policePosition);
	scoreSprite->setPosition(scorePosition);
}

void HUD::increaseMoneyAmount(int increaseAmount) {
	moneyAmount += increaseAmount;
	moneyAmountSprite->setAmount(moneyAmount);
}

void HUD::changeWeaponSprite(int spriteFrame) {
	weaponSprite->SetFrame(spriteFrame);
}

void HUD::Update() {
	moneyAmountSprite->setAmount(player->getMoney());
	lifeSprite->setProgress(player->getLife());
	weaponSprite->SetFrame(player->getWeapon());
	policeLevelSprite->setProgress(player->getPoliceLever());
	scoreSprite->setAmount(player->getScore());

}

void HUD::setPlayer(Player* player) {
	this->player = player;
}


