﻿#include "GroupOfHumans.h"

GroupOfHumans::GroupOfHumans(CImageFile* imgBackground, CollisionManagerTest* collisionMannager, Builders* builder) {
	this->imgBackground = imgBackground;
	this->collisionMannager = collisionMannager;
	this->builder = builder;
}


GroupOfHumans::~GroupOfHumans() {
	for(int i = 0; i < humans.size(); i++) {
		if (humans[i] != NULL) {
			delete humans[i];
			//humans[i] = NULL;
		}
	}
	humans.resize(0);
}

void GroupOfHumans::Draw() {
	for(int i = 0; i < humans.size(); i++) {
		humans[i]->Draw();
	}
}

void GroupOfHumans::add(Human* newHuman) {
	humans.push_back(newHuman);
}

void GroupOfHumans::generateNewHuman() {

	Human* newHuman = builder->buildHuman();

	Vec2 newHumanAbsolutePosition;
	newHumanAbsolutePosition.x = rand() % imgBackground->Width();
	newHumanAbsolutePosition.y = rand() % imgBackground->Height();
	newHuman->absoluteCenterPosition() = newHumanAbsolutePosition;

	while (collisionMannager->checkFreeSpace(newHuman) == false) {
		newHumanAbsolutePosition.x = rand() % imgBackground->Width();
		newHumanAbsolutePosition.y = rand() % imgBackground->Height();
		newHuman->absoluteCenterPosition() = newHumanAbsolutePosition;
	}

	
	humans.push_back(newHuman);

}

void GroupOfHumans::Move() {
	/*for(int i = 0; i < humans.size(); i++) {
		humans[i]->Move();
	}*/
}

void GroupOfHumans::Update(float get_time_elapsed) {
	for(int i = humans.size() - 1; i >= 0; i--) {
		if(humans[i]->isToDelete()) {
			Human* human = humans[i];
			delete human;
			human = NULL;
			humans.erase(humans.begin() + i);
			
		}
	}
	/*for(int i = 0; i < humans.size(); i++) {
		humans[i]->Update(get_time_elapsed);
	}*/
}

bool GroupOfHumans::checkCollision(ICollidableGroup* other) {
	for(int i = 0; i < humans.size(); i++) {
		if (other->checkCollision(humans[i])) {
			return true;
		}
	}
	return false;
}

bool GroupOfHumans::checkCollision(ICollidable* other) {
	for(int i = 0; i < humans.size(); i++) {
		if(other->isCollision(humans[i])) {
			humans[i]->isHit(other);
			return true;
		}
	}
	return false;
}

string GroupOfHumans::toString() {
	string result = to_string(humans.size()) + "\n";

	for (int i = 0; i < humans.size(); i++) {
		result += humans[i]->toString();
	}
	return result;
}

void GroupOfHumans::resize(int newDimension) {
	dimension = newDimension;
	humans.resize(newDimension);
}

std::istream& operator>>(std::istream& in, GroupOfHumans* humans) {

	int dimension;
	int absolutePositionX;
	int absolutePositionY;
	int rotationAngle;
	in >> dimension;
	for (int i = 0; i < humans->humans.size(); i++) {
		if (humans->humans[i] != NULL) {
			delete humans->humans[i];
			humans->humans[i] = NULL;
		}
	}
	humans->humans.clear();
	for(int i = 0; i < dimension; i++) {
		in >> absolutePositionX >> absolutePositionY >> rotationAngle;
		Vec2 absolutePosition(absolutePositionX, absolutePositionY);
		humans->add(humans->builder->buildHuman(absolutePosition, rotationAngle));
	}
	return in;
}
