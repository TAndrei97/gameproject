﻿#pragma once
#include "ICollidable.h"
#include <vector>
#include "ICollidableGroup.h"
#include "INature.h"

using namespace std;

class ICollidableGroup;

class CollisionManagerTest {
public:

	CollisionManagerTest(INature* natrue) { this->nature = natrue; }
	void checkCollision();
	void add(ICollidableGroup* collidableGroup);
	bool checkFreeSpace(ICollidable* other);

private:

	vector<ICollidableGroup*> collidableGroups;
	INature* nature;
};
