﻿#include "GroupOfPoliceCars.h"

GroupOfPoliceCars::GroupOfPoliceCars(CImageFile* img_backgroung, CollisionManagerTest* collision_Mannager, Builders* builder) : GroupOfCars(img_backgroung, collision_Mannager, builder) {
}

void GroupOfPoliceCars::setAlarm(bool value) {
	for (int i = 0; i < cars.size(); i++) {
		cars[i]->setAlarm(value);
	}
}

int GroupOfPoliceCars::getMinimalDistance(Vec2 otherPoint) {
	int minDistance = 50000;
	if (cars.size() >= 1) {
		minDistance = cars[0]->getDistance(otherPoint);
	}
	for (int i = 0; i < cars.size(); i++) {
		int auxDistance = cars[i]->getDistance(otherPoint);
		if (auxDistance < minDistance) {
			minDistance = auxDistance;
		}
	}
	return minDistance;
}

void GroupOfPoliceCars::generateNewCar() {
	PoliceCar* newCar = builder->buildPoliceCar();

	Vec2 carAbsolutePosition;
	carAbsolutePosition.x = rand() % imgBackground->Width();
	carAbsolutePosition.y = rand() % imgBackground->Height();

	newCar->absoluteCenterPosition() = carAbsolutePosition;

	while (collisionMannager->checkFreeSpace(newCar) == false) {
		carAbsolutePosition.x = rand() % imgBackground->Width();
		carAbsolutePosition.y = rand() % imgBackground->Height();
		newCar->absoluteCenterPosition() = carAbsolutePosition;
	}

	cars.push_back(newCar);

}
