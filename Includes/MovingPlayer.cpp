﻿#include "MovingPlayer.h"
#include <iostream>
#include "Player.h"

void MovingPlayer::moveLeft(int player_speed) {
	if (isPlayerRightSide()) {
		movePlayerLeft(player_speed);
	}
	else {
		if (isMapMovableToLeft()) {
			moveMapLeft(player_speed);
		}
		else {
			movePlayerLeft(player_speed);
		}

	}
}

void MovingPlayer::moveRight(int player_speed) {
	if (isPlayerLeftSide()) {
		movePlayerRight(player_speed);
	}
	else {
		if (isMapMovableToRight()) {
			moveMapRight(player_speed);
		}
		else {
			movePlayerRight(player_speed);
		}
	}
}

void MovingPlayer::moveForward(int player_speed) {
	if (isPlayerBottomSide()) {
		movePlayerForward(player_speed);
	}
	else {
		if (isMapMovableToForward()) {
			moveMapForward(player_speed);
		}
		else {
			movePlayerForward(player_speed);
		}
	}
}

void MovingPlayer::moveBackward(int player_speed) {
	if (isPlayerTopSide()) {
		movePlayerBackward(player_speed);
	}
	else {
		if (isMapMovableToBackward()) {
			moveMapBackward(player_speed);
		}
		else {
			movePlayerBackward(player_speed);
		}
	}
}

void MovingPlayer::movePlayerLeft(int player_speed) {
	m_pSprite->moveLeft(player_speed);
}

void MovingPlayer::movePlayerRight(int player_speed) {
	m_pSprite->moveRight(player_speed);
}

void MovingPlayer::movePlayerForward(int player_speed) {
	m_pSprite->moveForward(player_speed);
}

void MovingPlayer::movePlayerBackward(int player_speed) {
	m_pSprite->moveBackward(player_speed);
}

void MovingPlayer::moveMapLeft(int player_speed) {
	backgroundOffset.x -= player_speed;
}

void MovingPlayer::moveMapRight(int player_speed) {
	backgroundOffset.x += player_speed;
}

void MovingPlayer::moveMapForward(int player_speed) {
	backgroundOffset.y -= player_speed;
}

void MovingPlayer::moveMapBackward(int player_speed) {
	backgroundOffset.y += player_speed;
}

bool MovingPlayer::isPlayerLeftSide() {
	return m_pSprite->isLeftSide();
}

bool MovingPlayer::isPlayerRightSide() {
	return m_pSprite->isRightSide();
}

bool MovingPlayer::isPlayerTopSide() {
	return m_pSprite->isTopSide();
}

bool MovingPlayer::isPlayerBottomSide() {
	return  m_pSprite->isBottomSide();
}

bool MovingPlayer::isMapMovableToLeft() {
	if (backgroundOffset.x <= 0) {
		return false;
	}
	return true;
}

bool MovingPlayer::isMapMovableToRight() {
	int backBufferWidth = backBuffer->width();
	if (backgroundOffset.x >= imgBackground->Width() - backBufferWidth) {
		return false;
	}
	return true;
}

bool MovingPlayer::isMapMovableToForward() {
	if (backgroundOffset.y <= 0) {
		return false;
	}
	return true;
}

bool MovingPlayer::isMapMovableToBackward() {
	int backBufferHeight = backBuffer->height();
	if (backgroundOffset.y >= imgBackground->Height() - backBufferHeight) {
		return false;
	}
	return true;
}

void MovingPlayer::move(ULONG ul_direction, int player_speed) {
	
	backgroundOffset = imgBackground->mOffset;

	if (ul_direction & DIR_LEFT) {
		moveLeft(player_speed);
	}
	if (ul_direction & DIR_RIGHT) {
		moveRight(player_speed);
	}

	if (ul_direction & DIR_FORWARD) {
		moveForward(player_speed);
	}
	if (ul_direction & DIR_BACKWARD) {
		moveBackward(player_speed);
	}
	imgBackground->mOffset = backgroundOffset;
	keepBackgroundInsideScreen();
	keepPlayerInsideFrame();
}

void MovingPlayer::setSprite(Sprite* sprite) {
	m_pSprite = sprite;
}

void MovingPlayer::keepPlayerInsideFrame() {
	m_pSprite->keepInsideFrame();
}

void MovingPlayer::keepBackgroundInsideScreen() {
	int backBufferWidth = backBuffer->width();
	int backBufferHeight = backBuffer->height();
	if (backgroundOffset.x < 0) {
		backgroundOffset.x = 0;
	}
	if (backgroundOffset.x >= imgBackground->Width() - backBufferWidth) {
		backgroundOffset.x = imgBackground->Width() - backBufferWidth;
	}
	if (backgroundOffset.y < 0) {
		backgroundOffset.y = 0;
	}
	if (backgroundOffset.y >= imgBackground->Height() - backBufferHeight) {
		backgroundOffset.y = imgBackground->Height() - backBufferHeight;
	}
}