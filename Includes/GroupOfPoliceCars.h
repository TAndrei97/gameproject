﻿#pragma once
#include "GroupOfCars.h"


class Vec2;
class CImageFile;
class GroupOfCars;
class CollisionManagerTest;
class Builders;
class ICollidableGroup;

class GroupOfPoliceCars : public GroupOfCars{
public:
	GroupOfPoliceCars(CImageFile* img_backgroung, CollisionManagerTest* collision_Mannager, Builders* builder);
	void setAlarm(bool alarm);
	int getMinimalDistance(Vec2 otherPoint);
	void generateNewCar();
};


