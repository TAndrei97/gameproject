﻿#pragma once
#include "ICollidable.h"

class ICollidableGroup {
public:

	virtual bool checkCollision(ICollidableGroup* other) = 0;
	virtual bool checkCollision(ICollidable* other) = 0;

	virtual string toString() = 0;
	virtual void resize(int newDimension) = 0;
};
