﻿#include "ProgressBarSprite.h"
#include "Sprite.h"

ProgressBarSprite::ProgressBarSprite(const char* fileName, COLORREF backgroundTransparentColor, int progress, int numberOfDivisions) {
	
	this->fileName = fileName;
	PROGRESS = progress;
	NUMBER_OF_DIVISIONS = numberOfDivisions;

	progressBarSprite.resize(NUMBER_OF_DIVISIONS);

	for (int i = 0; i < NUMBER_OF_DIVISIONS; i++) {
		Sprite* temp;
		temp = new Sprite(fileName, backgroundTransparentColor);
		progressBarSprite[i] = temp;
	}

}

ProgressBarSprite::ProgressBarSprite(Sprite* barSprite, int progress, int numberOfDivisions, bool leftToRight) {
	PROGRESS = progress;
	NUMBER_OF_DIVISIONS = numberOfDivisions;
	progressBarSprite.resize(NUMBER_OF_DIVISIONS);
	this->leftToRight = leftToRight;
	for(int i = 0; i < NUMBER_OF_DIVISIONS; i++) {
		AnimatedSprite* tempBar = (AnimatedSprite*)malloc(sizeof(AnimatedSprite));
		memcpy(tempBar, barSprite, sizeof(AnimatedSprite));
		progressBarSprite[i] = tempBar;
	}
}

ProgressBarSprite::~ProgressBarSprite() {
	for(int i = 0; i < progressBarSprite.size(); i++) {
		delete progressBarSprite[i];
	}
}

void ProgressBarSprite::Draw() {
	if (leftToRight) {
		for (int i = 0; i < PROGRESS; i++) {
			//for(int i = 0; i < PROGRESS; i++) {
			progressBarSprite[i]->draw();
		}
	}
	else {
		for (int i = 0; i < PROGRESS; i++) {
			progressBarSprite[NUMBER_OF_DIVISIONS - i - 1]->draw();
		}
	}
}

void ProgressBarSprite::setProgress(int value) {
	PROGRESS = value;
}

void ProgressBarSprite::setPosition(Vec2 position) {
	for(int i = 0; i < progressBarSprite.size(); i++) {
		progressBarSprite[i]->mPosition.y = position.y;
	}

	int numberOfDigitsRightSide = NUMBER_OF_DIVISIONS / 2;
	progressBarSprite[0]->mPosition.x = position.x + progressBarSprite[0]->width() * numberOfDigitsRightSide;
	if (NUMBER_OF_DIVISIONS / 2 == 0) {
		progressBarSprite[0]->mPosition.x -= progressBarSprite[0]->width() / 2;
	}

	for (int i = 1; i < progressBarSprite.size(); i++) {
		progressBarSprite[i]->mPosition.x = progressBarSprite[i - 1]->mPosition.x - progressBarSprite[i]->width();
	}
}

void ProgressBarSprite::setBackBuffer(const BackBuffer* backBuffer) {
	for(int i = 0; i < progressBarSprite.size(); i++) {
		progressBarSprite[i]->setBackBuffer(backBuffer);
	}
}

double ProgressBarSprite::height() {
	return progressBarSprite[0]->height();
}

double ProgressBarSprite::width() {
	return progressBarSprite[0]->width() * NUMBER_OF_DIVISIONS;
}
