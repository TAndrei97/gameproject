﻿#include "Builders.h"
#include "MovingPlayer.h"
#include "Human.h"
#include "AliveHumanState.h"
#include "DeadHumanState.h"
#include "Nature.h"
#include "PlayerWalkingState.h"
#include "PlayerDrivingState.h"
#include "HUD.h"


Builders::Builders(const BackBuffer* backBuffer, CImageFile* imgBackground) {
	this->backBuffer = backBuffer;
	this->imgBackground = imgBackground;
	Sprite* natureSprite = buildSprite(BACKGROUND_MASK_FILE.c_str(), BACKGROUND_TRANSPARENT_COLOR);
	nature = new Nature(natureSprite);
	BACKGROUND_TRANSPARENT_COLOR = RGB(62, 62, 62);
}

PoliceCar* Builders::buildPoliceCar() {
	RECT r;
	r.top = 0;
	r.left = 0;
	r.bottom = 80;
	r.right = 189;
	int linesOfSprite = 1;
	int columnsOfSprite = 3;
	int rotationAngle = 270;
	AnimatedSprite* sprite = buildAnimatedSprite(POLICE_CAR_FILE_NAME.c_str(), BACKGROUND_TRANSPARENT_COLOR, r, linesOfSprite, columnsOfSprite, rotationAngle);
	vector<Vec2> collisionPoints;
	collisionPoints.push_back(Vec2(-83, 34));
	collisionPoints.push_back(Vec2(-83, -34));
	collisionPoints.push_back(Vec2(85, -37));
	collisionPoints.push_back(Vec2(85, 37));
	sprite->setCollisionPoints(collisionPoints);
	PoliceCar* policeCar = new PoliceCar(sprite, nature, backBuffer, imgBackground);

	return policeCar;
	
}

Weapon* Builders::buildWeapon() {
	int rotationAngle = 270;
	int columns = 4;
	int rows = 1;
	RECT r;

	r.left = 0;
	r.top = 0;
	r.right = 50;
	r.bottom = 50;

	AnimatedSprite* sprite = buildAnimatedSprite(WEAPON_FILE_NAME.c_str(), BACKGROUND_TRANSPARENT_COLOR, r, rows, columns, rotationAngle);
	
	AnimatedSprite* bullet = buildBullet();
	vector<Vec2> collisionPoints;
	collisionPoints.push_back(Vec2(0, 0));
	bullet->setCollisionPoints(collisionPoints);
	Weapon* weapon = new Weapon(sprite, backBuffer, imgBackground, rotationAngle, bullet);
	return weapon;
}

AnimatedSprite* Builders::buildAnimatedSprite(const char* fileName, COLORREF background_transparent_color, const RECT& rect, int rows, int columns, int rotationAngle) {
	AnimatedSprite* animated_sprite = new AnimatedSprite(backBuffer, imgBackground, fileName, background_transparent_color, rect, rows, columns, rotationAngle);
	return animated_sprite;
}

AnimatedSprite* Builders::buildBullet() {
	int rotationAngle = 270;
	int columns = 4;
	int rows = 1;
	RECT r;
	r.left = 0;
	r.top = 0;
	r.right = 32;
	r.bottom = 8;
	AnimatedSprite* bullet = buildAnimatedSprite(BULLET_FILE_NAME.c_str(), BACKGROUND_TRANSPARENT_COLOR, r, rows, columns, rotationAngle);
	vector<Vec2> collisionPoints;
	collisionPoints.push_back(Vec2(16, 0));
	bullet->setCollisionPoints(collisionPoints);
	return bullet;
}

IPlayerState* Builders::buildWalkingState(AnimatedSprite* sprite, Weapon* weapon, IMovingPlayer* movingPlayer) {
	IPlayerState* state = new PlayerWalkingState(sprite, weapon, movingPlayer);
	return state;
}

IPlayerState* Builders::buildDrivingState(AnimatedSprite* sprite, Weapon* weapon, IMovingPlayer* movingPlayer) {
	IPlayerState* state = new PlayerDrivingState(sprite, weapon, movingPlayer);
	return state;
}

IMovingPlayer* Builders::buildMovingPlayer(Sprite* sprite, const BackBuffer* backBuffer, CImageFile* imgBackground) {
	IMovingPlayer* movingPlayer = new MovingPlayer(sprite, backBuffer, imgBackground);
	return  movingPlayer;

}

Sprite* Builders::buildSprite(const char* fileName, COLORREF background_transparent_color) {
	Sprite* sprite = new Sprite(backBuffer, imgBackground, fileName, background_transparent_color, 0);
	return sprite;
}

INature* Builders::buildNature() {
	return nature;
}

Player* Builders::buildPlayer() {
	Weapon* weapon = buildWeapon();
	
	int playerColumns = 5;
	int playerRows = 1;
	int rotationAngle = 270;
	
	RECT r;
	r.left = 0;
	r.top = 0;
	r.right = 51;
	r.bottom = 50;
	
	AnimatedSprite* playerSprite = buildAnimatedSprite(PLAYER_INPUT_FILE_NAME.c_str(), BACKGROUND_TRANSPARENT_COLOR, r, playerRows, playerColumns, rotationAngle);
	vector<Vec2> collisionPoint;
	collisionPoint.push_back(Vec2(-23, 23));
	collisionPoint.push_back(Vec2(-23, -23));
	collisionPoint.push_back(Vec2(0, -25));
	collisionPoint.push_back(Vec2(0, 25));
	collisionPoint.push_back(Vec2(23, 0));

	playerSprite->setCollisionPoints(collisionPoint);
	IMovingPlayer* movingPlayer = buildMovingPlayer(playerSprite, backBuffer, imgBackground);

	IPlayerState* walkingPlayerState = buildWalkingState(playerSprite, weapon, movingPlayer);
	IPlayerState* drivingPlayerState = buildDrivingState(playerSprite, weapon, movingPlayer);

	walkingPlayerState->setNext(drivingPlayerState);
	drivingPlayerState->setNext(walkingPlayerState);

	Player* player = new Player(playerSprite, walkingPlayerState, weapon, nature, backBuffer, imgBackground, rotationAngle);

	return player;
}

Car* Builders::buildCar() {
	int rotationAngle = 270;
	int columns = 1;
	int rows = 1;
	RECT r;
	r.left = 0;
	r.top = 0;
	r.right = 170;
	r.bottom = 80;
	AnimatedSprite* sprite = buildAnimatedSprite(CAR_INPUT_FILE_NAME.c_str(), BACKGROUND_TRANSPARENT_COLOR, r, rows, columns, rotationAngle);
	
	vector<Vec2> collisionPoints;
	collisionPoints.push_back(Vec2(101, 32));
	collisionPoints.push_back(Vec2(-102, -33));
	collisionPoints.push_back(Vec2(-74, -31));
	collisionPoints.push_back(Vec2(-85, -12));
	collisionPoints.push_back(Vec2(-85, 14));
	collisionPoints.push_back(Vec2(-75, 32));
	sprite->setCollisionPoints(collisionPoints);

	Car* car = new Car(sprite, nature, backBuffer, imgBackground);

	return car;
}

CounterSprite* Builders::buildCounterSprite(AnimatedSprite* digitSpirte, int numberOfDigits) {
	CounterSprite* counterSprite = new CounterSprite(digitSpirte, numberOfDigits);
	return counterSprite;
}

ProgressBarSprite* Builders::buildProgressBar(Sprite* barSprite, int progress, int numberOfDivisions, bool leftToRight) {
	ProgressBarSprite* progressBar = new ProgressBarSprite(barSprite, progress, numberOfDivisions, leftToRight);
	return progressBar;
}

HUD* Builders::buildHUD(Player* player) {



	int rotationAngle = 270;


	RECT weaponRect;
	weaponRect.left = 0;
	weaponRect.top = 0;
	weaponRect.right = 75;
	weaponRect.bottom = 75;
	int weaponRows = 1;
	int weaponColumns = 4;
	AnimatedSprite* weaponSprite = buildAnimatedSprite(WEAPON_HUD_FILE_NAME.c_str(), BACKGROUND_TRANSPARENT_COLOR, weaponRect, weaponRows, weaponColumns, rotationAngle);


	RECT digitRect;
	digitRect.left = 0;
	digitRect.top = 0;
	digitRect.right = 40;
	digitRect.bottom = 33;
	int digitRows = 2;
	int digitColumns = 5;
	AnimatedSprite* digitSpirte = buildAnimatedSprite(MONEY_FILE_NAME.c_str(), BACKGROUND_TRANSPARENT_COLOR, digitRect, digitRows, digitColumns, rotationAngle);
	int numberOfDigits = 4;
	CounterSprite* moneyAmountSprite = buildCounterSprite(digitSpirte, numberOfDigits);


	CounterSprite* scoreSprite = buildCounterSprite(digitSpirte, numberOfDigits);

	Sprite* lifeBarSprite = buildSprite(LIFE_FILE_NAME.c_str(), BACKGROUND_TRANSPARENT_COLOR);
	int lifeProgress = player->getLife();
	int lifeNumberOfdivisions = 100;
	ProgressBarSprite* lifeSprite = buildProgressBar(lifeBarSprite, lifeProgress, lifeNumberOfdivisions, true);
	
	
	Sprite* policeLevelBarSprite = buildSprite(POLICE_LEVEL_HUD_FILE.c_str(), BACKGROUND_TRANSPARENT_COLOR);
	int policeLevelProgress = player->getPoliceLever();
	int policeLevelNumberOfDivisions = 5;
	ProgressBarSprite* policeLevelSprite = buildProgressBar(policeLevelBarSprite, policeLevelProgress, policeLevelNumberOfDivisions, false);


	
	HUD* hud = new HUD(player, backBuffer, imgBackground, weaponSprite, moneyAmountSprite, scoreSprite, lifeSprite, policeLevelSprite);
	return  hud;
}

IHumanState* Builders::buildAliveState(AnimatedSprite* sprite) {
	IHumanState* humanState = new AliveHumanState(sprite);
	return humanState;
}

IHumanState* Builders::buildDeadState(AnimatedSprite* sprite) {
	IHumanState* humanState = new DeadHumanState(sprite);
	return  humanState;
}

Human* Builders::buildHuman() {
	int humanRows = 1;
	int humanColumns = 1;
	int rotationAngle = 270;

	RECT r;
	r.left = 0;
	r.top = 0;
	r.right = 51;
	r.bottom = 50;


	AnimatedSprite* humanSprite = buildAnimatedSprite(HUMAN_INPUT_FILE_NAME.c_str(), BACKGROUND_TRANSPARENT_COLOR, r, humanRows, humanColumns, rotationAngle);
	vector<Vec2> collisionPoint;
	collisionPoint.push_back(Vec2(-23, 23));
	collisionPoint.push_back(Vec2(-23, -23));
	collisionPoint.push_back(Vec2(0, -25));
	collisionPoint.push_back(Vec2(0, 25));
	collisionPoint.push_back(Vec2(23, 0));
	humanSprite->setCollisionPoints(collisionPoint);
	RECT deadRect;
	deadRect.left = 0;
	deadRect.top = 0;
	deadRect.right = 25;
	deadRect.bottom = 25;
	int deadRows = 1;
	int deadColumns = 1;


	vector<Vec2> collisionPointDead;
	collisionPointDead.push_back(Vec2(10, 0));
	collisionPointDead.push_back(Vec2(-10, 0));
	collisionPointDead.push_back(Vec2(0, 10));
	collisionPointDead.push_back(Vec2(0, -10));
	AnimatedSprite* deadSprite = buildAnimatedSprite(HUMAN_DEAD_FILE.c_str(), BACKGROUND_TRANSPARENT_COLOR, deadRect, deadRows, deadColumns, rotationAngle);
	deadSprite->setCollisionPoints(collisionPointDead);
	IHumanState* humanAliveState = buildAliveState(humanSprite);
	IHumanState* humanDeadState = buildDeadState(deadSprite);

	humanAliveState->setNext(humanDeadState);
	humanDeadState->setNext(humanAliveState);


	Human* human = new Human(humanSprite, humanAliveState, nature, backBuffer, imgBackground);
	return human;
}

Human* Builders::buildHuman(Vec2 position, int rotationAngle) {
	Human* human = buildHuman();
	human->setAbsolutePosition(position);
	human->rotate(rotationAngle);
	return human;
}

Car* Builders::buildCar(Vec2 position, int rotationAngle) {
	Car* car = buildCar();
	car->setAbsolutePosition(position);
	car->rotate(rotationAngle);
	return car;
}

PoliceCar* Builders::buildPoliceCar(Vec2 position, int rotationAngle) {
	PoliceCar* policeCar = buildPoliceCar();
	policeCar->setAbsolutePosition(position);
	policeCar->rotate(rotationAngle);
	return policeCar;
}

AnimatedSprite* Builders::buildBullet(Vec2 position, int rotationAngle) {
	AnimatedSprite* bullet = buildBullet();
	bullet->mAbsoluteCenterPosition = position;
	bullet->set_angle(rotationAngle);
	return bullet;
}

Weapon* Builders::buildWeapon(int frame, vector<AnimatedSprite*> bullets) {
	Weapon* weapon = buildWeapon();
	weapon->change_weapon(frame);
	weapon->setBullets(bullets);
	return weapon;
}

Player* Builders::buildPlayer(Vec2 backgroundOffset, Vec2 position, int rotationAngle, int life, int money, int score, vector<int> availableWeapons, Weapon* weapon) {
	
	imgBackground->mOffset = backgroundOffset;
	Player* player = buildPlayer();
	player->setPosition(position);
	player->rotate(rotationAngle);
	player->setLife(life);
	player->setMoney(money);
	player->setScore(score);
	player->setAvailableWeapons(availableWeapons);
	player->setWeapon(weapon);

	return  player;
}
