﻿#pragma once
#include "Car.h"
#include "Player.h"

class PoliceCar:public Car {
public:
	//PoliceCar(const BackBuffer* pBackBuffer, const char* fileName, CImageFile* imgBackground, RECT r, int linesOfSprite, int columnsOfSprite);
	PoliceCar(AnimatedSprite* sprite, INature* nature, const BackBuffer* backBuffer, CImageFile* imgBackground);
	
	virtual ~PoliceCar();
	void setAlarm(bool alarm);
	void setCollisionPoints(vector<Vec2> collisionPoints);

	enum ESpeedStates
	{
		SPEED_START,
		SPEED_STOP
	};
private:
	void AnimateSprite();

	bool isAlarmed;
	float CAR_SPEED = 10;
	const BackBuffer* backBuffer;

	ESpeedStates m_eSpeedState;
	float					m_fTimer;

	Vec2					center;
	Vec2 direction;

	COLORREF				BACKGROUND_TRANSPARENT_COLOR = RGB(255, 0, 255);
	CImageFile *imgBackground;
	int frameCounter;
	int CHANGING_FRAME_TIME_INTERVAL = 6;
	const char* fileName;
	vector<int> ALARMED_SPRITE_FRAME = {1,2};
	int NOT_ALARMED_SPRITE_FRAME = 0;
};
