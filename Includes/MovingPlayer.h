﻿#pragma once
#include "IMovingPlayer.h"
#include "Sprite.h"

class MovingPlayer : public IMovingPlayer{
public:


	MovingPlayer(Sprite* sprite, const BackBuffer* back_buffer, CImageFile* img_background)
		: m_pSprite(sprite),
		  backBuffer(back_buffer),
		  imgBackground(img_background) {
	}

	virtual ~MovingPlayer(){}

	void move(ULONG ul_direction, int player_speed) override;
	void setSprite(Sprite* sprite) override;



private:
	void keepBackgroundInsideScreen();
	void keepPlayerInsideFrame();

	void moveLeft(int player_speed);
	void moveRight(int player_speed);
	void moveForward(int player_speed);
	void moveBackward(int player_speed);


	void movePlayerLeft(int player_speed);
	void movePlayerRight(int player_speed);
	void movePlayerForward(int player_speed);
	void movePlayerBackward(int player_speed);


	void moveMapLeft(int player_speed);
	void moveMapRight(int player_speed);
	void moveMapForward(int player_speed);
	void moveMapBackward(int player_speed);


	bool isPlayerLeftSide();
	bool isPlayerRightSide();
	bool isPlayerTopSide();
	bool isPlayerBottomSide();



	bool isMapMovableToLeft();
	bool isMapMovableToRight();
	bool isMapMovableToForward();
	bool isMapMovableToBackward();

	enum DIRECTION
	{
		DIR_FORWARD = 1,
		DIR_BACKWARD = 2,
		DIR_LEFT = 4,
		DIR_RIGHT = 8,
	};

	Sprite* m_pSprite;
	Vec2 backgroundOffset;
	const BackBuffer* backBuffer;
	CImageFile* imgBackground;
};
