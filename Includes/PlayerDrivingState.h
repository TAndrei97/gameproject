﻿#pragma once
#include "Sprite.h"
#include "Weapon.h"
#include "IMovingPlayer.h"
#include "IPlayerState.h"


class IPlayerState;

class PlayerDrivingState : public IPlayerState{
public:
	PlayerDrivingState(AnimatedSprite* sprite, Weapon* weapon, IMovingPlayer* moving_player)
		: weapon(weapon),
		  m_pSprite(sprite),
		movingPlayer(moving_player){
	}

	IPlayerState* goNextState(Sprite* animated_sprite) override;
	~PlayerDrivingState() {}
	void draw() override;

	void setSprite(Sprite* sprite) override;
	void setNext(IPlayerState* nextState) override;
	void setAbsoluteCenterPosition(const Vec2& absoluteCenterPosition) override;
	Sprite* getSprite() override;
	void animateSprite() override {}
	void move(ULONG ul_direction) override;
	void setPosition(const Vec2& position) override;

	void shoot() override {}
	bool isCollisionWith(Sprite* otherSprite) override;
private:
	Weapon* weapon;
	AnimatedSprite* m_pSprite;
	IPlayerState* nextState;
	IMovingPlayer* movingPlayer;
	int PLAYER_SPEED = 10;
};
