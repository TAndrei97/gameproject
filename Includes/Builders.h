﻿#pragma once
#include "IMovingPlayer.h"
#include "INature.h"
#include "CounterSprite.h"
#include "ProgressBarSprite.h"
#include "IHumanState.h"
#include "Human.h"
#include "Player.h"



class CImageFile;
class BackBuffer;
class Weapon;
class Player;
class AnimatedSprite;
class IPlayerState;
class PoliceCar;
class Car;
class HUD;

class Builders{
public:
	Builders(const BackBuffer* backBuffer, CImageFile* imgBackground);
	virtual ~Builders(){}
	PoliceCar* buildPoliceCar();
	Weapon* buildWeapon();
	AnimatedSprite* buildAnimatedSprite(const char *fileName, COLORREF background_transparent_color, const RECT& rect, int rows, int columns, int rotationAngle);
	AnimatedSprite* buildBullet();
	IPlayerState* buildWalkingState(AnimatedSprite* sprite, Weapon* weapon, IMovingPlayer* movingPlayer);
	IPlayerState* buildDrivingState(AnimatedSprite* sprite, Weapon* weapon, IMovingPlayer* movingPlayer);
	IMovingPlayer* buildMovingPlayer(Sprite* sprite, const BackBuffer* backBuffer, CImageFile* imgBackground);
	Sprite* buildSprite(const char *fileName, COLORREF background_transparent_color);
	INature* buildNature();
	Player* buildPlayer();
	Car* buildCar();
	CounterSprite* buildCounterSprite(AnimatedSprite* digitSpirte, int numberOfDigits);
	ProgressBarSprite* buildProgressBar(Sprite* barSprite, int progress, int numberOfDivisions, bool leftToRight);
	HUD* buildHUD(Player* player);
	IHumanState* buildAliveState(AnimatedSprite* sprite);
	IHumanState* buildDeadState(AnimatedSprite* sprite);
	Human* buildHuman();
	Human* buildHuman(Vec2 position, int rotationAngle);
	Car* buildCar(Vec2 position, int rotationAngle);
	PoliceCar* buildPoliceCar(Vec2 position, int rotationAngle);
	AnimatedSprite* buildBullet(Vec2 position, int rotationAngle);
	Weapon* buildWeapon(int frame, vector<AnimatedSprite*> bullets);
	Player* buildPlayer(Vec2 backgroundOffset, Vec2 position, int rotationAngle, int life, int money, int score, vector<int> availableWeapons, Weapon* weapon);
private:
	const BackBuffer* backBuffer;
	CImageFile* imgBackground;
	INature* nature;

	string					ENEMY_BULLET_INPUT_FILE = "data\\bullet.bmp";
	string					ENEMY_PLANE_INPUT_FILE = "data\\PlaneImgAndMask12.bmp";
	string					BACKGROUND_FILE = "data\\bgr.bmp";
	int						new_enemy;
	int						SINGLE_PLAYER = 1;
	string					PLAYER_INPUT_FILE_NAME = "Textures\\Character\\SpriteCharacter.bmp";
	string					HUMAN_INPUT_FILE_NAME = "Textures\\Character\\SpriteCharacter.bmp";
	string					BUILDING_INPUT_FILE_NAME = "Textures\\Character\\character.bmp";
	string					CAR_INPUT_FILE_NAME = "Textures\\Masini facute pe Vapor\\Car.bmp";
	string					PLAYER_MINI_MAP_FILE_NAME = "Textures\\MiniMap\\Indicator.bmp";
	string					MINI_MAP_FILE_NAME = "Textures\\MiniMap\\MiniMap.bmp";
	string					WEAPON_HUD_FILE_NAME = "Textures\\Weapons\\WeaponsSide.bmp";
	string					WEAPON_FILE_NAME = "Textures\\Weapons\\Weapons.bmp";
	string					LIFE_FILE_NAME = "Textures\\HUD\\LifeBar.bmp";
	string					MONEY_FILE_NAME = "Textures\\HUD\\Numbers.bmp";
	string					BULLET_FILE_NAME = "Textures\\Weapons\\Ammo.bmp";
	string					POLICE_CAR_FILE_NAME = "Textures\\Masini facute pe Vapor\\CarSprite.bmp";
	string					BACKGROUND_MASK_FILE = "Textures\\Background\\bgrMask.bmp";
	string					POLICE_LEVEL_HUD_FILE = "Textures\\HUD\\Star.bmp";
	string					HUMAN_DEAD_FILE = "Textures\\HUD\\dollarPack.bmp";
	
	COLORREF				BACKGROUND_TRANSPARENT_COLOR = RGB(62, 62, 62);

};
