﻿#include "PlayerWalkingState.h"

void PlayerWalkingState::setSprite(Sprite* sprite) {
	m_pSprite = (AnimatedSprite*) sprite;
	movingPlayer->setSprite(m_pSprite);
}

IPlayerState* PlayerWalkingState::goNextState(Sprite* animated_sprite) {
	return nextState;
}

void PlayerWalkingState::draw() {
	m_pSprite->draw();
	weapon->setPosition(m_pSprite->mPosition);
	weapon->Draw();
}

void PlayerWalkingState::setNext(IPlayerState* nextState) {
	this->nextState = nextState;
}

void PlayerWalkingState::setAbsoluteCenterPosition(const Vec2& absoluteCenterPosition) {
	m_pSprite->mAbsoluteCenterPosition = absoluteCenterPosition;
	weapon->setPosition(m_pSprite->mPosition);
}

Sprite* PlayerWalkingState::getSprite() {
	return m_pSprite;
}

void PlayerWalkingState::animateSprite() {
	int frameIndexToSet = 3;
	if (frameCounter > CHANGING_FRAME_TIME_INTERVAL / 2) {
		frameIndexToSet = 4;
	}

	if (frameCounter >= CHANGING_FRAME_TIME_INTERVAL) {
		frameCounter = 1;
	}

	if (0 == frameCounter) {
		frameIndexToSet = 0;
	}

	m_pSprite->SetFrame(frameIndexToSet);
}

void PlayerWalkingState::move(ULONG ul_direction) {
	if (ul_direction != 0)
		frameCounter++;
	else {
		frameCounter = 0;
	}
	movingPlayer->move(ul_direction, SPEED);
}

void PlayerWalkingState::setPosition(const Vec2& position) {
	m_pSprite->mPosition = position;
	movingPlayer->setSprite(m_pSprite);
}

void PlayerWalkingState::shoot() {
	m_pSprite->computeAbsolutePosition();
	weapon->shoot(m_pSprite->mAbsoluteCenterPosition);
}

bool PlayerWalkingState::isCollisionWith(Sprite* otherSprite) {
	return m_pSprite->isCollisionWith(otherSprite) || weapon->isCollisionWith(otherSprite);
}
