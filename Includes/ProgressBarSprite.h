﻿#pragma once
#include <vector>
#include "BackBuffer.h"

class BackBuffer;
class Sprite;
class Vec2;

class ProgressBarSprite {
public:
	ProgressBarSprite(const char* fileName, COLORREF backgroundTransparentColor, int progress = 0, int numberOfDivisions = 100);
	ProgressBarSprite(Sprite* barSprite, int progress, int numberOfDivisions, bool leftToRight = true);
	virtual ~ProgressBarSprite();

	void Draw();
	void setProgress(int value);
	void setPosition(Vec2 position);
	void setBackBuffer(const BackBuffer* backBuffer);
	double height();
	double width();

private:
	int NUMBER_OF_DIVISIONS;
	std::vector<Sprite*> progressBarSprite;
	const char* fileName;
	int PROGRESS;
	bool leftToRight;
};
