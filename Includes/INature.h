﻿#pragma once


class ICollidable;
class Sprite;

class INature {
public:
	virtual ~INature() {}

	virtual bool isCollsionWith(Sprite* otherSprite) = 0;
	virtual bool isBlockedZone(Sprite* otherSprite) = 0;
	virtual bool isDangerZone(Sprite* otherSprite) = 0;
	virtual bool isShopZone(Sprite* otherSprite) = 0;
	virtual bool checkFreeSpace(ICollidable* other) = 0;
};
