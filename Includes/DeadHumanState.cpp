﻿#include "DeadHumanState.h"


DeadHumanState::~DeadHumanState() {
	delete m_pSprite;
}

void DeadHumanState::setNext(IHumanState* nextState) {
	this->nextState = nextState;
}

void DeadHumanState::draw() {
	m_pSprite->drawByAbsolutePosition();
}

IHumanState* DeadHumanState::goNextState() {
	m_pSprite->setToDelete(true);
	return this;
}

void DeadHumanState::setAbsolutePosition(const Vec2& absolutePosition) {
	m_pSprite->mAbsoluteCenterPosition = absolutePosition;
}

void DeadHumanState::update(float dt) {
	m_pSprite->update(dt);
}

void DeadHumanState::setSprite(AnimatedSprite* sprite) {
	m_pSprite = sprite;
}

bool DeadHumanState::isToDelete() {
	return m_pSprite->isToDelete();
}

void DeadHumanState::isHit(ICollidable* other) {
	other->increaseMoney(money);
}
