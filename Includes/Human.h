﻿#pragma once

#include "Main.h"
#include "Sprite.h"
#include <iostream>
#include <vector>
#include "ImageFile.h"
#include "ICollidable.h"
#include "IHumanState.h"
#include "INature.h"

class Human : public ICollidable {
public:
	Human(const BackBuffer* pBackBuffer, const char* fileName, CImageFile* imgBackground);
	Human(AnimatedSprite* m_pSprite, IHumanState* humanState, INature* nature,const BackBuffer* backBuffer, CImageFile* imgBackground);
	virtual ~Human();

	void					Update(float dt);
	void					Draw();
	void					Move();

	Vec2&					Position();
	Vec2&					Velocity();
	Vec2&					absoluteCenterPosition();
	int height() { return m_pSprite->height(); }
	int width() { return  m_pSprite->width(); }
	bool isToDelete();

	enum ESpeedStates
	{
		SPEED_START,
		SPEED_STOP
	};

private:
	void AnimateSprite();

public:
	bool isCollision(ICollidable* other) override;
	bool isCollision(Sprite* otherSprite) override;
	void isHit(ICollidable* other) override;
	void isInDangerZone() override;
	void goLastPostion() override;
	bool isCollisionWithNature() override;
	void increaseMoney(int money) override {}
	string toString() override;

	void setAbsolutePosition(Vec2 position) override;
	void rotate(int rotationAngle) override;
	void isHit() override;
private:
	float					PLAYER_SPEED = 5;
	AnimatedSprite*					m_pSprite;
	const BackBuffer*		backBuffer;

	ESpeedStates			m_eSpeedState;
	float					m_fTimer;

	Vec2					center;
	Vec2 direction;

	COLORREF				BACKGROUND_TRANSPARENT_COLOR = RGB(255, 0, 255);
	CImageFile *imgBackground;
	int frameCounter;
	int CHANGING_FRAME_TIME_INTERVAL = 1;
	IHumanState* humanState;
	INature* nature;

};
