﻿#pragma once
#include "ICollidableGroup.h"
#include "Player.h"
#include "CGameApp.h"

class Player;

class GroupOfPlayers : public ICollidableGroup{
public:
	GroupOfPlayers(Builders* builder) { this->builder = builder; }
	bool checkCollision(ICollidableGroup* other) override;
	bool checkCollision(ICollidable* other) override;
	void add(Player* newPlayer);
	friend std::istream & operator >> (std::istream & in, GroupOfPlayers* players);
	string toString() override;
	void resize(int newDimension) override;
	Player* get(int index);
	vector<Player*> players;
	Builders* builder;
};
