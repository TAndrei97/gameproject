﻿#include "GroupOfPlayers.h"


bool GroupOfPlayers::checkCollision(ICollidableGroup* other) {
	for (int i = 0; i < players.size(); i++) {
		if(other->checkCollision(players[i])) {
			return true;
		}
	}
	return false;
}

bool GroupOfPlayers::checkCollision(ICollidable* other) {
	for (int i = 0; i < players.size(); i++) {
		if (other->isCollision(players[i])) {
			players[i]->isHit(other);
			other->isHit(players[i]);
			return true;
		}
	}
	return false;
}

void GroupOfPlayers::add(Player* newPlayer) {
	players.push_back(newPlayer);
}

string GroupOfPlayers::toString() {
	string result = to_string(players.size()) + "\n";

	for (int i = 0; i < players.size(); i++) {
		result += players[i]->toString();
	}
	return result;
}

void GroupOfPlayers::resize(int newDimension) {
	players.resize(newDimension);
}

Player* GroupOfPlayers::get(int index) {
	return players[index];
}

std::istream& operator>>(std::istream& in, GroupOfPlayers* players) {
	int dimension;
	int rotationAngle;
	in >> dimension;
	for(int i = 0; i < players->players.size(); i++) {
		if(players->players[i] != NULL) {
			delete players->players[i];
			players->players[i] = NULL;
		}
	}
	players->players.clear();
	for (int i = 0; i < dimension; i++) {
		int backgroundX;
		int backgroundY;
		int positionX;
		int positionY;
		int life;
		int money;
		int score;
		int availableWeaponsSize;

		in >> backgroundX >> backgroundY >> positionX >> positionY >> rotationAngle >> life >> money >> score;
		in >> availableWeaponsSize;
		vector<int> availableWeapons;
		availableWeapons.resize(availableWeaponsSize);
		for(int j = 0; j < availableWeaponsSize; j++) {
			int availableWeapon;
			in >> availableWeapon;
			availableWeapons[j] = availableWeapon;;
		}

		int weaponFrame;
		int bulletsSize;
		in >> weaponFrame >> bulletsSize;
		vector<AnimatedSprite*> bullets;
		for(int j = 0; j < bulletsSize; j++) {
			int bulletPositionX;
			int bulletPositionY;
			int bulletAngle;
			in >> bulletPositionX >> bulletPositionY >> bulletAngle;
			Vec2 bulletPosition(bulletPositionX, bulletPositionY);
			bullets.push_back(players->builder->buildBullet(bulletPosition, bulletAngle));
		}
		Weapon* weapon = players->builder->buildWeapon(weaponFrame, bullets);
		

		Vec2 backGroundOffset(backgroundX, backgroundY);
		Vec2 position(positionX, positionY);
		players->add(players->builder->buildPlayer(backGroundOffset, position, rotationAngle, life, money, score, availableWeapons, weapon));
	}
	return in;
}
