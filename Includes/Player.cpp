﻿#include "Player.h"
#include "ImageFile.h"
#include "Car.h"

//Player::Player(const BackBuffer* pBackBuffer, const char* playerFileName, const char* weaponFileName, const char* bulletFileName,CImageFile* imgBackground, int rotationAngle) : ICollidable(){
//	
//	this->imgBackground = imgBackground;
//	backBuffer = pBackBuffer;
//	mCar = NULL;
//	RECT r;
//	r.left = 0;
//	r.top = 0;
//	r.right = 51;
//	r.bottom = 50;
//	this->rotationAngle = rotationAngle;
//	m_pSprite = new AnimatedSprite(playerFileName, BACKGROUND_TRANSPARENT_COLOR_PLAYER, r, 1, 5, rotationAngle);
//	m_pSprite->setBackBuffer(pBackBuffer);
//	m_eSpeedState = SPEED_STOP;
//	m_fTimer = 0;
//	m_pSprite->UpdatePixelBuffer();
//
//	center.x = m_pSprite->width() / 2;
//	center.y = m_pSprite->height() / 2;
//
//	money = 300;
//	life = 100;
//	weapon1 = 0;
//	
//	weapon = new Weapon(backBuffer, weaponFileName, bulletFileName, BACKGROUND_TRANSPARENT_COLOR_WEAPON, imgBackground,rotationAngle);
//	weaponNumber = 1;
//	availableWeapons.push_back(DRIVING_FRAME);
//	availableWeapons.push_back(0);
//	availableWeapons.push_back(3);
//
//
//	driving = false;
//}

Player::Player(AnimatedSprite* m_pSprite, IPlayerState* playerState,Weapon* weapon, INature* nature, const BackBuffer* backBuffer, CImageFile* imgBackground, int rotationAngle) : ICollidable() {
	this->m_pSprite = m_pSprite;
	this->weapon = weapon;
	this->playerState = playerState;
	this->backBuffer = backBuffer;
	this->imgBackground = imgBackground;
	this->nature = nature;

	this->rotationAngle = rotationAngle;
	money = 300;
	life = 100;
	weapon1 = 0;
	mCar = NULL;
	m_eSpeedState = SPEED_STOP;
	m_fTimer = 0;
	weaponNumber = 1;
	availableWeapons.push_back(DRIVING_FRAME);
	availableWeapons.push_back(0);
	changeWeapon(0);
	m_pSprite->UpdatePixelBuffer();
	driving = false;
}

Player::~Player() {
	delete m_pSprite;
}

void Player::buyWeapon(int weaponID) {
	if (weaponID != -1 && money >= 200) {
		availableWeapons.push_back(weaponID);
		money -= 200;
	}
}

void Player::policeLevelUpdate() {
	if (policeLevel > 0) {
		int minimalDistancePolice = policeCars->getMinimalDistance(m_pSprite->mAbsoluteCenterPosition);
		if (policeTimeSpent == POLICE_LEVEL_INTERVAL) {
			policeTimeSpent = 0;
			if (minimalDistancePolice < INCERASE_POLICE_LEVEL_DISTANCE) {
				policeLevel++;
				if(policeLevel > 5) {
					policeLevel = 5;
				}
				if(policeCars->size() * POLICE_CAR_MULTIPLIER < policeLevel) {
					//policeCars->generateNewCar();
				}
			}
			else if (minimalDistancePolice > DECREASE_POLICE_LEVEL_DISTANCE) {
				policeLevel--;
				
			}
		}
		policeTimeSpent++;
	} else {
		policeCars->setAlarm(false);
	}
}

void Player::Update(float dt) {
	score += INCREASE_SCORE_POINTS;

	AnimateSprite();

	m_pSprite->update(dt);

	weapon->update(dt);

	policeLevelUpdate();

	// Get velocity
	double v = m_pSprite->mVelocity.Magnitude();

	// NOTE: for each async sound played Windows creates a thread for you
	// but only one, so you cannot play multiple sounds at once.
	// This creation/destruction of threads also leads to bad performance
	// so this method is not recommanded to be used in complex projects.

	// update internal time counter used in sound handling (not to overlap sounds)
	m_fTimer += dt;
}

void Player::Draw() {
	backgroundOffset = imgBackground->mOffset;
	imgBackground->Paint(backBuffer->getDC(), -backgroundOffset.x, -backgroundOffset.y);
	playerState->draw();
}

void Player::rotateAntiClockwise() {
	rotationAngle -= ANGLE_ROTATION;
	if (rotationAngle < 0)
		rotationAngle += 360;

	rotate(rotationAngle);
}

void Player::rotateClockwise() {
	rotationAngle += ANGLE_ROTATION;
	rotationAngle %= 360;
	rotate(rotationAngle);
}


void Player::keepPlayerInsideFrame() {
	m_pSprite->keepInsideFrame();
}

void Player::keepBackgroundInsideScreen() {
	int backBufferWidth = backBuffer->width();
	int backBufferHeight = backBuffer->height();
	if (backgroundOffset.x < 0) {
		backgroundOffset.x = 0;
	}
	if (backgroundOffset.x >= imgBackground->Width() - backBufferWidth) {
		backgroundOffset.x = imgBackground->Width() - backBufferWidth;
	}
	if (backgroundOffset.y < 0) {
		backgroundOffset.y = 0;
	}
	if (backgroundOffset.y >= imgBackground->Height() - backBufferHeight) {
		backgroundOffset.y = imgBackground->Height() - backBufferHeight;
	}	
}

void Player::AnimateSprite() {
	playerState->animateSprite();
}

void Player::MoveMap(ULONG ulDirection) {
	if (ulDirection & Player::DIR_LEFT)
		backgroundOffset.x -= PLAYER_SPEED;


	if (ulDirection & Player::DIR_RIGHT)
		backgroundOffset.x += PLAYER_SPEED;

	if (ulDirection & Player::DIR_FORWARD)
		backgroundOffset.y -= PLAYER_SPEED;

	if (ulDirection & Player::DIR_BACKWARD)
		backgroundOffset.y += PLAYER_SPEED;
}

void Player::setPoliceCars(GroupOfPoliceCars* policeCars) {
	this->policeCars = policeCars;
}


void Player::MovePlayer(ULONG ulDirection) {
	if (ulDirection & Player::DIR_LEFT)
		m_pSprite->mPosition.x -= PLAYER_SPEED;


	if (ulDirection & Player::DIR_RIGHT)
		m_pSprite->mPosition.x += PLAYER_SPEED;

	if (ulDirection & Player::DIR_FORWARD)
		m_pSprite->mPosition.y -= PLAYER_SPEED;

	if (ulDirection & Player::DIR_BACKWARD)
		m_pSprite->mPosition.y += PLAYER_SPEED;
}

bool Player::isCollision(ICollidable* other) {
	vector<AnimatedSprite*> bullets = weapon->getBullets();
	for(int i = 0; i < bullets.size(); i++) {
		if(other->isCollision(bullets[i])) {
			return true;
		}
	}
	return other->isCollision(m_pSprite);
}

bool Player::isCollision(Sprite* otherSprite) {
	return playerState->isCollisionWith(otherSprite);
}

void Player::isHit(ICollidable* other) {
	other->isHit();
	goLastPostion();
	if (policeLevel == 0) {
		policeCars->setAlarm(true); 
		policeLevel = 1;
	} 
}

string Player::toString() {
	string result;
	result = to_string(int(imgBackground->mOffset.x)) + " " + to_string(int(imgBackground->mOffset.x)) + "\n";
	result += to_string(int(m_pSprite->mPosition.x)) + " " + to_string(int(m_pSprite->mPosition.y)) + " " + to_string(m_pSprite->get_angle()) +  "\n";
	result += to_string(life) + " " + to_string(money) + " " + to_string(int(score)) + "\n";
	result = result + to_string(availableWeapons.size()) + " ";
	for(int i = 0; i < availableWeapons.size(); i++) {
		result = result + to_string(availableWeapons[i]) + " ";
	}
	result += "\n";
	result += weapon->toString();
	return result;
}

void Player::setAbsolutePosition(Vec2 position) {
	m_pSprite->mAbsoluteCenterPosition = position;
}

void Player::Move(ULONG ulDirection) {
	lastPostionPlayer = playerState->getSprite()->mPosition;
	lastPositionBackground = imgBackground->mOffset;
	playerState->move(ulDirection);
}


Vec2& Player::Position() {
	return m_pSprite->mPosition;
}

Vec2& Player::AbsoluteCenterPosition() {
	m_pSprite->computeAbsolutePosition();
	return m_pSprite->mAbsoluteCenterPosition;
}

Vec2& Player::Velocity() {
	return m_pSprite->mVelocity;
}

void Player::drive(GroupOfCars* cars) {
	if(driving) {
		return;
	}
	for(int i = 0; i < cars->size(); i++) {
		Car* car = cars->get(i);
		if(car->isPickable(m_pSprite->mPosition)) {
			drive(car);
		}
	}
}

void Player::drive(Car* car) {
	changeWeapon(DRIVING_FRAME);
	driving = true;
	AnimatedSprite* carSprite = (AnimatedSprite*)malloc(sizeof(AnimatedSprite));
	memcpy(carSprite, car->getSprite(), sizeof(AnimatedSprite));
	car->isDriven();
	playerState = playerState->goNextState(NULL);

	playerState->setSprite(carSprite);
	m_pSprite = (AnimatedSprite*)playerState->getSprite();
}

void Player::endDrive(GroupOfCars* cars) {
	if (driving) {
		driving = false;
		changeWeapon(availableWeapons[1]);
		AnimatedSprite* carSprite = (AnimatedSprite*)malloc(sizeof(AnimatedSprite));
		
		memcpy(carSprite, (AnimatedSprite*)playerState->getSprite(), sizeof(AnimatedSprite));		
		carSprite->computeAbsolutePosition();
		Car* car = new Car(carSprite, nature, backBuffer, imgBackground);
		cars->addNewCar(car);
		playerState = playerState->goNextState(NULL);
		m_pSprite = (AnimatedSprite*)playerState->getSprite();
		m_pSprite->Rotate(carSprite->get_angle());
		Vec2 newPosition;
		newPosition = carSprite->mPosition;
		newPosition.x = newPosition.x - carSprite->width() / 2 - m_pSprite->width() / 2 - 10;
		playerState->setPosition(newPosition);
	}
}

void Player::increaseMoney(int moneyIncome) {
	money += moneyIncome;
}

void Player::setMoney(int newMoneyAmount) {
	money = newMoneyAmount;
}

void Player::decreaseLife(int lifeLost) {
	life -= lifeLost;
}

void Player::increaseLife(int lifeGain) {
	life += lifeGain;
	if(life > 100) {
		life = 100;
	}
}

void Player::setLife(int newLife) {
	life = newLife;
}

void Player::setWeapon(Weapon* newWeapon) {
	weapon = newWeapon;
}

int Player::getMoney() {
	return money;
}



int Player::getWeapon() {
	return  availableWeapons[weaponNumber];
}

void Player::shoot() {
	playerState->shoot();
}

void Player::changeWeaponUp() {

	weaponNumber++;
	weaponNumber %= availableWeapons.size();
	if (weaponNumber == 0) {
		weaponNumber++;
	}
	changeWeapon(availableWeapons[weaponNumber]);
}

void Player::changeWeaponDown() {
	weaponNumber--;
	if(weaponNumber <= 0) {
		weaponNumber = availableWeapons.size() - 1;
	}

	changeWeapon(availableWeapons[weaponNumber]);
}

void Player::stop() {
	
}

void Player::isOutsideDangerZone() {
	if (life_interval >= REFRESH_LIFE_INTERVAL) {
		life_interval = 0;
		increaseLife(LIFE_CHANGE);
	}
	life_interval++;
}

void Player::isInDangerZone() {
	if(life_interval >= REFRESH_LIFE_INTERVAL) {
		life_interval = 0;
		decreaseLife(LIFE_CHANGE);
	}
	life_interval++;
	
}

void Player::goLastPostion() {
	imgBackground->mOffset = lastPositionBackground;
	m_pSprite->mPosition = lastPostionPlayer;
}

void Player::changeWeapon(int weapon_number) {
	weapon->change_weapon(weapon_number);
}

void Player::rotate(int angle) {
	m_pSprite->Rotate(angle);
	weapon->rotate(angle);
	
}

int Player::getLife() {
	return life;
}

void Player::moveLeft() {
	if (isPlayerRightSide()) {
		movePlayerLeft();
	}
	else {
		if (isMapMovableToLeft()) {
			moveMapLeft();
		}
		else {
			movePlayerLeft();
		}

	}
}

void Player::moveRight() {
	if (isPlayerLeftSide()) {
		movePlayerRight();
	}
	else {
		if (isMapMovableToRight()) {
			moveMapRight();
		}
		else {
			movePlayerRight();
		}
	}
}

void Player::moveForward() {
	if (isPlayerBottomSide()) {
		movePlayerForward();
	}
	else {
		if (isMapMovableToForward()) {
			moveMapForward();
		}
		else {
			movePlayerForward();
		}
	}
}

void Player::moveBackward() {
	if (isPlayerTopSide()) {
		movePlayerBackward();
	}
	else {
		if (isMapMovableToBackward()) {
			moveMapBackward();
		}
		else {
			movePlayerBackward();
		}
	}
}

void Player::movePlayerLeft() {
	m_pSprite->moveLeft(PLAYER_SPEED);
};

void Player::movePlayerRight() {
	m_pSprite->moveRight(PLAYER_SPEED);
};

void Player::movePlayerForward() {
	m_pSprite->moveForward(PLAYER_SPEED);
};

void Player::movePlayerBackward() {
	m_pSprite->moveBackward(PLAYER_SPEED);
};

void Player::moveMapLeft() {
	backgroundOffset.x -= PLAYER_SPEED;
};

void Player::moveMapRight() {
	backgroundOffset.x += PLAYER_SPEED;
};

void Player::moveMapForward() {
	backgroundOffset.y -= PLAYER_SPEED;
};

void Player::moveMapBackward() {
	backgroundOffset.y += PLAYER_SPEED;
};


bool Player::isPlayerLeftSide() {
	return m_pSprite->isLeftSide();
}
bool Player::isPlayerRightSide() {
	return m_pSprite->isRightSide();
	
}

bool Player::isPlayerTopSide() {
	return m_pSprite->isTopSide();
	
}

bool Player::isPlayerBottomSide() {
	return  m_pSprite->isBottomSide();

}

bool Player::isMapMovableToLeft() {
	if (backgroundOffset.x <= 0) {
		return false;
	}
	return true;
}

bool Player::isMapMovableToRight() {
	int backBufferWidth = backBuffer->width();
	if (backgroundOffset.x >= imgBackground->Width() - backBufferWidth) {
		return false;
	}
	return true;
}

bool Player::isMapMovableToForward() {
	if (backgroundOffset.y <= 0) {
		return false;
	}
	return true;
}

bool Player::isMapMovableToBackward() {
	int backBufferHeight = backBuffer->height();
	if (backgroundOffset.y >= imgBackground->Height() - backBufferHeight) {
		return false;
	}
	return true;
}

bool Player::isCollisionWithNature() {
	weapon->checkCollisionWithNature(nature);
	if (nature->isBlockedZone(m_pSprite) || nature->isShopZone(m_pSprite)) {
		goLastPostion();
		return true;
	}
	if(nature->isDangerZone(m_pSprite)) {
		isInDangerZone();
		return true;
	} else {
		isOutsideDangerZone();
	}
	return false;
}

bool Player::isGameOver() {
	return life < 0 || policeLevel > 5;
}

bool Player::isInShopZone() {
	return nature->isShopZone(m_pSprite);
}

int Player::getPoliceLever() {
	return policeLevel;
}

float Player::getScore() {
	return int(score) % 10000;
}

void Player::setPosition(Vec2 position) {
	m_pSprite->mPosition = position;
	playerState->setSprite(m_pSprite);
	m_pSprite->updateCollisionPointsRelative();
}

void Player::setScore(int score) {
	this->score = score;
}

void Player::setAvailableWeapons(vector<int> availableWeapons) {
	this->availableWeapons = availableWeapons;
}
