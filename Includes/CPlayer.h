//-----------------------------------------------------------------------------
// File: CPlayer.cpp
//
// Desc: This file stores the player object class. This class performs tasks
//	   such as player movement, some minor physics as well as rendering.
//
// Original design by Adam Hoult & Gary Simmons. Modified by Mihai Popescu.
//-----------------------------------------------------------------------------

#ifndef _CPLAYER_H_
#define _CPLAYER_H_

//-----------------------------------------------------------------------------
// CPlayer Specific Includes
//-----------------------------------------------------------------------------
#include "Main.h"
#include "Sprite.h"
#include <iostream>
#include <vector>

using namespace std;

#define MAX_SPEED 100
#define MAX_LIVES 3
//-----------------------------------------------------------------------------
// Main Class Definitions
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// Name : CPlayer (Class)
// Desc : Player class handles all player manipulation, update and management.
//-----------------------------------------------------------------------------
class CPlayer
{
public:
	//-------------------------------------------------------------------------
	// Enumerators
	//-------------------------------------------------------------------------
	enum DIRECTION 
	{ 
		DIR_FORWARD	 = 1, 
		DIR_BACKWARD	= 2, 
		DIR_LEFT		= 4, 
		DIR_RIGHT	   = 8, 
	};

	enum ESpeedStates
	{
		SPEED_START,
		SPEED_STOP
	};

	//-------------------------------------------------------------------------
	// Constructors & Destructors for This Class.
	//-------------------------------------------------------------------------
	CPlayer(const BackBuffer *pBackBuffer, const char* planeFileName, const char *aBulletFileName);
	virtual ~CPlayer();
	

	//-------------------------------------------------------------------------
	// Public Functions for This Class.
	//-------------------------------------------------------------------------
	Sprite* CreateBulletSprite(int angle);
	vector<Sprite*>			getBullets() const { return m_bulletSpriteVector; }


	void					Update( float dt );
	void					DrawScore();
	void					SetLivesSide(int side);
	void					Draw();
	void					Move(ULONG ulDirection);
	void					Shoot();
	void					Explode();
	void					deleteBullets();
	void					DrawLives();
	void					Rotate(int angle);
	void					RotateLeft();
	void					RotateRight();
	void					hittedSomething();
	void					wasHitted();


	Vec2&					Position();
	Vec2&					Velocity();


	bool					checkCollision(vector<Sprite*> bullets);
	bool					AdvanceExplosion();

	
	int						get_lives() { return lives; }
	int						getScore() { return score; }
	
	string					toString();
	
	
	friend std::istream & operator >> (std::istream & in, CPlayer* cplayer);
	
	
	static const int		LEFT_SIDE = -1;
	static const int		RIGHT_SIDE = 1;
private:
	//-------------------------------------------------------------------------
	// Private Variables for This Class.
	//-------------------------------------------------------------------------
	Sprite*					m_pSprite;
	

	vector<Sprite*>			score_sprites_vector;
	vector<Sprite*>			lives_sprite_vector;
	vector<Sprite*>			m_bulletSpriteVector;

	const BackBuffer*		backBuffer;

	ESpeedStates			m_eSpeedState;
	float					m_fTimer;
	
	bool					m_bExplosion;
	AnimatedSprite*			m_pExplosionSprite;
	int						m_iExplosionFrame;

	Vec2					center;

	char					bulletFileName[MAX_PATH];

	bool					isInsideTheFrame() const;

	int						angle;
	int						lives;
	int						score;
	int						SCORE_NO_DIGITS = 3;
	int						SCORE_INCREASE = 1;

	void					decreaseLives();
	void					updateLives();
	void					increaseScore(int increase_value) { score += increase_value; }
	void					updateScoreSprites();

	COLORREF				BACKGROUND_TRANSPARENT_COLOR = RGB(255, 0, 255);

	vector<string>			LIVES_PATH = { "data/life.bmp", "data/missingLife.bmp" };
	vector<string>			SCORE_DIGITS_PATHS = { "data/0.bmp", "data/1.bmp", "data/2.bmp", "data/3.bmp", "data/4.bmp", "data/5.bmp", "data/6.bmp", "data/7.bmp", "data/8.bmp", "data/9.bmp"};
};

#endif // _CPLAYER_H_