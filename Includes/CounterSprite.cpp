﻿#include "CounterSprite.h"
#include <iostream>
#include "Sprite.h"

CounterSprite::CounterSprite(const char* fileName, COLORREF BACKGROUND_TRANSPARENT_COLOR, RECT rect, int numberOfLines, int numberOfColumns, int numberOfDigits) {
	
	this->fileName = fileName;
	NUMBER_OF_DIGITS = numberOfDigits;
	for(int i = 0; i < NUMBER_OF_DIGITS; i++) {
		AnimatedSprite* temp = new AnimatedSprite(fileName, BACKGROUND_TRANSPARENT_COLOR, rect, numberOfLines, numberOfColumns);
		counterSprite.push_back(temp);
	}
}

CounterSprite::CounterSprite(AnimatedSprite* digitSprite, int numberOfDigits) {
	NUMBER_OF_DIGITS = numberOfDigits;
	for(int i = 0; i < NUMBER_OF_DIGITS; i++) {
		AnimatedSprite* tempDigit = (AnimatedSprite*)malloc(sizeof(AnimatedSprite));
		memcpy(tempDigit, digitSprite, sizeof(AnimatedSprite));
		counterSprite.push_back(tempDigit);
	}
}


CounterSprite::~CounterSprite() {
	for(int i = 0; i < counterSprite.size(); i++) {
		delete counterSprite[i];
	}
}

void CounterSprite::Draw() {
	for(int i = 0; i < counterSprite.size(); i++) {
		counterSprite[i]->draw();
	}
}

void CounterSprite::setAmount(int money_amount) {
	for(int i = 0; i < counterSprite.size(); i++, money_amount /= 10) {
		int moneyDigit;
		moneyDigit = money_amount % 10;
		counterSprite[i]->SetFrame(moneyDigit);
	}
}

void CounterSprite::setPosition(const Vec2& position) {
	for (int i = 0; i < counterSprite.size(); i++) {
		counterSprite[i]->mPosition.y = position.y;
	}

	int numberOfDigitsRightSide = NUMBER_OF_DIGITS / 2;
	counterSprite[0]->mPosition.x = position.x + counterSprite[0]->width() * numberOfDigitsRightSide;
	if (NUMBER_OF_DIGITS / 2 == 0) {
		counterSprite[0]->mPosition.x -= counterSprite[0]->width() / 2;
	}

	for (int i = 1; i < counterSprite.size(); i++) {
		counterSprite[i]->mPosition.x = counterSprite[i - 1]->mPosition.x - counterSprite[i]->width();
	}
}

void CounterSprite::setBackBuffer(const BackBuffer* backBuffer) {
	for(int i = 0; i < counterSprite.size(); i++) {
		counterSprite[i]->setBackBuffer(backBuffer);
	}
}

double CounterSprite::height() {
	return counterSprite[0]->height();
}

double CounterSprite::width() {
	return counterSprite[0]->width() * NUMBER_OF_DIGITS;
}
