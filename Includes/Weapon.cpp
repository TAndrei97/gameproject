﻿#include "Weapon.h"
//#include "Builders.h"
#include <stdio.h>
#include <iostream>
//#include <string.h>

Weapon::Weapon(const BackBuffer* backBuffer, const char* weaponFileName, const char* bulletFileName, COLORREF backGroundTransparentColor, CImageFile *imgBackground,int angle) {
	this->backBuffer = backBuffer;
	this->weaponFileName = weaponFileName;
	this->bulletFileName = bulletFileName;
	this->imgBackground = imgBackground;
	rotationAngle = angle;
	BACKGROUND_TRANSPARENT_COLOR = backGroundTransparentColor;

	RECT weaponRect;

	weaponRect.left = 0;
	weaponRect.top = 0;
	weaponRect.right = 50;
	weaponRect.bottom = 50;

	weaponSprite = new AnimatedSprite(weaponFileName, backGroundTransparentColor, weaponRect, ROWS_SPRITE, COLUMNS_SPRITE, rotationAngle);
	weaponSprite->setBackBuffer(backBuffer);

	bulletRect.left = 0;
	bulletRect.top = 0;
	bulletRect.right = 4;
	bulletRect.bottom = 3;

	BULLET_SPEED = 50;

	weaponShootPoint = WEAPON_SHOOT_POINT;
}

Weapon::Weapon(AnimatedSprite* m_pSprite, const BackBuffer* backBuffer, CImageFile* imgBackground,int rotationAngle, AnimatedSprite* bullet) {
	this->weaponSprite = m_pSprite;
	this->backBuffer = backBuffer;
	this->imgBackground = imgBackground;
	this->rotationAngle = rotationAngle;
	this->bullet = bullet;
}

Weapon::~Weapon() {
	delete weaponSprite;
}

void Weapon::Draw() {
	weaponSprite->draw();
	
	for(int i = 0; i < bullets.size(); i++) {
		bullets[i]->drawByAbsolutePosition();
	}
}

void Weapon::setPosition(Vec2 position) {
	weaponSprite->mPosition = position;
}

Vec2& Weapon::Position() {
	return weaponSprite->mPosition;
}

void Weapon::change_weapon(int weaponFrame) {
	this->weaponFrame = weaponFrame;
	weaponSprite->SetFrame(weaponFrame);
}

void Weapon::shoot(Vec2 playerAbsolutePosition) {
	AnimatedSprite* newBullet  = (AnimatedSprite*)malloc(sizeof(AnimatedSprite));
	memcpy(newBullet, bullet, sizeof(AnimatedSprite));
	newBullet->Rotate(rotationAngle);
	newBullet->SetFrame(weaponFrame);

	Vec2 newBulletAbsolutePosition = playerAbsolutePosition + weaponShootPoint;
	newBullet->mAbsoluteCenterPosition = newBulletAbsolutePosition;
	newBullet->computeRelativePosition();
	newBullet->computeVelocity(rotationAngle, BULLET_SPEED);
	bullets.push_back(newBullet);
}

void Weapon::rotate(int angle) {
	rotationAngle = angle;
	weaponShootPoint = WEAPON_SHOOT_POINT.Rotate(DEG2RAD(rotationAngle));
	weaponSprite->Rotate(angle);
	
}

void Weapon::update(float dt) {
	
	for(int i = 0; i < bullets.size(); i++) {
		bullets[i]->updateAbsolutePosition(dt);
	}

	for (int i = bullets.size() - 1; i >= 0; i--) {
		if (bullets[i]->isToDelete()) {
			AnimatedSprite* deleteBullet = bullets[i];
			bullets.erase(bullets.begin() + i);
			deleteBullet = NULL;
		}
	}
}

bool Weapon::isCollisionWith(Sprite* otherSprite) {
	for (int i = 0; i < bullets.size(); i++) {
		if (bullets[i]->isCollisionWith(otherSprite)) {
			bullets[i]->setToDelete(true);
			return true;
		}
	}
	return false;
}

void Weapon::checkCollisionWithNature(INature* nature) {
	for(int i = 0; i < bullets.size(); i++) {
		if(nature->isCollsionWith(bullets[i])) {
			bullets[i]->setToDelete(true);
		}
	}
}

string Weapon::toString() {
	string result = to_string(weaponFrame) + "\n";
	result += to_string(bullets.size()) + "\n";

	for(int i = 0; i < bullets.size(); i++) {
		result += bullets[i]->toString();
	}
	return result;
}

void Weapon::setBullets(vector<AnimatedSprite*> bullets) {
	this->bullets = bullets;
}

vector<AnimatedSprite*> Weapon::getBullets() {
	return bullets;
}
