﻿#include "MiniMap.h"
#include <iostream>


MiniMap::MiniMap(const BackBuffer* pBackBuffer, const char* miniMapFileName, const char* playerMiniMapFileName, CImageFile* imgBackground) {
	backBuffer = pBackBuffer;
	this->miniMapFileName = miniMapFileName;
	this->playerMiniMapFileName = playerMiniMapFileName;
	
	miniMapSprite = new Sprite(miniMapFileName, BACKGROUND_TRANSPARENT_COLOR);
	miniMapSprite->setBackBuffer(backBuffer);

	playerSprite = new Sprite(playerMiniMapFileName, BACKGROUND_TRANSPARENT_COLOR);
	playerSprite->setBackBuffer(backBuffer);
	ASPECT_RATIO = imgBackground->Width() / miniMapSprite->width();
}

MiniMap::~MiniMap() {
	delete miniMapSprite;
	delete playerSprite;
}

void MiniMap::Draw() {
	miniMapSprite->draw();
	playerSprite->draw();
}

void MiniMap::Update(Vec2 newPlayerPosition) {

	playerSprite->mPosition = upperLeftCornerPosition();
	playerSprite->mPosition += newPlayerPosition / ASPECT_RATIO;
}

Vec2& MiniMap::Position() {
	return miniMapSprite->mPosition;
}

void MiniMap::placement() {
	Vec2 backBufferDimension(backBuffer->width(), backBuffer->height());
	Vec2 miniMapDimension(miniMapSprite->width(), miniMapSprite->height());
	Vec2 miniMapCenter = miniMapDimension / 2;
	miniMapSprite->mPosition = backBufferDimension - miniMapCenter;
}

Vec2 MiniMap::upperLeftCornerPosition() {
	Vec2 result;
	Vec2 backBufferDimension(backBuffer->width(), backBuffer->height());
	Vec2 miniMapDimension(miniMapSprite->width(), miniMapSprite->height());
	result = backBufferDimension - miniMapDimension;
	return  result;

}
