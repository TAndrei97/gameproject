﻿#pragma once
#include "Sprite.h"


class ICollidable {
public:
	virtual ~ICollidable();
	virtual bool isCollision(ICollidable* other) = 0;
	virtual bool isCollision(Sprite* otherSprite) = 0;
	virtual void isHit(ICollidable* other) = 0;
	virtual void isHit() = 0;
	virtual void isInDangerZone() = 0;
	virtual void goLastPostion() = 0;
	virtual bool isCollisionWithNature() = 0;
	virtual void increaseMoney(int money) = 0;
	virtual string toString() = 0;
	virtual void setAbsolutePosition(Vec2 position) = 0;
	virtual void rotate(int rotationAngle) = 0;
};
