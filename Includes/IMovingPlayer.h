﻿#pragma once
#include "Sprite.h"

class IMovingPlayer {
public:
	virtual ~IMovingPlayer() = default;

	virtual void move(ULONG ul_direction, int player_speed) = 0;
	virtual void setSprite(Sprite* sprite) = 0;
};
