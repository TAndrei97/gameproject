﻿#include "Car.h"

//Car::Car(const BackBuffer* pBackBuffer, const char* fileName, CImageFile* imgBackground) : ICollidable(){
//
//	this->imgBackground = imgBackground;
//	backBuffer = pBackBuffer;
//
//	RECT r;
//	r.left = 0;
//	r.top = 0;
//	r.right = 128;
//	r.bottom = 128;
//	this->fileName = fileName;
//	m_pSprite = new AnimatedSprite(backBuffer, imgBackground, fileName, BACKGROUND_TRANSPARENT_COLOR, r, 4, 4);
//	m_pSprite->setBackBuffer(pBackBuffer);
//	m_pSprite->setBackgroundImage(imgBackground);
//
//	m_eSpeedState = SPEED_STOP;
//	m_fTimer = 0;
//	m_pSprite->UpdatePixelBuffer();
//
//	center.x = m_pSprite->width() / 2;
//	center.y = m_pSprite->height() / 2;
//	toDelete = false;
//}
//
//Car::Car(const BackBuffer* pBackBuffer, const char* fileName, CImageFile* imgBackground, RECT r, int linesOfSprite, int columnsOfSprite) : ICollidable(){
//
//	this->imgBackground = imgBackground;
//	backBuffer = pBackBuffer;
//
//	
//	this->fileName = fileName;
//	m_pSprite = new AnimatedSprite(backBuffer, imgBackground, fileName, BACKGROUND_TRANSPARENT_COLOR, r, linesOfSprite, columnsOfSprite);
//	m_pSprite->setBackBuffer(pBackBuffer);
//	m_pSprite->setBackgroundImage(imgBackground);
//
//	m_eSpeedState = SPEED_STOP;
//	m_fTimer = 0;
//	m_pSprite->UpdatePixelBuffer();
//
//	center.x = m_pSprite->width() / 2;
//	center.y = m_pSprite->height() / 2;
//
//	toDelete = false;
//}

Car::Car(AnimatedSprite* sprite, INature* nature, const BackBuffer* backBuffer, CImageFile* imgBackground) {
	this->m_pSprite = sprite;
	this->backBuffer = backBuffer;
	this->imgBackground = imgBackground;
	this->nature = nature;

	m_eSpeedState = SPEED_STOP;
	m_fTimer = 0;
	m_pSprite->UpdatePixelBuffer();

	center.x = m_pSprite->width() / 2;
	center.y = m_pSprite->height() / 2;
	toDelete = false;
}


Car::~Car() {
	delete m_pSprite;
}

void Car::Update(float dt) {
	AnimateSprite();
	timer++;
	m_pSprite->update(dt);

	// Get velocity
	double v = m_pSprite->mVelocity.Magnitude();

	// NOTE: for each async sound played Windows creates a thread for you
	// but only one, so you cannot play multiple sounds at once.
	// This creation/destruction of threads also leads to bad performance
	// so this method is not recommanded to be used in complex projects.

	// update internal time counter used in sound handling (not to overlap sounds)
	m_fTimer += dt;
}

void Car::Draw() {
	m_pSprite->drawByAbsolutePosition();
}

void Car::Move() {
	lastPostionPlayer = m_pSprite->mAbsoluteCenterPosition;
}

Vec2& Car::Position() {
	return m_pSprite->mPosition;
}

Vec2& Car::Velocity() {
	return m_pSprite->mVelocity;
}

Vec2& Car::absoluteCenterPosition() {
	return m_pSprite->mAbsoluteCenterPosition;
}

void Car::isDriven() {
	toDelete = true;	
}

bool Car::isToDelete() {
	return toDelete;
}

void Car::setSprite(AnimatedSprite* animated_sprite) {
	m_pSprite = animated_sprite;
}

void Car::AnimateSprite() {
}

bool Car::isCollision(ICollidable* other) {
	return other->isCollision(m_pSprite);
}

bool Car::isCollision(Sprite* otherSprite) {
	return m_pSprite->isCollisionWith(otherSprite);
}


void Car::isInDangerZone() {
	goLastPostion();
}

void Car::goLastPostion() {
	m_pSprite->mAbsoluteCenterPosition = lastPostionPlayer;
}

bool Car::isCollisionWithNature() {
	if (nature->isBlockedZone(m_pSprite) || nature->isShopZone(m_pSprite)) {
		goLastPostion();
		return true;
	}
	if (nature->isDangerZone(m_pSprite)) {
		isInDangerZone();
		return true;
	}
	return false;
}

string Car::toString() {
	return m_pSprite->toString();
}

void Car::setAbsolutePosition(Vec2 position) {
	m_pSprite->mAbsoluteCenterPosition = position;
	m_pSprite->updateCollisionPointsRelative();

}

void Car::rotate(int rotationAngle) {
	m_pSprite->Rotate(rotationAngle);
	
}


bool Car::isPickable(Vec2 position) {
	Vec2 doorPosition;
	doorPosition.x = m_pSprite->mPosition.x - m_pSprite->width() / 2;
	doorPosition.y = m_pSprite->mPosition.y;
	if(doorPosition.Distance(position) < m_pSprite->width()) {
		return true;
	}
	return false;
}

void Car::openDoor() {
	m_pSprite->SetFrame(m_pSprite->GetFrameCount());
}

void Car::setCollisionPoints(vector<Vec2> collisionPoints) {
	m_pSprite->setCollisionPoints(collisionPoints);
}

int Car::getDistance(Vec2 otherPoint) {
	return otherPoint.Distance(m_pSprite->mAbsoluteCenterPosition);
}
