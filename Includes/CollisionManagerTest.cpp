﻿#include "CollisionManagerTest.h"


void CollisionManagerTest::checkCollision() {
	for(int i = 0; i < collidableGroups.size(); i++) {
		for(int j = 0; j < collidableGroups.size(); j++) {
			if(i != j) {
				collidableGroups[i]->checkCollision(collidableGroups[j]);
			}
		}
	}
}


void CollisionManagerTest::add(ICollidableGroup* collidableGroup) {
	collidableGroups.push_back(collidableGroup);
}

bool CollisionManagerTest::checkFreeSpace(ICollidable* other) {
	if(nature->checkFreeSpace(other) == false) {
		return false;
	}
	for(int i = 0; i < collidableGroups.size(); i++) {
		if (false == collidableGroups[i]->checkCollision(other)) {
			return false;
		}
	}
	return true;

}
