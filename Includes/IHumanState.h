﻿#pragma once
#include "Vec2.h"
#include "Sprite.h"
#include "ICollidable.h"

class IHumanState {
public:
	virtual ~IHumanState(){}
	virtual void setNext(IHumanState* nextState) = 0;
	virtual void draw() = 0;
	virtual IHumanState* goNextState() = 0;
	virtual void setAbsolutePosition(const Vec2& absolutePosition) = 0;
	virtual void update(float dt) = 0;
	virtual void setSprite(AnimatedSprite* sprite) = 0;
	virtual bool isToDelete() = 0;
	virtual void isHit(ICollidable* other) = 0;
};
