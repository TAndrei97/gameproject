﻿#include "Nature.h"

Nature::Nature(Sprite* sprite) {
	m_pSprite = sprite;
	m_pSprite->UpdatePixelBufferNautre();
}

bool Nature::isCollsionWith(Sprite* otherSprite) {
	return false;
}

bool Nature::isBlockedZone(Sprite* otherSprite) {
	return otherSprite->isCollisionWithNature(m_pSprite, BLOCK_ZONE_COLOR);
}

bool Nature::isDangerZone(Sprite* otherSprite) {
	return otherSprite->isCollisionWithNature(m_pSprite, DANGER_ZONE_COLOR);
}

bool Nature::isShopZone(Sprite* otherSprite) {
	return otherSprite->isCollisionWithNature(m_pSprite, SHOP_ZONE_COLOR);
}

bool Nature::checkFreeSpace(ICollidable* other) {
	return other->isCollisionWithNature();
}
