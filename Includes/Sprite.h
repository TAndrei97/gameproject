// Sprite.h
// By Frank Luna
// August 24, 2004.
#ifndef SPRITE_H
#define SPRITE_H

#include "main.h"
#include "Vec2.h"
#include "BackBuffer.h"
#include <memory>
#include <vector>
#include <string>
#include "ImageFile.h"

#define MAX_SPEED 100
using namespace std;

#define ANGLE_ROTATION 3

class Sprite
{
public:
	Sprite(int imageID, int maskID);
	Sprite(const char *szImageFile, const char *szMaskFile);
	Sprite(const BackBuffer* backBuffer, CImageFile* imgBackground, const char *szImageFile, COLORREF crTransparentColor, int rotation_angle = 270);
	Sprite(const char *szImageFile, COLORREF crTransparentColor, int rotation_angle = 270);
	//void CreateRotationFrames();

	virtual ~Sprite();

	int width() const { return mImageBM.bmWidth; }
	int height() const { return mImageBM.bmHeight; }

	int buffer_width() { return mpBackBuffer->width(); }
	int buffer_height() { return mpBackBuffer->height(); }

	void update(float dt);
	void updateAbsolutePosition(float dt);

	void setBackBuffer(const BackBuffer *pBackBuffer);
	void draw();
	void setBackgroundImage(const CImageFile* imgBackground);
	void computeAbsolutePosition();
	void drawByAbsolutePosition();
	bool areCollisionPointsInsideOtherSprite(const Sprite* other);
	int computePixelToCheckCollision(Vec2 collision_point) const;
	vector<int> computeCollisionPixels(const Sprite* other);
	bool isBackgroundColor(COLORREF pixel_color) const;
	//bool isBackgroundColor(COLORREF pixel_color);
	bool isPixelsSpriteCollision(const Sprite* other, vector<int> pixels);
	
	bool isCollisionWith(const Sprite* other) const;
	int GetPixelToCheck(const Sprite* other);

	void set_apex(Vec2 apex) { this->apex = apex; }
	Vec2 get_apex() const { return apex; }

	int get_angle() { return rotationAngle; }
	void set_angle(int angle_rotation) { rotationAngle = angle_rotation; Rotate(rotationAngle);	}

	void updateCollisionPointsRelative();
	virtual void Rotate(int angle);
	void UpdatePixelBuffer();
	bool isApexInsideSprite(const Sprite* other) const;
	bool isPointInsideSprite(const Vec2 point) const;
	void setCollisionPoints(vector<Vec2> collisionPoints);
	//bool isPointInsideSprite(const Vec2 point);
	void Rotate(float radians, const char* szImageFile);
	int changeAngleSystemCoordonites(int angle);
	void	computeVelocity();
	void computeVelocity(int angle, int maxSpeed);
	void	computeCenter();
	string	toString();
	friend std::istream & operator >> (std::istream & in, Sprite* sprite);

	bool isInsideFrame();
	bool isInsideFrame(Vec2 backgroundOffset);
	void computeRelativePosition(const Vec2& backgroundOffset);
	void computeRelativePosition();
	void keepInsideFrame();

	void moveLeft(float speed);
	void moveRight(float speed);
	void moveForward(float speed);
	void moveBackward(float speed);

	bool isLeftSide() const;
	bool isRightSide() const;
	bool isTopSide() const;
	bool isBottomSide() const;
	virtual void setToDelete(bool value);
	void UpdatePixelBufferNautre();
	bool isSameColor(Vec2 pointToCheck, COLORREF colorToCheck);
	virtual bool isCollisionWithNature(Sprite* otherSprite, COLORREF COLOR_TO_CHECK);
	virtual bool isToDelete();

public:
	// Keep these public because they need to be
	// modified externally frequently.
	Vec2 mPosition;
	Vec2 mVelocity;
	Vec2 mAbsoluteCenterPosition;
	vector<Vec2> collisionPoints;
	vector<Vec2> collisionPointsRelative;

	vector<COLORREF> image_pixels;
	virtual void inCollisionWithNature(Sprite* sprite);
private:
	// Make copy constructor and assignment operator private
	// so client cannot copy Sprites. We do this because
	// this class is not designed to be copied because it
	// is not efficient--copying bitmaps is slow (lots of memory).
	Sprite(const Sprite& rhs);
	Sprite& operator=(const Sprite& rhs);

protected:


	int rotationAngle;
	Vec2 apex;

	HBITMAP auxmhImage;
	HBITMAP mhImage;
	HBITMAP mhMask;
	BITMAP mImageBM;
	BITMAP mMaskBM;

	HDC mhSpriteDC;
	const BackBuffer *mpBackBuffer;
	const CImageFile* imgBackground;
	COLORREF mcTransparentColor;
	void drawTransparent();
	void TransformAndDraw(INT iTransform, HWND hWnd);
	HBITMAP GetRotatedBitmapNT(HBITMAP hBitmap, float radians, COLORREF clrBack);
	void Pixel();

	void Rotate1(float radians, const char* szImageFile);
	void drawMask();
	vector<unsigned char> ToPixels(HBITMAP BitmapHandle, int& width, int& height);
	bool isSamePixelColor(const COLORREF first_pixel, const COLORREF second_pixel);
	bool toDelete = false;
	LONG mHeight;
	LONG mWidth;

	//vector<HBITMAP> rotated_images;
	


};

// AnimatedSprite
// by Mihai Popescu
// April 5, 2008
class AnimatedSprite : public Sprite
{
public:
	//NOTE: The animation is on a single row.
	//AnimatedSprite(const BackBuffer* backBuffer, CImageFile* imgBackgroung): Sprite(backBuffer, imgBackgroung) {
	//}

	AnimatedSprite(const char *szImageFile, const char *szMaskFile, const RECT& rcFirstFrame, int iFrameCount);
	AnimatedSprite(const BackBuffer* backBuffer, CImageFile* imgBackgroung, const char* szImageFile, COLORREF crTransparentColor, const RECT& rcFirstFrame, int noOfRowFrames, int noOfColumnFrames, int rotation_angle = 270);
	AnimatedSprite(const char* szImageFile, COLORREF crTransparentColor, const RECT& rcFirstFrame, int noOfRowFrames, int noOfColumnFrames, int rotation_angle = 270);
	AnimatedSprite(const char *szImageFile, COLORREF crTransparentColor, const RECT& rcFirstFrame, int iFrameCount);
	virtual ~AnimatedSprite() { }
	virtual void Rotate(int angle);
	void CropFrame();
public:
	void SetFrame(int iIndex);
	int GetFrameCount() { return miFrameCount; }


	//virtual void draw();
	//bool isInsideFrame(Vec2 backgroundOffset);
	//void computeRelativePosition(const Vec2& vec2);


protected:
	POINT mptFrameStartCrop;// first point of the frame (upper-left corner)
	POINT mptFrameCrop;		// crop point of frame
	int miFrameWidth;		// width
	int miFrameHeight;		// height
	int miFrameCount;		// number of frames
	int noOfRowFrames;
	int noOfColumnFrames;
	RECT frameRect;
	HBITMAP croppedFrame;
};



#endif // SPRITE_H

