﻿#pragma once
#include <vector>
#include "BackBuffer.h"
#include "ICollidableGroup.h"
#include "Car.h"
#include "PoliceCar.h"
#include "Builders.h"
#include "CollisionManagerTest.h"

class ICollidable;
class Car;
class CImageFile;
class ICollidableGroup;
class Builders;
class CollisionManagerTest;

using namespace std;

class GroupOfCars : public ICollidableGroup {
public:
	GroupOfCars(CImageFile* img_backgroung, CollisionManagerTest* collision_Mannager, Builders* builder);
	

	virtual ~GroupOfCars();

	void Draw();
	void addNewCar(Car* newCar);
	virtual void generateNewCar();
	void Move();
	void Update(float get_time_elapsed);
	int size();
	Car* get(int i);


	virtual bool checkCollision(ICollidableGroup* other) override;
	virtual bool checkCollision(ICollidable* other) override;
	string toString() override;

	void resize(int newDimension) override;
	friend std::istream & operator >> (std::istream & in, GroupOfCars* cars);
protected:
	vector<Car*> cars;
	const BackBuffer* backBuffer;
	CImageFile* imgBackground;
	CollisionManagerTest* collisionMannager;
	Builders* builder;
	int dimension = 0;
};

class GroupOfPoliceCars : public GroupOfCars {
public:
	GroupOfPoliceCars(CImageFile* img_backgroung, CollisionManagerTest* collision_Mannager, Builders* builder);
	void setAlarm(bool value);
	int getMinimalDistance(Vec2 otherPoint);
	void generateNewCar();
	string toString() override;
	friend std::istream & operator >> (std::istream & in, GroupOfPoliceCars* policeCars);

private:
	bool alarm = false;
};