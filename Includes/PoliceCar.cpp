﻿#include "PoliceCar.h"
#include "Player.h"

//PoliceCar::PoliceCar(const BackBuffer* pBackBuffer, const char* fileName, CImageFile* imgBackground, RECT r, int linesOfSprite, int columnsOfSprite) : Car(pBackBuffer, fileName, imgBackground, r, linesOfSprite, columnsOfSprite) {
//	backBuffer = pBackBuffer;
//	this->fileName = fileName;
//	this->imgBackground = imgBackground;
//
//}

PoliceCar::PoliceCar(AnimatedSprite* sprite, INature* nature, const BackBuffer* backBuffer, CImageFile* imgBackground) : Car(sprite, nature, backBuffer, imgBackground) {
}

PoliceCar::~PoliceCar() {
}

void PoliceCar::setAlarm(bool alarm) {
	isAlarmed = alarm;
}

void PoliceCar::setCollisionPoints(vector<Vec2> collisionPoints) {
	m_pSprite->setCollisionPoints(collisionPoints);
}

void PoliceCar::AnimateSprite() {
	int frameToSet;
	if(timer >= CHANGING_FRAME_TIME_INTERVAL * ALARMED_SPRITE_FRAME.size()) {
		timer = 0;
	}
	if(isAlarmed) {
		frameToSet = ALARMED_SPRITE_FRAME[timer / CHANGING_FRAME_TIME_INTERVAL];
	}
	else {
		frameToSet = NOT_ALARMED_SPRITE_FRAME;
	}
	
	m_pSprite->SetFrame(frameToSet);
}
