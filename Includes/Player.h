﻿#pragma once
#include "Main.h"
#include "Sprite.h"
#include <iostream>
#include <vector>
#include "ImageFile.h"
#include "Weapon.h"
#include "ICollidable.h"
#include "GroupOfCars.h"
#include "IPlayerState.h"
#include "INature.h"



class Car;
class Weapon;
class IPlayerState;
class GroupOfCars;
class GroupOfPoliceCars;

class Player : public ICollidable {
public:
	//-------------------------------------------------------------------------
	// Enumerators
	//-------------------------------------------------------------------------
	enum DIRECTION
	{
		DIR_FORWARD = 1,
		DIR_BACKWARD = 2,
		DIR_LEFT = 4,
		DIR_RIGHT = 8,
	};

	enum ESpeedStates
	{
		SPEED_START,
		SPEED_STOP
	};

	//-------------------------------------------------------------------------
	// Constructors & Destructors for This Class.
	//-------------------------------------------------------------------------

	//Player(const BackBuffer* pBackBuffer, const char* playerFileName, const char* weaponFileName, const char* bulletFileName, CImageFile* imgBackground, int rotationAngle = 0);
	Player(AnimatedSprite* m_pSprite, IPlayerState* playerState, Weapon* weapon, INature* nature, const BackBuffer* backBuffer, CImageFile* imgBackground, int rotationAngle);
	virtual ~Player();
	void buyWeapon(int weaponID);


	void policeLevelUpdate();
	void					Update(float dt);
	void					Draw();
	void rotateAntiClockwise();
	void rotateClockwise();

	void keepPlayerInsideFrame();
	
	void					Move(ULONG ulDirection);


	Vec2&					Position();
	Vec2&					AbsoluteCenterPosition();
	Vec2&					Velocity();

	void drive(GroupOfCars* cars);
	void drive(Car* car);
	void endDrive(GroupOfCars* cars);

	
	void increaseMoney(int moneyIncome) override;
	void setMoney(int newMoneyAmount);
	void decreaseLife(int lifeLost);
	void increaseLife(int lifeGain);
	void setLife(int newLife);
	void setWeapon(Weapon* newWeapon);

	int getMoney();
	int getLife();
	int getWeapon();
	void shoot();
	void changeWeaponUp();
	void changeWeaponDown();
	void stop();

	void isOutsideDangerZone();
	bool isCollisionWithNature() override;
	bool isGameOver();
	bool isInShopZone();
	int getPoliceLever();
	float getScore();
	void setPosition(Vec2 position);
	void setScore(int score);
	void setAvailableWeapons(vector<int> availableWeapons);
	AnimatedSprite*					m_pSprite;
	Car* mCar;
	bool driving;
	INature* nature;
	int life_interval = 0;
	int REFRESH_LIFE_INTERVAL = 10;
	int LIFE_CHANGE = 5;
	int policeLevel = 0;
	int policeTimeSpent = 0;
	int POLICE_LEVEL_INTERVAL = 10;
	int INCERASE_POLICE_LEVEL_DISTANCE = 200;
	int DECREASE_POLICE_LEVEL_DISTANCE = 400;
	int DRIVING_FRAME = 3;
	int POLICE_CAR_MULTIPLIER = 3;
	float INCREASE_SCORE_POINTS = 0.05;
	float score = 0;

	void isInDangerZone() override;
	void goLastPostion() override;
	void rotate(int angle);
private:



	Vec2 lastPositionBackground;
	Vec2 lastPostionPlayer;
	void changeWeapon(int weapon_number);
	

	void moveLeft();
	void moveRight();
	void moveForward();
	void moveBackward();


	void movePlayerLeft();
	void movePlayerRight();
	void movePlayerForward();
	void movePlayerBackward();
	

	void moveMapLeft();
	void moveMapRight();
	void moveMapForward();
	void moveMapBackward();


	bool isPlayerLeftSide();
	bool isPlayerRightSide();
	bool isPlayerTopSide();
	bool isPlayerBottomSide();

	

	bool isMapMovableToLeft();
	bool isMapMovableToRight();
	bool isMapMovableToForward();
	bool isMapMovableToBackward();
	


	void keepBackgroundInsideScreen();
	void AnimateSprite();
	void MoveMap(ULONG ulDirection);

	


	void MovePlayer(ULONG ulDirection);

public:
	void setPoliceCars(GroupOfPoliceCars* policeCars);
	bool isCollision(ICollidable* other) override;
	bool isCollision(Sprite* otherSprite) override;
	void isHit(ICollidable* other) override;
	string toString() override;
	void setAbsolutePosition(Vec2 position) override;
	void isHit() override { /*goLastPostion();*/ }
private:
	float					PLAYER_SPEED = 30;

	AnimatedSprite*			aux_sprite;

	const BackBuffer*		backBuffer;

	ESpeedStates			m_eSpeedState;
	float					m_fTimer;

	Vec2					center;
	Vec2 backgroundOffset;

	int life;
	int money;
	
	COLORREF				BACKGROUND_TRANSPARENT_COLOR_WEAPON = RGB(62, 62, 62);
	COLORREF				BACKGROUND_TRANSPARENT_COLOR_PLAYER = RGB(62, 62, 62);
	CImageFile *imgBackground;
	int frameCounter;
	int CHANGING_FRAME_TIME_INTERVAL = 10;
	int weapon1;

	Weapon* weapon;
	vector<AnimatedSprite*> bullets;
	int rotationAngle;
	int weaponNumber;
	vector<int> availableWeapons;
	IPlayerState* playerState;
	GroupOfPoliceCars* policeCars;
};
