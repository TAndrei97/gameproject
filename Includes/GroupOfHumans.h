﻿#pragma once
#include "ICollidableGroup.h"
#include "Human.h"
#include "Builders.h"
#include "CollisionManagerTest.h"

class Builders;
class Human;
class CollisionManagerTest;

class GroupOfHumans : public ICollidableGroup {
public:

	GroupOfHumans(CImageFile* imgBackground, CollisionManagerTest* collisionMannager, Builders* builder);
	virtual ~GroupOfHumans();

	void Draw();
	void add(Human* newHuman);
	void generateNewHuman();
	void Move();
	void Update(float get_time_elapsed);

	bool checkCollision(ICollidableGroup* other) override;
	bool checkCollision(ICollidable* other) override;
	string toString() override;
	void resize(int newDimension) override;
	friend std::istream & operator >> (std::istream & in, GroupOfHumans* humans);
private:
	vector<Human*> humans;
	const BackBuffer* backBuffer;
	CImageFile* imgBackground;
	CollisionManagerTest* collisionMannager;
	Builders* builder;
	int dimension = 0;
};
