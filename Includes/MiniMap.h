﻿#pragma once
#include "Vec2.h"
#include "BackBuffer.h"
#include "ImageFile.h"
#include "Sprite.h"

class MiniMap {
public:
	MiniMap(const BackBuffer* pBackBuffer, const char* miniMapFileName, const char* playerMiniMapFileName, CImageFile* imgBackground);
	virtual	~MiniMap();
	void					Draw();
	void	Update(Vec2 newPlayerPosition);
	Vec2&					Position();
	void placement();
private:
	Vec2 upperLeftCornerPosition();
	float ASPECT_RATIO;
	Sprite*	miniMapSprite;
	Sprite* playerSprite;
	COLORREF				BACKGROUND_TRANSPARENT_COLOR = RGB(62, 62, 62);
	CImageFile *imgBackground;
	const BackBuffer* backBuffer;
	const char* miniMapFileName;
	const char* playerMiniMapFileName;
};
