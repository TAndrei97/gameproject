﻿#pragma once
#include "Sprite.h"
#include "Player.h"

class IPlayerState {
public:
	virtual ~IPlayerState(){}
	virtual void setNext(IPlayerState* nextState) = 0;
	virtual void setSprite(Sprite* sprite) = 0;

	virtual void animateSprite() = 0;

	virtual IPlayerState* goNextState(Sprite* animated_sprite) = 0;
	virtual void draw() = 0;
	virtual void setAbsoluteCenterPosition(const Vec2& absoluteCenterPosition) = 0;
	virtual Sprite* getSprite() = 0;
	virtual void move(ULONG ul_direction) = 0;
	virtual void setPosition(const Vec2& position) = 0;
	virtual void shoot() = 0;
	virtual bool isCollisionWith(Sprite* otherSprite) = 0;
};
