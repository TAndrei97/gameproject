﻿#pragma once
#include "Vec2.h"
#include "BackBuffer.h"
#include "Sprite.h"
#include "INature.h"


class Weapon {
public:
	Weapon(const BackBuffer* backBuffer, const char* weaponFileName, const char* bulletFileName, COLORREF backGroundTransparentColor, CImageFile *imgBackground, int angle);
	Weapon(AnimatedSprite* m_pSprite, const BackBuffer* backBuffer, CImageFile* imgBackground, int rotationAngle, AnimatedSprite* bullet);
	virtual ~Weapon();
	void					Draw();
	void setPosition(Vec2 position);

	Vec2&					Position();
	void change_weapon(int weaponFrame);
	void shoot(Vec2 playerAbsolutePosition);
	void rotate(int angle);
	void update(float dt);
	bool isCollisionWith(Sprite* otherSprite);
	void checkCollisionWithNature(INature* nature);
	string toString();
	void setBullets(vector<AnimatedSprite*> bullets);
	vector<AnimatedSprite*> getBullets();

private:
	const char* weaponFileName;
	const char* bulletFileName;
	AnimatedSprite*	weaponSprite;
	COLORREF				BACKGROUND_TRANSPARENT_COLOR = RGB(61, 61, 61);
	const BackBuffer* backBuffer;
	int COLUMNS_SPRITE = 4;
	int ROWS_SPRITE = 1;
	RECT bulletRect;
	int weaponFrame;
	int rotationAngle;
	Vec2 weaponShootPoint;
	int BULLET_SPEED = 50;
	Vec2 WEAPON_SHOOT_POINT = Vec2(50, 0);
	const CImageFile* imgBackground;
	AnimatedSprite* bullet;
	vector<AnimatedSprite*> bullets;
};
