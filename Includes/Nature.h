﻿#pragma once
#include "ICollidable.h"
#include "INature.h"
#include "Sprite.h"

class Nature : public INature{
public:
	Nature(Sprite* sprite);
	virtual ~Nature() {}

	bool isCollsionWith(Sprite* otherSprite) override;
	bool isBlockedZone(Sprite* otherSprite) override;
	bool isDangerZone(Sprite* otherSprite) override;
	bool isShopZone(Sprite* otherSprite) override;
	bool checkFreeSpace(ICollidable* other) override;
	Sprite* m_pSprite;
	COLORREF DANGER_ZONE_COLOR = RGB(0, 0, 255);
	COLORREF BLOCK_ZONE_COLOR = RGB(0, 0, 0);
	COLORREF SHOP_ZONE_COLOR = RGB(255, 85, 0);

};
