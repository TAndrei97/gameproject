﻿#pragma once
#include "ProgressBarSprite.h"
#include "CounterSprite.h"
#include "Sprite.h"
#include "Player.h"
using namespace std;

class Vec2;
class CounterSprite;
class AnimatedSprite;
class CImageFile;
class BackBuffer;
class Player;


class HUD {
public:
	HUD(Player* player, const BackBuffer* backBuffer, CImageFile* imgBackground, AnimatedSprite* weaponSprite, CounterSprite* moneyAmountSprite, CounterSprite* scoreSprite, ProgressBarSprite* lifeSprite, ProgressBarSprite* policeLevelSprite);
	virtual ~HUD();
	void					Draw();
	void setPosition(Vec2 position);


	void					increaseMoneyAmount(int increaseAmount);
	void					changeWeaponSprite(int spriteFrame);

	void	Update();
	void setPlayer(Player* player);

private:
	int moneyAmount;
	AnimatedSprite*	weaponSprite;
	CounterSprite* moneyAmountSprite;
	CounterSprite* scoreSprite;
	ProgressBarSprite* lifeSprite;
	ProgressBarSprite* policeLevelSprite;

	COLORREF				BACKGROUND_TRANSPARENT_COLOR = RGB(255, 0, 255);
	CImageFile *imgBackground;
	Player* player;
	const BackBuffer* backBuffer;

	Vec2	position;
	string POLICE_LEVEL_FILE = "Textures\\HUD\\Dollar.bmp";
	
};
