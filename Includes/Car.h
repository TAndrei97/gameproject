﻿#pragma once

#include "Main.h"
#include "Sprite.h"
#include <iostream>
#include "ImageFile.h"
#include "ICollidable.h"
#include "INature.h"

class Car : public ICollidable {
public:
	Car(AnimatedSprite* sprite, INature* nature,const BackBuffer* backBuffer, CImageFile* imgBackground);
	virtual ~Car();

	void					Update(float dt);
	void					Draw();
	void					Move();
	virtual int height() { return m_pSprite->height(); }
	virtual int width() { return  m_pSprite->width(); }
	virtual void setAlarm(bool alarm) {}

	Vec2&					Position();
	Vec2&					Velocity();
	Vec2&					absoluteCenterPosition();
	AnimatedSprite*			getSprite() { return m_pSprite; }
	void isDriven();
	bool isToDelete();
	void setSprite(AnimatedSprite* animated_sprite);
	bool isPickable(Vec2 position);
	void openDoor();
	virtual void setCollisionPoints(vector<Vec2> collisionPoints);
	virtual int getDistance(Vec2 otherPoint);

	enum ESpeedStates
	{
		SPEED_START,
		SPEED_STOP
	};

protected:
	virtual void AnimateSprite();


public:
	bool isCollision(ICollidable* other) override;
	bool isCollision(Sprite* otherSprite) override;
	void isInDangerZone() override;
	void goLastPostion() override;
	bool isCollisionWithNature() override;
	void isHit(ICollidable* other) override { other->isHit(); }
	void increaseMoney(int money) override {}
	string toString() override;
	void setAbsolutePosition(Vec2 position) override;
	void rotate(int rotationAngle) override;
	void isHit() override{}
protected:
	float					PLAYER_SPEED = 5;
	AnimatedSprite*					m_pSprite;
	const BackBuffer*		backBuffer;

	ESpeedStates			m_eSpeedState;
	float					m_fTimer;

	Vec2					center;
	Vec2 direction;

	COLORREF				BACKGROUND_TRANSPARENT_COLOR = RGB(255, 0, 255);
	CImageFile *imgBackground;
	int frameCounter;
	int CHANGING_FRAME_TIME_INTERVAL = 1;
	const char* fileName;
	int timer;
	bool toDelete;
	INature* nature;
	Vec2 lastPostionPlayer;
};
