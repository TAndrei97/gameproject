﻿#include "GroupOfCars.h"

GroupOfCars::GroupOfCars(CImageFile* img_backgroung, CollisionManagerTest* collision_Mannager, Builders* builder) {
	imgBackground = img_backgroung;
	collisionMannager = collision_Mannager;
	this->builder = builder;
}

GroupOfCars::~GroupOfCars() {
	for(int i = 0; i < cars.size(); i++) {
		delete cars[i];
	}
	cars.resize(0);
}

void GroupOfCars::Draw() {
	for (int i = cars.size() - 1; i >= 0; i--) {
		if(cars[i]->isToDelete()) {
			cars.erase(cars.begin() + i);
		}
	}
	for(int i = 0; i < cars.size(); i++) {
		cars[i]->Draw();
	}
}

void GroupOfCars::addNewCar(Car* newCar) {
	cars.push_back(newCar);
	//collisionMannager->add(newCar);
}

void GroupOfCars::generateNewCar() {
	Car* newCar = builder->buildCar();
	
	Vec2 carAbsolutePosition;
	carAbsolutePosition.x = rand() % imgBackground->Width();
	carAbsolutePosition.y = rand() % imgBackground->Height();
	newCar->absoluteCenterPosition() = carAbsolutePosition;
	while (collisionMannager->checkFreeSpace(newCar) == false) {
		carAbsolutePosition.x = rand() % imgBackground->Width();
		carAbsolutePosition.y = rand() % imgBackground->Height();
		newCar->absoluteCenterPosition() = carAbsolutePosition;
	}

	cars.push_back(newCar);
	//collisionMannager->add(newCar);
}

void GroupOfCars::Move() {
}

void GroupOfCars::Update(float get_time_elapsed) {
	for(int i = 0; i < cars.size(); i++) {
		cars[i]->Update(get_time_elapsed);
	}
}

int GroupOfCars::size() {
	return  cars.size();
}

Car* GroupOfCars::get(int i) {
	return cars[i];
}

bool GroupOfCars::checkCollision(ICollidableGroup* other) {
	for(int i = 0; i < cars.size(); i++) {
		if(other->checkCollision(cars[i])) {
			return true;
		}
	}
	return false;
}

bool GroupOfCars::checkCollision(ICollidable* other) {
	for (int i = 0; i < cars.size(); i++) {
		if(other->isCollision(cars[i])) {
			cars[i]->isHit(other);
			return true;
		}
	}
	return false;
}

string GroupOfCars::toString() {
	string result = to_string(cars.size()) + "\n";

	for(int i = 0; i < cars.size(); i++) {
		result += cars[i]->toString();
	}
	return result;
}

void GroupOfCars::resize(int newDimension) {
	dimension = newDimension;
	cars.resize(newDimension);
}

GroupOfPoliceCars::GroupOfPoliceCars(CImageFile* img_backgroung, CollisionManagerTest* collision_Mannager, Builders* builder) : GroupOfCars(img_backgroung, collision_Mannager, builder) {
}

void GroupOfPoliceCars::setAlarm(bool value) {
	alarm = value;
	for (int i = 0; i < cars.size(); i++) {
		cars[i]->setAlarm(value);
	}
}

int GroupOfPoliceCars::getMinimalDistance(Vec2 otherPoint) {
	int minDistance = 50000;
	if (cars.size() >= 1) {
		minDistance = cars[0]->getDistance(otherPoint);
	}
	for (int i = 0; i < cars.size(); i++) {
		int auxDistance = cars[i]->getDistance(otherPoint);
		if (auxDistance < minDistance) {
			minDistance = auxDistance;
		}
	}
	return minDistance;
}

void GroupOfPoliceCars::generateNewCar() {
	PoliceCar* newCar = builder->buildPoliceCar();

	Vec2 carAbsolutePosition;
	carAbsolutePosition.x = rand() % imgBackground->Width();
	carAbsolutePosition.y = rand() % imgBackground->Height();

	newCar->absoluteCenterPosition() = carAbsolutePosition;

	while (collisionMannager->checkFreeSpace(newCar) == false) {
		carAbsolutePosition.x = 150;//= rand() % imgBackground->Width();
		carAbsolutePosition.y = 500; rand() % imgBackground->Height();
		newCar->absoluteCenterPosition() = carAbsolutePosition;
	}

	cars.push_back(newCar);
}

string GroupOfPoliceCars::toString() {
	string result = "";
	result = to_string(cars.size()) + "\n" + to_string(alarm) + "\n";

	for(int i = 0; i < cars.size(); i++) {
		result += cars[i]->toString();
	}
	
	return result;
}


std::istream& operator>>(std::istream& in, GroupOfCars* cars) {
	int dimension;
	int absolutePositionX;
	int absolutePositionY;
	int rotationAngle;
	in >> dimension;
	for (int i = 0; i < cars->cars.size(); i++) {
		if (cars->cars[i] != NULL) {
			delete cars->cars[i];
			cars->cars[i] = NULL;
		}
	}
	cars->cars.clear();
	for (int i = 0; i < dimension; i++) {
		in >> absolutePositionX >> absolutePositionY >> rotationAngle;
		Vec2 absolutePosition(absolutePositionX, absolutePositionY);
		cars->addNewCar(cars->builder->buildCar(absolutePosition, rotationAngle));
	}
	return in;
}

std::istream& operator>>(std::istream& in, GroupOfPoliceCars* policeCars) {
	int dimension;
	int absolutePositionX;
	int absolutePositionY;
	int rotationAngle;
	bool alarm;
	in >> dimension >> alarm;
	for (int i = 0; i < policeCars->cars.size(); i++) {
		if (policeCars->cars[i] != NULL) {
			delete policeCars->cars[i];
			policeCars->cars[i] = NULL;
		}
	}
	policeCars->cars.clear();
	for (int i = 0; i < dimension; i++) {
		in >> absolutePositionX >> absolutePositionY >> rotationAngle;
		Vec2 absolutePosition(absolutePositionX, absolutePositionY);
		policeCars->addNewCar(policeCars->builder->buildPoliceCar(absolutePosition, rotationAngle));
	}
	policeCars->setAlarm(alarm);
	return in;
}
