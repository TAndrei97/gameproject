﻿#include "PlayerDrivingState.h"
#include <iostream>

IPlayerState* PlayerDrivingState::goNextState(Sprite* animated_sprite) {
	return nextState;
}

void PlayerDrivingState::draw() {
	m_pSprite->draw();
}

void PlayerDrivingState::setSprite(Sprite* sprite) {
	m_pSprite = (AnimatedSprite*) sprite;
	movingPlayer->setSprite(m_pSprite);
}

void PlayerDrivingState::setNext(IPlayerState* nextState) {
	this->nextState = nextState;
}

void PlayerDrivingState::setAbsoluteCenterPosition(const Vec2& absoluteCenterPosition) {
	m_pSprite->mAbsoluteCenterPosition = absoluteCenterPosition;
}

Sprite* PlayerDrivingState::getSprite() {
	return m_pSprite;
}

void PlayerDrivingState::move(ULONG ul_direction) {
	movingPlayer->move(ul_direction, PLAYER_SPEED);
}

void PlayerDrivingState::setPosition(const Vec2& position) {
	m_pSprite->mPosition = position;
}

bool PlayerDrivingState::isCollisionWith(Sprite* otherSprite) {
	return m_pSprite->isCollisionWith(otherSprite);
}
