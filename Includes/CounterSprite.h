﻿#pragma once
#include <vector>
#include "BackBuffer.h"

class BackBuffer;
class Vec2;
class AnimatedSprite;
using namespace std;

class CounterSprite {
public:
	CounterSprite(const char* fileName, COLORREF BACKGROUND_TRANSPARENT_COLOR, RECT rect, int numberOfLines, int numberOfColumns, int numberOfDigits);
	CounterSprite(AnimatedSprite* digitSprite, int numberOfDigits);
	virtual ~CounterSprite();

	void Draw();
	void setAmount(int money_amount);
	void setPosition(const Vec2& vec2);
	void setBackBuffer(const BackBuffer* backBuffer);
	double height();
	double width();

private:
	int NUMBER_OF_DIGITS = 4;
	vector<AnimatedSprite*> counterSprite;
	const BackBuffer* backBuffer;
	const char* fileName;
	
};
