﻿#include "GroupOfBuildings.h"

GroupOfBuildings::~GroupOfBuildings() {
	for(int i = 0; i < buildings.size(); i++) {
		delete buildings[i];
	}
	buildings.resize(0);
}

void GroupOfBuildings::Draw() {
	for(int i = 0; i < buildings.size(); i++) {
		buildings[i]->Draw();
	}
}

void GroupOfBuildings::LoadFromFile(const char* fileName) {
	int noOfBuilding;

	ifstream inputStream;
	
	string buildingFileName;
	Vec2 buildingCenterPositionAbsolute;

	inputStream.open(fileName);

	inputStream >> noOfBuilding;

	for (int i = 0; i < noOfBuilding; i++) {
		inputStream >> buildingFileName >> buildingCenterPositionAbsolute.x >> buildingCenterPositionAbsolute.y;
		Building* building = new Building(backBuffer ,buildingFileName.c_str(), imgBackground);
		building->absoluteCenterPosition() = buildingCenterPositionAbsolute;
		buildings.push_back(building);
	}

}
