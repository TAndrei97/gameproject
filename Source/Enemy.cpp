//-----------------------------------------------------------------------------
// File: CPlayer.cpp
//
// Desc: This file stores the player object class. This class performs tasks
//       such as player movement, some minor physics as well as rendering.
//
// Original design by Adam Hoult & Gary Simmons. Modified by Mihai Popescu.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// CPlayer Specific Includes
//-----------------------------------------------------------------------------
#include "Enemy.h"
#include <iostream>
#include <ctime>

//-----------------------------------------------------------------------------
// Name : CPlayer () (Constructor)
// Desc : CPlayer Class Constructor
//-----------------------------------------------------------------------------
Enemy::Enemy(const BackBuffer *pBackBuffer, const char* planeFileName,const char *aBulletFileName)
{

	backBuffer = pBackBuffer;
	strcpy_s(bulletFileName, MAX_PATH, aBulletFileName);
	angle = 270;
	m_eSprite = new Sprite(planeFileName, RGB(0xff, 0x00, 0xff),angle);
	m_eSprite->setBackBuffer(pBackBuffer);
	m_eSpeedState = SPEED_STOP;
	m_fTimer = 0;
	center.x = m_eSprite->width() / 2;
	center.y = m_eSprite->height() / 2;
	m_eSprite->UpdatePixelBuffer();

	srand(time(NULL));

	int randX = rand() % backBuffer->width();
	int randY = rand() % backBuffer->height();
	m_eSprite->mPosition = Vec2(randX, randY);

	// Animation frame crop rectangle
	RECT r;
	r.left = 0;
	r.top = 0;
	r.right = 128;
	r.bottom = 128;
	m_pExplosionSprite = new AnimatedSprite("data/explosion.bmp", "data/explosionmask.bmp", r, 16);
	m_pExplosionSprite->setBackBuffer(pBackBuffer);
	m_bExplosion = false;
	m_iExplosionFrame = 1;
}

//-----------------------------------------------------------------------------
// Name : ~CPlayer () (Destructor)
// Desc : CPlayer Class Destructor
//-----------------------------------------------------------------------------
Enemy::~Enemy()
{
	delete m_eSprite;
	delete m_pExplosionSprite;
}

void Enemy::Shoot() {
	Sprite* m_bulletSprite = new Sprite(bulletFileName, RGB(0xff, 0x00, 0xff), angle);
	m_bulletSprite->setBackBuffer(backBuffer);

	Vec2 enemy_apex = m_eSprite->get_apex();

	m_bulletSprite->mPosition.x = m_eSprite->mPosition.x + enemy_apex.x;
	m_bulletSprite->mPosition.y = m_eSprite->mPosition.y - enemy_apex.y;

	m_bulletSprite->mVelocity.x = MAX_SPEED * cos(DEG2RAD(angle));
	m_bulletSprite->mVelocity.y = -MAX_SPEED * sin(DEG2RAD(angle));
	m_bulletSpriteVector.push_back(m_bulletSprite);
}

void Enemy::Update(float dt)
{
	// Update sprite
	m_eSprite->update(dt);


	// Get velocity
	double v = m_eSprite->mVelocity.Magnitude();

	// NOTE: for each async sound played Windows creates a thread for you
	// but only one, so you cannot play multiple sounds at once.
	// This creation/destruction of threads also leads to bad performance
	// so this method is not recommanded to be used in complex projects.

	// update internal time counter used in sound handling (not to overlap sounds)
	m_fTimer += dt;

	// A FSM is used for sound manager 
	switch (m_eSpeedState)
	{
	case SPEED_STOP:
		if (v > 35.0f)
		{
			m_eSpeedState = SPEED_START;
			PlaySound("data/jet-start.wav", NULL, SND_FILENAME | SND_ASYNC);
			m_fTimer = 0;
		}
		break;
	case SPEED_START:
		if (v < 25.0f)
		{
			m_eSpeedState = SPEED_STOP;
			PlaySound("data/jet-stop.wav", NULL, SND_FILENAME | SND_ASYNC);
			m_fTimer = 0;
		}
		else
			if (m_fTimer > 1.f)
			{
				PlaySound("data/jet-cabin.wav", NULL, SND_FILENAME | SND_ASYNC);
				m_fTimer = 0;
			}
		break;
	}

	// NOTE: For sound you also can use MIDI but it's Win32 API it is a bit hard
	// see msdn reference: http://msdn.microsoft.com/en-us/library/ms711640.aspx
	// In this case you can use a C++ wrapper for it. See the following article:
	// http://www.codeproject.com/KB/audio-video/midiwrapper.aspx (with code also)
}



bool Enemy::checkCollision(vector<Sprite*> bullets) {
	return false;
}

void Enemy::Draw()
{
	if (!m_bExplosion) {
		m_eSprite->draw();
		for (int i = 0; i < m_bulletSpriteVector.size(); i++) {
			m_bulletSpriteVector[i]->draw();
			m_bulletSpriteVector[i]->mPosition.y += 1;
			if (m_bulletSpriteVector[i]->mPosition.y + m_bulletSpriteVector[i]->height() / 2 <= 0)
				m_bulletSpriteVector.erase(m_bulletSpriteVector.begin() + i);
		}

	}
	else
		m_pExplosionSprite->draw();
}

void Enemy::deleteBullets() {
	m_bulletSpriteVector.erase(m_bulletSpriteVector.begin(), m_bulletSpriteVector.end());
}

string Enemy::toString() {
	string result = "";
	m_eSprite->set_angle(angle);
	result += m_eSprite->toString();
	result += to_string(m_bulletSpriteVector.size()) + "\n";
	for (int i = 0; i < m_bulletSpriteVector.size(); i++) {
		result += m_bulletSpriteVector[i]->toString();
	}

	return result;
}

Sprite* Enemy::CreateBulletSprite(int angle = 0) {
	Sprite* m_bulletSprite = new Sprite(bulletFileName, RGB(0xff, 0x00, 0xff), angle);
	m_bulletSprite->setBackBuffer(backBuffer);
	Vec2 plane_apex = m_eSprite->get_apex();

	m_bulletSprite->mPosition.x = m_eSprite->mPosition.x + plane_apex.x;
	m_bulletSprite->mPosition.y = m_eSprite->mPosition.y - plane_apex.y;

	m_bulletSprite->computeVelocity();
	return m_bulletSprite;

}

bool Enemy::isInsideTheFrame() const {
	if (m_eSprite->mPosition.x <= center.x || m_eSprite->mPosition.x + center.x >= m_eSprite->buffer_width())
		return false;

	if (m_eSprite->mPosition.y <= center.y || m_eSprite->mPosition.y + center.y >= m_eSprite->buffer_height())
		return false;

	return true;
}


void Enemy::Move()
{
	srand(time(NULL));
	float randomNumberX = (rand() % (2 * MAX_MOVE_SPEED) - MAX_MOVE_SPEED);
	float randomNumberY = (rand() % (2 * MAX_MOVE_SPEED) - MAX_MOVE_SPEED);
	m_eSprite->mVelocity.x = randomNumberX;
	m_eSprite->mVelocity.y = randomNumberY;

	if (m_eSprite->mPosition.x <= center.x || m_eSprite->mPosition.x + center.x >= m_eSprite->buffer_width())
		m_eSprite->mVelocity.x = 0;

	if (m_eSprite->mPosition.y <= center.y || m_eSprite->mPosition.y + center.y >= m_eSprite->buffer_height())
		m_eSprite->mVelocity.y = 0;
	SHOOT_VARIABLE--;
	
	int fire = rand() % 10;
	//if (fire == 5)
		//Shoot();
	if (SHOOT_VARIABLE == 0) {
		SHOOT_VARIABLE = (rand() % 10 + 1) * 300;
		Shoot();
	}
}


Vec2& Enemy::Position()
{
	return m_eSprite->mPosition;
}

Vec2& Enemy::Velocity()
{
	return m_eSprite->mVelocity;
}

void Enemy::Explode()
{
	m_pExplosionSprite->mPosition = m_eSprite->mPosition;
	m_pExplosionSprite->SetFrame(0);
	PlaySound("data/explosion.wav", NULL, SND_FILENAME | SND_ASYNC);
	m_bExplosion = true;
}

bool Enemy::AdvanceExplosion()
{
	if (m_bExplosion)
	{
		m_pExplosionSprite->SetFrame(m_iExplosionFrame++);
		if (m_iExplosionFrame == m_pExplosionSprite->GetFrameCount())
		{
			m_bExplosion = false;
			m_iExplosionFrame = 0;
			m_eSprite->mVelocity = Vec2(0, 0);
			m_eSpeedState = SPEED_STOP;
			return false;
		}
	}
	return true;
}

istream & operator >> (istream & in, Enemy* enemy) {

	in >> enemy->m_eSprite;
	enemy->m_eSprite->mVelocity = Vec2(0, 0);
	enemy->angle = enemy->m_eSprite->get_angle();
	int no_of_bullets;
	in >> no_of_bullets;

	enemy->m_bulletSpriteVector.resize(0);
	for (int i = 0; i < no_of_bullets; i++) {
		Sprite* bullet = enemy->CreateBulletSprite();
		in >> bullet;
		enemy->m_bulletSpriteVector.push_back(bullet);
	}
	return in;
}