
#include "Sprite.h"
#include <iostream>
#include <vector>
#include "ImageFile.h"

extern HINSTANCE g_hInst;
using namespace Gdiplus;
Sprite::Sprite(int imageID, int maskID)
{
	// Load the bitmap resources.
	mhImage = LoadBitmap(g_hInst, MAKEINTRESOURCE(imageID));
	mhMask = LoadBitmap(g_hInst, MAKEINTRESOURCE(maskID));

	// Get the BITMAP structure for each of the bitmaps.
	GetObject(mhImage, sizeof(BITMAP), &mImageBM);
	GetObject(mhMask, sizeof(BITMAP), &mMaskBM);

	// Image and Mask should be the same dimensions.
	assert(mImageBM.bmWidth == mMaskBM.bmWidth);
	assert(mImageBM.bmHeight == mMaskBM.bmHeight);

	mcTransparentColor = 0;
	mhSpriteDC = 0;
}

Sprite::Sprite(const char *szImageFile, const char *szMaskFile)
{
	mhImage = (HBITMAP)LoadImage(g_hInst, szImageFile, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION | LR_LOADFROMFILE);
	mhMask = (HBITMAP)LoadImage(g_hInst, szMaskFile, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION | LR_LOADFROMFILE);

	// Get the BITMAP structure for each of the bitmaps.
	GetObject(mhImage, sizeof(BITMAP), &mImageBM);
	GetObject(mhMask, sizeof(BITMAP), &mMaskBM);

	// Image and Mask should be the same dimensions.
	assert(mImageBM.bmWidth == mMaskBM.bmWidth);
	assert(mImageBM.bmHeight == mMaskBM.bmHeight);

	mcTransparentColor = 0;
	mhSpriteDC = 0;

}

Sprite::Sprite(const BackBuffer* backBuffer, CImageFile* imgBackground, const char *szImageFile, COLORREF crTransparentColor, int rotation_angle)
{
	this->mpBackBuffer = backBuffer;
	this->imgBackground = imgBackground;
	
	mhImage = (HBITMAP)LoadImage(g_hInst, szImageFile, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION | LR_LOADFROMFILE);

	mhMask = 0;
	mhSpriteDC = 0;
	mcTransparentColor = crTransparentColor;
	mcTransparentColor = mcTransparentColor & 0xFFFFFF;
	// Get the BITMAP structure for the bitmap.
	GetObject(mhImage, sizeof(BITMAP), &mImageBM);

	auxmhImage = mhImage;

	rotationAngle = rotation_angle;
	Rotate(rotationAngle);
	
	/*if(rotationAngle != 0) {
		mhImage = GetRotatedBitmapNT(mhImage, DEG2RAD(rotationAngle), mcTransparentColor);

		GetObject(mhImage, sizeof(BITMAP), &mImageBM);
	}*/
	int raza = width() / 2;
	double radians = degreeToRadians(rotationAngle);
	Vec2 plane_apex = Polar(raza, radians);

	set_apex(plane_apex);
	setBackBuffer(this->mpBackBuffer);
	setBackgroundImage(this->imgBackground);
}

Sprite::Sprite(const char *szImageFile, COLORREF crTransparentColor, int rotation_angle)
{
	mhImage = (HBITMAP)LoadImage(g_hInst, szImageFile, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION | LR_LOADFROMFILE);

	mhMask = 0;
	mhSpriteDC = 0;
	mcTransparentColor = crTransparentColor;
	mcTransparentColor = mcTransparentColor & 0xFFFFFF;
	// Get the BITMAP structure for the bitmap.
	GetObject(mhImage, sizeof(BITMAP), &mImageBM);

	auxmhImage = mhImage;

	rotationAngle = rotation_angle;
	Rotate(rotationAngle);

	/*if(rotationAngle != 0) {
	mhImage = GetRotatedBitmapNT(mhImage, DEG2RAD(rotationAngle), mcTransparentColor);

	GetObject(mhImage, sizeof(BITMAP), &mImageBM);
	}*/
	int raza = width() / 2;
	double radians = degreeToRadians(rotationAngle);
	Vec2 plane_apex = Polar(raza, radians);

	set_apex(plane_apex);

}

//void Sprite::CreateRotationFrames() {
//	int i = 0;
//
//	for (int angle = 0; angle < 360; angle += ANGLE_ROTATION, i++) {
//		rotated_images.push_back(GetRotatedBitmapNT(mhImage, DEG2RAD(angle), RGB(255, 0, 255)));
//	}
//}


Sprite::~Sprite()
{
	// Free the resources we created in the constructor.
	DeleteObject(mhImage);
	DeleteObject(mhMask);

	DeleteDC(mhSpriteDC);
	//delete[] rotated_images;
}

void Sprite::update(float dt)
{
	// Update the sprites position.
	updateCollisionPointsRelative();
	mPosition += mVelocity * dt;
	// Update bounding rectangle/circle
}

void Sprite::updateAbsolutePosition(float dt) {
	updateCollisionPointsRelative();
	mAbsoluteCenterPosition += mVelocity * dt;

}

void Sprite::setBackBuffer(const BackBuffer *pBackBuffer)
{
	mpBackBuffer = pBackBuffer;
	if (mpBackBuffer)
	{
		DeleteDC(mhSpriteDC);
		mhSpriteDC = CreateCompatibleDC(mpBackBuffer->getDC());
	}
}

void Sprite::draw()
{
	if (mhMask != 0)
		drawMask();
	else
		drawTransparent();
}

void Sprite::setBackgroundImage(const CImageFile* imgBackground) {
	this->imgBackground = imgBackground;
}

void Sprite::computeAbsolutePosition() {
	mAbsoluteCenterPosition = mPosition + imgBackground->mOffset;
}

void Sprite::drawByAbsolutePosition() {
	if (isInsideFrame(imgBackground->mOffset)) {
		computeRelativePosition(imgBackground->mOffset);
		draw();
	}
}



void Sprite::drawMask()
{
	if (mpBackBuffer == NULL)
		return;

	HDC hBackBufferDC = mpBackBuffer->getDC();

	// The position BitBlt wants is not the sprite's center
	// position; rather, it wants the upper-left position,
	// so compute that.
	int w = width();
	int h = height();

	// Upper-left corner.
	int x = (int)mPosition.x - (w / 2);
	int y = (int)mPosition.y - (h / 2);

	// Note: For this masking technique to work, it is assumed
	// the backbuffer bitmap has been cleared to some
	// non-zero value.
	// Select the mask bitmap.
	HGDIOBJ oldObj = SelectObject(mhSpriteDC, mhMask);

	// Draw the mask to the backbuffer with SRCAND. This
	// only draws the black pixels in the mask to the backbuffer,
	// thereby marking the pixels we want to draw the sprite
	// image onto.
	BitBlt(hBackBufferDC, x, y, w, h, mhSpriteDC, 0, 0, SRCAND);

	// Now select the image bitmap.
	SelectObject(mhSpriteDC, mhImage);

	// Draw the image to the backbuffer with SRCPAINT. This
	// will only draw the image onto the pixels that where previously
	// marked black by the mask.
	BitBlt(hBackBufferDC, x, y, w, h, mhSpriteDC, 0, 0, SRCPAINT);

	// Restore the original bitmap object.
	SelectObject(mhSpriteDC, oldObj);
}

vector<unsigned char> Sprite::ToPixels(HBITMAP BitmapHandle, int &width, int &height)
{
	BITMAP Bmp = { 0 };
	BITMAPINFO Info = { 0 };
	std::vector<unsigned char> Pixels = std::vector<unsigned char>();

	HDC DC = CreateCompatibleDC(NULL);
	std::memset(&Info, 0, sizeof(BITMAPINFO)); //not necessary really..
	HBITMAP OldBitmap = (HBITMAP)SelectObject(DC, BitmapHandle);
	GetObject(BitmapHandle, sizeof(Bmp), &Bmp);

	Info.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	Info.bmiHeader.biWidth = width = Bmp.bmWidth;
	Info.bmiHeader.biHeight = height = Bmp.bmHeight;
	Info.bmiHeader.biPlanes = 1;
	Info.bmiHeader.biBitCount = Bmp.bmBitsPixel;
	Info.bmiHeader.biCompression = BI_RGB;
	Info.bmiHeader.biSizeImage = ((width * Bmp.bmBitsPixel + 31) / 32) * 4 * height;

	Pixels.resize(Info.bmiHeader.biSizeImage);
	GetDIBits(DC, BitmapHandle, 0, height, &Pixels[0], &Info, DIB_RGB_COLORS);
	SelectObject(DC, OldBitmap);

	height = std::abs(height);
	DeleteDC(DC);
	return Pixels;
}

bool Sprite::isSamePixelColor(const COLORREF first_pixel, const COLORREF second_pixel) {
	COLORREF aux_first_pixel = first_pixel & 0xFFFFFF;
	COLORREF aux_second_pixel = second_pixel & 0xFFFFFF;
	return aux_first_pixel == aux_second_pixel;
}

//collision points de implementat

void Sprite::moveLeft(float speed) {
	mPosition.x -= speed;
}

void Sprite::moveRight(float speed) {
	mPosition.x += speed;
}

void Sprite::moveForward(float speed) {
	mPosition.y -= speed;
}

void Sprite::moveBackward(float speed) {
	mPosition.y += speed;
}

bool Sprite::isLeftSide() const {
	if (mPosition.x < mpBackBuffer->width() / 2) {
		return true;
	}
	return false;
}

bool Sprite::isRightSide() const {
	if (mPosition.x > mpBackBuffer->width() / 2) {
		return true;
	}
	return false;

}

bool Sprite::isTopSide() const {
	if (mPosition.y < mpBackBuffer->height() / 2) {
		return true;
	}
	return false;
}

bool Sprite::isBottomSide() const {
	if (mPosition.y > mpBackBuffer->height() / 2) {
		return true;
	}
	return false;
}

void Sprite::setToDelete(bool value) {
	toDelete = value;
}


void Sprite::UpdatePixelBuffer() {
	int image_width = width();
	int image_height = height();
	
	image_pixels.resize(0);
	image_pixels.resize(image_width* image_height,1);
	BITMAPINFO bitmapInfo = { 0 };
	bitmapInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitmapInfo.bmiHeader.biWidth = image_width;
	bitmapInfo.bmiHeader.biHeight = image_height;
	bitmapInfo.bmiHeader.biPlanes = 1;
	bitmapInfo.bmiHeader.biBitCount = 32;
	bitmapInfo.bmiHeader.biCompression = BI_RGB;
	bitmapInfo.bmiHeader.biSizeImage = 0;

	GetDIBits(mhSpriteDC, mhImage, 0, 0, NULL, &bitmapInfo, DIB_RGB_COLORS);

	if(GetDIBits(mhSpriteDC, mhImage, 0, image_height, &image_pixels[0], &bitmapInfo, DIB_RGB_COLORS) != image_height) {
		int a;
	}
}

void Sprite::UpdatePixelBufferNautre() {
	int image_width = imgBackground->Width();
	int image_height = imgBackground->Height();

	image_pixels.resize(0);
	image_pixels.resize(image_width* image_height, 1);
	BITMAPINFO bitmapInfo = { 0 };
	bitmapInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitmapInfo.bmiHeader.biWidth = image_width;
	bitmapInfo.bmiHeader.biHeight = -image_height;
	bitmapInfo.bmiHeader.biPlanes = 1;
	bitmapInfo.bmiHeader.biBitCount = 32;
	bitmapInfo.bmiHeader.biCompression = BI_RGB;
	bitmapInfo.bmiHeader.biSizeImage = 0;

	GetDIBits(mhSpriteDC, mhImage, 0, 0, NULL, &bitmapInfo, DIB_RGB_COLORS);

	if (GetDIBits(mhSpriteDC, mhImage, 0, image_height, &image_pixels[0], &bitmapInfo, DIB_RGB_COLORS) != image_height) {
		int a;
	}
}

bool Sprite::isSameColor(Vec2 pointToCheck, COLORREF colorToCheck) {
	int _width = imgBackground->Width();
	bool _height = imgBackground->Height();

	int PixelX = pointToCheck.x;
	int PixelY = pointToCheck.y;
	int pixelToCheck = _width * PixelY + PixelX;
	COLORREF pixelColor = image_pixels[pixelToCheck];
	return isSamePixelColor(pixelColor, colorToCheck);
}

bool Sprite::isCollisionWithNature(Sprite* natureSprite, COLORREF COLOR_TO_CHECK) {
	Vec2 topRightCorner = mAbsoluteCenterPosition - Vec2(width() / 2, height() / 2);
	
	for(int i = 0; i < collisionPointsRelative.size(); i++) {
		Vec2 pointToCheck = collisionPointsRelative[i];
		if (natureSprite->isSameColor(pointToCheck, COLOR_TO_CHECK)) {
			return true;
		}
	
	}
	return false;
}

bool Sprite::isToDelete() {
	return toDelete;
}


bool Sprite::isApexInsideSprite(const Sprite* other) const {
	bool width_collision = false;
	bool height_collision = false;

	int half_width_dimension = width() / 2;
	int half_height_dimension = height() / 2;

	if(mPosition.x + half_width_dimension >= other->mPosition.x + other->apex.x &&
		mPosition.x - half_width_dimension <= other->mPosition.x + other->apex.x) {
		width_collision = true;
	}

	if(mPosition.y - half_height_dimension <= other->mPosition.y - other->apex.y && 
		mPosition.y + half_height_dimension >= other->mPosition.y - other->apex.y) {
		height_collision = true;
	}
	return width_collision && height_collision;
}

bool Sprite::isPointInsideSprite(const Vec2 point) const {
	bool width_collision = false;
	bool height_collision = false;

	int half_width_dimension = width() / 2;
	int half_height_dimension = height() / 2;

	if (point.x >= mAbsoluteCenterPosition.x - half_width_dimension &&
		point.x <= mAbsoluteCenterPosition.x + half_width_dimension) {
		width_collision = true;
	}
	if (point.y >= mAbsoluteCenterPosition.y - half_height_dimension &&
		point.y <= mAbsoluteCenterPosition.y + half_height_dimension) {
		height_collision = true;
	}
	return width_collision && height_collision;
}

void Sprite::setCollisionPoints(vector<Vec2> collisionPoints) {
	this->collisionPoints = collisionPoints;
	updateCollisionPointsRelative();
}

bool Sprite::isCollisionWith(const Sprite* other) const
{	
	
	for(int i = 0; i < collisionPointsRelative.size(); i++) {
		if(other->isPointInsideSprite(collisionPointsRelative[i])) {

			int pixelToCheck = other->computePixelToCheckCollision(collisionPointsRelative[i]);
			COLORREF pixelToCheckColor = other->image_pixels[pixelToCheck];
			if(other->isBackgroundColor(pixelToCheckColor) == true) {
				return false;
			}
			else {
				return true;
			}
		}
	}
	return false;
}

bool Sprite::areCollisionPointsInsideOtherSprite(const Sprite* other) {
	for(int i = 0; i < collisionPoints.size(); i++) {
		if(true == other->isPointInsideSprite(collisionPoints[i])) {
			return true;
		}
	}
	return false;
}

int Sprite::computePixelToCheckCollision(Vec2 collision_point) const {
	int _width = width();
	int _height = height();

	
	Vec2 bottom_right_point_absolute(mAbsoluteCenterPosition.x + _width / 2, mAbsoluteCenterPosition.y + _height / 2);
	Vec2 pixel_to_check = bottom_right_point_absolute - collision_point;

	pixel_to_check.x = _width - pixel_to_check.x;
	pixel_to_check.y = _height - pixel_to_check.y;

	int PixelX = pixel_to_check.x;
	int PixelY = pixel_to_check.y;

	return (_height - PixelY - 1) * _width + PixelX;

}

vector<int> Sprite::computeCollisionPixels(const Sprite* other) {
	vector<int> collisionPixels;
	for(int i = 0; i < collisionPoints.size(); i++) {
		int pixel = other->computePixelToCheckCollision(collisionPoints[i]);
		collisionPixels.push_back(pixel);
	}
	return collisionPixels;
}

bool Sprite::isBackgroundColor(COLORREF pixel_color) const{
	int aux = pixel_color & 0xFFFFFF;
	return aux == mcTransparentColor;
}

bool Sprite::isPixelsSpriteCollision(const Sprite* other, vector<int> pixels) {
	for(int i = 0; i < pixels.size(); i++) {
		COLORREF pixelColor = other->image_pixels[pixels[i]];
		if (isBackgroundColor(pixelColor) == false) {
			return true;
		}
	}
	return false;
}



int Sprite::GetPixelToCheck(const Sprite* other) {

	int _width = width();
	int _height = height();
	
	Vec2 other_apex_relative = other->get_apex();
	
	other_apex_relative.x = other->mPosition.x + other_apex_relative.x;
	other_apex_relative.y = other->mPosition.y - other_apex_relative.y;
	
	Vec2 bottom_right_point_relative(mPosition.x + _width / 2, mPosition.y + _height / 2);
	Vec2 pixel_to_check = bottom_right_point_relative - other_apex_relative;
	
	pixel_to_check.x = _width - pixel_to_check.x;
	pixel_to_check.y = _height - pixel_to_check.y;
	
	int PixelX = pixel_to_check.x;
	int PixelY = pixel_to_check.y;
	
	return (_height - PixelY - 1) * _width + PixelX;
}

void Sprite::drawTransparent()
{
	if (mpBackBuffer == NULL)
		return;

	HDC hBackBuffer = mpBackBuffer->getDC();

	int w = width();
	int h = height();

	// Upper-left corner.
	int x = (int)mPosition.x - (w / 2);
	int y = (int)mPosition.y - (h / 2);

	COLORREF crOldBack = SetBkColor(hBackBuffer, RGB(255, 255, 255));
	COLORREF crOldText = SetTextColor(hBackBuffer, RGB(0, 0, 0));
	HDC dcImage, dcTrans;

	// Create two memory dcs for the image and the mask
	dcImage = CreateCompatibleDC(hBackBuffer);
	dcTrans = CreateCompatibleDC(hBackBuffer);

	// Select the image into the appropriate dc
	SelectObject(dcImage, mhImage);

	// Create the mask bitmap
	BITMAP bitmap;
	GetObject(mhImage, sizeof(BITMAP), &bitmap);
	HBITMAP bitmapTrans = CreateBitmap(bitmap.bmWidth, bitmap.bmHeight, 1, 1, NULL);

	// Select the mask bitmap into the appropriate dc
	SelectObject(dcTrans, bitmapTrans);

	// Build mask based on transparent color
	SetBkColor(dcImage, mcTransparentColor);
	BitBlt(dcTrans, 0, 0, bitmap.bmWidth, bitmap.bmHeight, dcImage, 0, 0, SRCCOPY);

	// Do the work - True Mask method - cool if not actual display
	BitBlt(hBackBuffer, x, y, bitmap.bmWidth, bitmap.bmHeight, dcImage, 0, 0, SRCINVERT);
	BitBlt(hBackBuffer, x, y, bitmap.bmWidth, bitmap.bmHeight, dcTrans, 0, 0, SRCAND);
	BitBlt(hBackBuffer, x, y, bitmap.bmWidth, bitmap.bmHeight, dcImage, 0, 0, SRCINVERT);

	// free memory	
	DeleteDC(dcImage);
	DeleteDC(dcTrans);
	DeleteObject(bitmapTrans);

	// Restore settings
	SetBkColor(hBackBuffer, crOldBack);
	SetTextColor(hBackBuffer, crOldText);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Sprite::computeVelocity() {
	mVelocity.x = MAX_SPEED * cos(DEG2RAD(rotationAngle));
	mVelocity.y = -MAX_SPEED * sin(DEG2RAD(rotationAngle));
}

void Sprite::computeVelocity(int angle, int maxSpeed) {
	mVelocity.x = maxSpeed * cos(DEG2RAD(angle));
	mVelocity.y = maxSpeed * sin(DEG2RAD(angle));
}

void Sprite::computeCenter() {

}

string Sprite::toString() {
	string result;

	result = to_string(int(mAbsoluteCenterPosition.x)) + " " + to_string(int(mAbsoluteCenterPosition.y)) + " " + to_string(rotationAngle) + "\n";

	return result;
}

void Sprite::inCollisionWithNature(Sprite* sprite) {
	int _width = 5760;//width();
	int _height = 3240;// height();
	int PixelX =  mAbsoluteCenterPosition.x;
	int PixelY =  mAbsoluteCenterPosition.y;

	int pixel = (_height - PixelY - 1) * _width + PixelX;
	COLORREF pixelToCheckCollision = sprite->image_pixels[pixel];
}

AnimatedSprite::AnimatedSprite(const char *szImageFile, const char *szMaskFile, const RECT& rcFirstFrame, int iFrameCount)
	: Sprite(szImageFile, szMaskFile)
{
	frameRect = rcFirstFrame;
	mptFrameCrop.x = rcFirstFrame.left;
	mptFrameCrop.y = rcFirstFrame.top;
	mptFrameStartCrop = mptFrameCrop;
	miFrameWidth = rcFirstFrame.right - rcFirstFrame.left;
	miFrameHeight = rcFirstFrame.bottom - rcFirstFrame.top;
	miFrameCount = iFrameCount;
}

AnimatedSprite::AnimatedSprite(const BackBuffer* backBuffer, CImageFile* imgBackground, const char* szImageFile, COLORREF crTransparentColor, const RECT& rcFirstFrame, int noOfRowFrames, int noOfColumnFrames, int rotation_angle)
	: Sprite(backBuffer, imgBackground, szImageFile, crTransparentColor){
	rotationAngle = rotation_angle;
	frameRect = rcFirstFrame;
	mptFrameCrop.x = rcFirstFrame.left;
	mptFrameCrop.y = rcFirstFrame.top;
	mptFrameStartCrop = mptFrameCrop;
	miFrameWidth = rcFirstFrame.right - rcFirstFrame.left;
	miFrameHeight = rcFirstFrame.bottom - rcFirstFrame.top;
	this->noOfRowFrames = noOfRowFrames;
	this->noOfColumnFrames = noOfColumnFrames;
	miFrameCount = noOfRowFrames * noOfColumnFrames;
	Rotate(rotationAngle);
}

AnimatedSprite::AnimatedSprite(const char* szImageFile, COLORREF crTransparentColor, const RECT& rcFirstFrame, int noOfRowFrames, int noOfColumnFrames, int rotation_angle)
	: Sprite(szImageFile, crTransparentColor) {
	rotationAngle = rotation_angle;
	frameRect = rcFirstFrame;
	mptFrameCrop.x = rcFirstFrame.left;
	mptFrameCrop.y = rcFirstFrame.top;
	mptFrameStartCrop = mptFrameCrop;
	miFrameWidth = rcFirstFrame.right - rcFirstFrame.left;
	miFrameHeight = rcFirstFrame.bottom - rcFirstFrame.top;
	this->noOfRowFrames = noOfRowFrames;
	this->noOfColumnFrames = noOfColumnFrames;
	miFrameCount = noOfRowFrames * noOfColumnFrames;
	Rotate(rotationAngle);
}

AnimatedSprite::AnimatedSprite(const char* szImageFile, COLORREF crTransparentColor, const RECT& rcFirstFrame, int iFrameCount)
	: Sprite(szImageFile, crTransparentColor) {
		frameRect = rcFirstFrame;
		mptFrameCrop.x = rcFirstFrame.left;
		mptFrameCrop.y = rcFirstFrame.top;
		mptFrameStartCrop = mptFrameCrop;
		miFrameWidth = rcFirstFrame.right - rcFirstFrame.left;
		miFrameHeight = rcFirstFrame.bottom - rcFirstFrame.top;
		noOfRowFrames = 4;
		noOfColumnFrames = 4;
		miFrameCount = noOfRowFrames * noOfColumnFrames;
		

		mWidth = miFrameWidth;
		mHeight = miFrameHeight;
}



void AnimatedSprite::SetFrame(int iIndex)
{
	// index must be in range
	assert(iIndex >= 0 && iIndex < miFrameCount && "AnimatedSprite frame Index must be in range!");
	

	int rand = iIndex / noOfColumnFrames;
	int coloana = iIndex % noOfColumnFrames;
	mptFrameCrop.x = mptFrameStartCrop.x + coloana*miFrameWidth;
	mptFrameCrop.y = mptFrameStartCrop.y + rand*miFrameHeight;

	Rotate(rotationAngle);

}


bool Sprite::isInsideFrame() {
	Vec2 backgroundOffset = imgBackground->mOffset;
	if (mAbsoluteCenterPosition.x - backgroundOffset.x - width() / 2 < mpBackBuffer->width() &&
		mAbsoluteCenterPosition.y - backgroundOffset.y - height() / 2< mpBackBuffer->height()) {
		return true;
	}
	return false;
}
bool Sprite::isInsideFrame(Vec2 backgroundOffset) {
	if(mAbsoluteCenterPosition.x - backgroundOffset.x - width() / 2 < mpBackBuffer->width() &&
		mAbsoluteCenterPosition.y - backgroundOffset.y - height() / 2< mpBackBuffer->height()) {
		return true;
	}
	return false;
}

void Sprite::computeRelativePosition(const Vec2& backgroundOffset) {
	mPosition = mAbsoluteCenterPosition - backgroundOffset;
}

void Sprite::computeRelativePosition() {
	mPosition = mAbsoluteCenterPosition - imgBackground->mOffset;
}

void Sprite::keepInsideFrame() {
	int halfWidth = width() / 2;
	int halfHeight = height() / 2;
	if (mPosition.x <= halfWidth) {
		mPosition.x = halfWidth;
	}
	if (mPosition.y <= halfHeight) {
		mPosition.y = halfHeight;
	}
	if(mPosition.x >= mpBackBuffer->width() - halfWidth) {
		mPosition.x = mpBackBuffer->width() - halfWidth;
	}
	if(mPosition.y >= mpBackBuffer->height() - halfHeight) {
		mPosition.y = mpBackBuffer->height() - halfHeight;
	}
}

HBITMAP Sprite::GetRotatedBitmapNT(HBITMAP hBitmap, float radians, COLORREF clrBack)
{
	// Create a memory DC compatible with the display
	CDC sourceDC, destDC;
	sourceDC.CreateCompatibleDC(NULL);
	destDC.CreateCompatibleDC(NULL);

	// Get logical coordinates
	BITMAP bm;
	::GetObject(hBitmap, sizeof(bm), &bm);

	float cosine = (float)cos(radians);
	float sine = (float)sin(radians);

	// Compute dimensions of the resulting bitmap
	// First get the coordinates of the 3 corners other than origin
	int x1 = (int)(bm.bmHeight * sine);
	int y1 = (int)(bm.bmHeight * cosine);
	int x2 = (int)(bm.bmWidth * cosine + bm.bmHeight * sine);
	int y2 = (int)(bm.bmHeight * cosine - bm.bmWidth * sine);
	int x3 = (int)(bm.bmWidth * cosine);
	int y3 = (int)(-bm.bmWidth * sine);

	int minx = min(0, min(x1, min(x2, x3)));
	int miny = min(0, min(y1, min(y2, y3)));
	int maxx = max(0, max(x1, max(x2, x3)));
	int maxy = max(0, max(y1, max(y2, y3)));

	int w = maxx - minx;
	int h = maxy - miny;

	// Create a bitmap to hold the result
	HBITMAP hbmResult = ::CreateCompatibleBitmap(CClientDC(NULL), w, h);

	HBITMAP hbmOldSource = (HBITMAP)::SelectObject(sourceDC.m_hDC, hBitmap);
	HBITMAP hbmOldDest = (HBITMAP)::SelectObject(destDC.m_hDC, hbmResult);

	// Draw the background color before we change mapping mode
	HBRUSH hbrBack = CreateSolidBrush(clrBack);
	HBRUSH hbrOld = (HBRUSH)::SelectObject(destDC.m_hDC, hbrBack);
	destDC.PatBlt(0, 0, w, h, PATCOPY);
	::DeleteObject(::SelectObject(destDC.m_hDC, hbrOld));

	// We will use world transform to rotate the bitmap
	SetGraphicsMode(destDC.m_hDC, GM_ADVANCED);
	XFORM xform;
	xform.eM11 = cosine;
	xform.eM12 = -sine;
	xform.eM21 = sine;
	xform.eM22 = cosine;
	xform.eDx = (float)-minx;
	xform.eDy = (float)-miny;

	SetWorldTransform(destDC.m_hDC, &xform);

	// Now do the actual rotating - a pixel at a time
	destDC.BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &sourceDC, 0, 0, SRCCOPY);

	// Restore DCs
	::SelectObject(sourceDC.m_hDC, hbmOldSource);
	::SelectObject(destDC.m_hDC, hbmOldDest);

	return hbmResult;
}

void Sprite::updateCollisionPointsRelative() {
	collisionPointsRelative.resize(0);
	for(int i = 0; i < collisionPoints.size();i++) {
		Vec2 point = collisionPoints[i];
		point = point.Rotate(DEG2RAD(rotationAngle));
		point += mAbsoluteCenterPosition;
		collisionPointsRelative.push_back(point);
	}
}

void Sprite::Rotate(int angle) {
	rotationAngle = angle;
	mhImage = GetRotatedBitmapNT(auxmhImage, DEG2RAD(changeAngleSystemCoordonites(rotationAngle)), mcTransparentColor);
	GetObject(mhImage, sizeof(BITMAP), &mImageBM);
	UpdatePixelBuffer();
	updateCollisionPointsRelative();
	int raza = width() / 2;
	double radians = degreeToRadians(angle);
	Vec2 plane_apex = Polar(raza, radians);

	set_apex(plane_apex);
}

void Sprite::Rotate(float radians, const char* szImageFile)
{
	// Create a memory DC compatible with the display
	HDC sourceDC, destDC;
	sourceDC = CreateCompatibleDC(mpBackBuffer->getDC());
	destDC = CreateCompatibleDC(mpBackBuffer->getDC());

	// Get logical coordinates
	HBITMAP hBitmap = (HBITMAP)LoadImage(g_hInst, szImageFile, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION | LR_LOADFROMFILE);
	BITMAP bm;
	::GetObject(hBitmap, sizeof(bm), &bm);

	float cosine = (float)cos(radians);
	float sine = (float)sin(radians);

	// Compute dimensions of the resulting bitmap
	// First get the coordinates of the 3 corners other than origin
	int x1 = (int)(bm.bmHeight * sine);
	int y1 = (int)(bm.bmHeight * cosine);
	int x2 = (int)(bm.bmWidth * cosine + bm.bmHeight * sine);
	int y2 = (int)(bm.bmHeight * cosine - bm.bmWidth * sine);
	int x3 = (int)(bm.bmWidth * cosine);
	int y3 = (int)(-bm.bmWidth * sine);

	int minx = min(0, min(x1, min(x2, x3)));
	int miny = min(0, min(y1, min(y2, y3)));
	int maxx = max(0, max(x1, max(x2, x3)));
	int maxy = max(0, max(y1, max(y2, y3)));

	int w = maxx - minx;
	int h = maxy - miny;

	// Create a bitmap to hold the result
	HBITMAP hbmResult = ::CreateCompatibleBitmap(mpBackBuffer->getDC(), w, h);

	HBITMAP hbmOldSource = (HBITMAP)::SelectObject(sourceDC, hBitmap);
	HBITMAP hbmOldDest = (HBITMAP)::SelectObject(destDC, hbmResult);

	// Draw the background color before we change mapping mode
	HBRUSH hbrBack = CreateSolidBrush(mcTransparentColor);
	HBRUSH hbrOld = (HBRUSH)::SelectObject(destDC, hbrBack);
	PatBlt(destDC, 0, 0, w, h, PATCOPY);
	::DeleteObject(::SelectObject(destDC, hbrOld));

	// We will use world transform to rotate the bitmap
	SetGraphicsMode(destDC, GM_ADVANCED);
	XFORM xform;
	xform.eM11 = cosine;
	xform.eM12 = -sine;
	xform.eM21 = sine;
	xform.eM22 = cosine;
	xform.eDx = (float)-minx;
	xform.eDy = (float)-miny;

	SetWorldTransform(destDC, &xform);

	// Now do the actual rotating - a pixel at a time
	BitBlt(destDC, 0, 0, w, h, sourceDC, 0, 0, SRCCOPY);
	// Restore DCs
	::SelectObject(sourceDC, hbmOldSource);
	::SelectObject(destDC, hbmOldDest);

	BITMAP btm;
	::GetObject(hbmResult, sizeof(mImageBM), &mImageBM);

}

int Sprite::changeAngleSystemCoordonites(int angle) {
	return 360 - angle;
}

void AnimatedSprite::Rotate(int angle) {
	rotationAngle = angle;
	
	CropFrame();
	mhImage = GetRotatedBitmapNT(croppedFrame, DEG2RAD(changeAngleSystemCoordonites(rotationAngle)), mcTransparentColor);
	GetObject(mhImage, sizeof(BITMAP), &mImageBM);
	
	UpdatePixelBuffer();
	updateCollisionPointsRelative();
}

void AnimatedSprite::CropFrame()
{


	HDC hSrc = CreateCompatibleDC(NULL);
	SelectObject(hSrc, auxmhImage);


	HDC hNew = CreateCompatibleDC(hSrc);

	HBITMAP hBmp = CreateCompatibleBitmap(hSrc, miFrameWidth, miFrameHeight);
	HBITMAP hOld = (HBITMAP)SelectObject(hNew, hBmp);

	bool retVal = (BitBlt(hNew, 0, 0, miFrameWidth, miFrameHeight, hSrc, mptFrameCrop.x, mptFrameCrop.y, SRCCOPY)) ? true : false;

	if (retVal) {
		int a = 10;
		a++;
	}
	SelectObject(hNew, hOld);

	DeleteDC(hSrc);
	DeleteDC(hNew);

	DeleteObject(croppedFrame);

	croppedFrame = hBmp;
}

istream & operator >> (istream & in, Sprite* sprite) {
	in >> sprite->mPosition.x >> sprite->mPosition.y >> sprite->rotationAngle;
	sprite->computeVelocity();
	sprite->computeCenter();
	sprite->Rotate(sprite->rotationAngle);
	return in;
}