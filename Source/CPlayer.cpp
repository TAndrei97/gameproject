//-----------------------------------------------------------------------------
// File: CPlayer.cpp
//
// Desc: This file stores the player object class. This class performs tasks
//       such as player movement, some minor physics as well as rendering.
//
// Original design by Adam Hoult & Gary Simmons. Modified by Mihai Popescu.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// CPlayer Specific Includes
//-----------------------------------------------------------------------------
#include "CPlayer.h"
#include <iostream>

//-----------------------------------------------------------------------------
// Name : CPlayer () (Constructor)
// Desc : CPlayer Class Constructor
//-----------------------------------------------------------------------------
CPlayer::CPlayer(const BackBuffer *pBackBuffer, const char* planeFileName, const char *aBulletFileName)
{
	score = 0;
	lives = MAX_LIVES;
	backBuffer = pBackBuffer;
	strcpy_s(bulletFileName, MAX_PATH, aBulletFileName);
	angle = 90;
	m_pSprite = new Sprite(planeFileName, RGB(255, 0, 255), angle);
	m_pSprite->setBackBuffer(pBackBuffer);
	m_eSpeedState = SPEED_STOP;
	m_fTimer = 0;
	center.x = m_pSprite->width() / 2;
	center.y = m_pSprite->height() / 2;
	m_pSprite->UpdatePixelBuffer();

	// Animation frame crop rectangle
	RECT r;
	r.left = 0;
	r.top = 0;
	r.right = 128;
	r.bottom = 128;
	m_pExplosionSprite = new AnimatedSprite("data/explosion.bmp", "data/explosionmask.bmp", r, 16);
	m_pExplosionSprite->setBackBuffer(pBackBuffer);
	m_bExplosion = false;
	m_iExplosionFrame = 1;

	for (int i = 0; i < MAX_LIVES; i++) {
		Sprite* liveSprite = new Sprite(LIVES_PATH[0].c_str(), BACKGROUND_TRANSPARENT_COLOR);
		liveSprite->mPosition = Vec2(backBuffer->width() - 50 * i - liveSprite->width(), liveSprite->height());
		liveSprite->setBackBuffer(backBuffer);
		lives_sprite_vector.push_back(liveSprite);

	}
	for (int i = 0; i < SCORE_NO_DIGITS; i++) {
		Sprite*	score_sprite = new Sprite(SCORE_DIGITS_PATHS[0].c_str(), BACKGROUND_TRANSPARENT_COLOR);
		score_sprite->setBackBuffer(backBuffer);
		score_sprite->mPosition = Vec2(score_sprite->width() * (SCORE_NO_DIGITS - i), score_sprite->height());
		score_sprites_vector.push_back(score_sprite);
	}
}

//-----------------------------------------------------------------------------
// Name : ~CPlayer () (Destructor)
// Desc : CPlayer Class Destructor
//-----------------------------------------------------------------------------
CPlayer::~CPlayer()
{
	delete m_pSprite;
	delete m_pExplosionSprite;
}

Sprite* CPlayer::CreateBulletSprite(int angle = 0) {
	Sprite* m_bulletSprite = new Sprite(bulletFileName, RGB(0xff, 0x00, 0xff), angle);
	m_bulletSprite->setBackBuffer(backBuffer);
	Vec2 plane_apex = m_pSprite->get_apex();

	m_bulletSprite->mPosition.x = m_pSprite->mPosition.x + plane_apex.x;
	m_bulletSprite->mPosition.y = m_pSprite->mPosition.y - plane_apex.y;

	m_bulletSprite->computeVelocity();
	return m_bulletSprite;
}

void CPlayer::Shoot() {
	Sprite* m_bulletSprite = CreateBulletSprite(angle);
	m_bulletSpriteVector.push_back(m_bulletSprite);
}

void CPlayer::Update(float dt)
{
	// Update sprite
	m_pSprite->update(dt);

	for (int i = 0; i < m_bulletSpriteVector.size(); i++) {
		m_bulletSpriteVector[i]->update(dt);
	}
	// Get velocity
	double v = m_pSprite->mVelocity.Magnitude();

	// NOTE: for each async sound played Windows creates a thread for you
	// but only one, so you cannot play multiple sounds at once.
	// This creation/destruction of threads also leads to bad performance
	// so this method is not recommanded to be used in complex projects.

	// update internal time counter used in sound handling (not to overlap sounds)
	m_fTimer += dt;

	// A FSM is used for sound manager 
	switch (m_eSpeedState)
	{
	case SPEED_STOP:
		if (v > 35.0f)
		{
			m_eSpeedState = SPEED_START;
			PlaySound("data/jet-start.wav", NULL, SND_FILENAME | SND_ASYNC);
			m_fTimer = 0;
		}
		break;
	case SPEED_START:
		if (v < 25.0f)
		{
			m_eSpeedState = SPEED_STOP;
			PlaySound("data/jet-stop.wav", NULL, SND_FILENAME | SND_ASYNC);
			m_fTimer = 0;
		}
		else
			if (m_fTimer > 1.f)
			{
				PlaySound("data/jet-cabin.wav", NULL, SND_FILENAME | SND_ASYNC);
				m_fTimer = 0;
			}
		break;
	}

	// NOTE: For sound you also can use MIDI but it's Win32 API it is a bit hard
	// see msdn reference: http://msdn.microsoft.com/en-us/library/ms711640.aspx
	// In this case you can use a C++ wrapper for it. See the following article:
	// http://www.codeproject.com/KB/audio-video/midiwrapper.aspx (with code also)
}

void CPlayer::deleteBullets() {
	m_bulletSpriteVector.erase(m_bulletSpriteVector.begin(), m_bulletSpriteVector.end());
}

void CPlayer::DrawLives() {
	for (int i = 0; i < MAX_LIVES; i++) {
		lives_sprite_vector[i]->draw();
	}
}

void CPlayer::DrawScore() {
	for (int i = 0; i < SCORE_NO_DIGITS; i++)
		score_sprites_vector[i]->draw();
}

void CPlayer::SetLivesSide(int side) {
	for (int i = 0; i < MAX_LIVES; i++) {
		lives_sprite_vector[i]->mPosition = Vec2(backBuffer->width() / 2 + side * (backBuffer->width() / 2 - lives_sprite_vector[i]->width()* (i + 1)), lives_sprite_vector[i]->height());
	}
	for (int i = 0; i < SCORE_NO_DIGITS; i++) {
		score_sprites_vector[i]->mPosition = Vec2(backBuffer->width() / 2 + side * (backBuffer->width() / 2 - score_sprites_vector[i]->width()* (i + 1)), lives_sprite_vector[i]->height() * 3 / 2 + score_sprites_vector[i]->height());
	}

}

void CPlayer::Draw()
{
	if (!m_bExplosion) {
		m_pSprite->draw();
		for (int i = 0; i < m_bulletSpriteVector.size(); i++) {
			m_bulletSpriteVector[i]->draw();

			if (m_bulletSpriteVector[i]->mPosition.y + m_bulletSpriteVector[i]->height() / 2 <= 0)
				m_bulletSpriteVector.erase(m_bulletSpriteVector.begin() + i);
		}


	}
	else
		m_pExplosionSprite->draw();
}

bool CPlayer::isInsideTheFrame() const {
	if (m_pSprite->mPosition.x <= center.x || m_pSprite->mPosition.x + center.x >= m_pSprite->buffer_width())
		return false;

	if (m_pSprite->mPosition.y <= center.y || m_pSprite->mPosition.y + center.y >= m_pSprite->buffer_height())
		return false;

	return true;
}



void CPlayer::decreaseLives() {
	lives--;
}

void CPlayer::updateLives() {
	for (int i = 0; i < MAX_LIVES - lives; i++) {
		Sprite* missing_live_sprite = new Sprite(LIVES_PATH[1].c_str(), RGB(255, 0, 255));

		missing_live_sprite->setBackBuffer(backBuffer);
		missing_live_sprite->mPosition = lives_sprite_vector[i]->mPosition;
		lives_sprite_vector[i] = missing_live_sprite;
	}

}

void CPlayer::updateScoreSprites() {
	int copy_score = score;
	for (int i = 0; i < SCORE_NO_DIGITS; i++) {
		int digit = copy_score % 10;
		copy_score /= 10;
		Sprite* digit_sprite = new Sprite(SCORE_DIGITS_PATHS[digit].c_str(), BACKGROUND_TRANSPARENT_COLOR);
		digit_sprite->setBackBuffer(backBuffer);
		digit_sprite->mPosition = score_sprites_vector[i]->mPosition;
		score_sprites_vector[i] = digit_sprite;
	}
}

void CPlayer::hittedSomething() {
	increaseScore(SCORE_INCREASE);
	updateScoreSprites();
}

void CPlayer::wasHitted() {
	decreaseLives();
	updateLives();
}

string CPlayer::toString() {
	string result = "";
	m_pSprite->set_angle(angle);
	result += m_pSprite->toString();
	result += to_string(lives) + " " + to_string(score) + "\n";
	result += to_string(m_bulletSpriteVector.size()) + "\n";
	for (int i = 0; i < m_bulletSpriteVector.size(); i++) {
		result += m_bulletSpriteVector[i]->toString();
	}

	return result;
}


void CPlayer::Move(ULONG ulDirection)
{

	if (m_pSprite->mPosition.x <= center.x || m_pSprite->mPosition.x + center.x >= m_pSprite->buffer_width())
		m_pSprite->mVelocity.x = 0;

	if (m_pSprite->mPosition.y <= center.y || m_pSprite->mPosition.y + center.y >= m_pSprite->buffer_height())
		m_pSprite->mVelocity.y = 0;

	if (ulDirection & CPlayer::DIR_LEFT)
		m_pSprite->mVelocity.x -= 1;


	if (ulDirection & CPlayer::DIR_RIGHT)
		m_pSprite->mVelocity.x += 1;

	if (ulDirection & CPlayer::DIR_FORWARD)
		m_pSprite->mVelocity.y -= 1;

	if (ulDirection & CPlayer::DIR_BACKWARD)
		m_pSprite->mVelocity.y += 1;

	//AllocConsole();
	//freopen("conin$", "r", stdin);
	//freopen("conout$", "w", stdout);
	//freopen("conout$", "w", stderr);
}


Vec2& CPlayer::Position()
{
	return m_pSprite->mPosition;
}

Vec2& CPlayer::Velocity()
{
	return m_pSprite->mVelocity;
}

void CPlayer::Explode()
{
	m_pExplosionSprite->mPosition = m_pSprite->mPosition;
	m_pExplosionSprite->SetFrame(0);
	PlaySound("data/explosion.wav", NULL, SND_FILENAME | SND_ASYNC);
	m_bExplosion = true;
}

bool CPlayer::AdvanceExplosion()
{
	if (m_bExplosion)
	{
		m_pExplosionSprite->SetFrame(m_iExplosionFrame++);
		if (m_iExplosionFrame == m_pExplosionSprite->GetFrameCount())
		{
			m_bExplosion = false;
			m_iExplosionFrame = 0;
			m_pSprite->mVelocity = Vec2(0, 0);
			m_eSpeedState = SPEED_STOP;
			return false;
		}
	}
	return true;
}

bool CPlayer::checkCollision(vector<Sprite*> bullets) {
	for (int i = 0; i < bullets.size(); i++) {

	}
	return false;
}

void CPlayer::Rotate(int angle) {
	m_pSprite->Rotate(angle);
	center.x = m_pSprite->width() / 2;
	center.y = m_pSprite->height() / 2;
}

void CPlayer::RotateLeft() {
	angle += ANGLE_ROTATION;
	m_pSprite->set_angle(angle);
	angle %= 360;
	Rotate(angle);
}

void CPlayer::RotateRight() {
	angle -= ANGLE_ROTATION;
	m_pSprite->set_angle(angle);
	if (angle < 0)
		angle += 360;
	Rotate(angle);
}

istream & operator >> (istream & in, CPlayer* cplayer) {

	in >> cplayer->m_pSprite >> cplayer->lives >> cplayer->score;
	cplayer->updateLives();
	cplayer->updateScoreSprites();
	cplayer->angle = cplayer->m_pSprite->get_angle();
	cplayer->m_pSprite->mVelocity = Vec2(0, 0);
	int no_of_bullets;
	in >> no_of_bullets;
	cplayer->m_bulletSpriteVector.resize(0);
	for (int i = 0; i < no_of_bullets; i++) {
		Sprite* bullet = cplayer->CreateBulletSprite();
		in >> bullet;
		cplayer->m_bulletSpriteVector.push_back(bullet);
	}
	return in;
}