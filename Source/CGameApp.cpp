//-----------------------------------------------------------------------------
// File: CGameApp.cpp
//
// Desc: Game Application class, this is the central hub for all app processing
//
// Original design by Adam Hoult & Gary Simmons. Modified by Mihai Popescu.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// CGameApp Specific Includes
//-----------------------------------------------------------------------------
#include "CGameApp.h"
#include "Car.h"
#include "PoliceCar.h"
#include "GroupOfPlayers.h"

extern HINSTANCE g_hInst;
//extern int		 IDCONTINUEGAME;
using namespace std;
//-----------------------------------------------------------------------------
// CGameApp Member Functions
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// Name : CGameApp () (Constructor)
// Desc : CGameApp Class Constructor
//-----------------------------------------------------------------------------
CGameApp::CGameApp()
{
	// Reset / Clear all required values
	m_hWnd			= NULL;
	m_hIcon			= NULL;
	m_hMenu			= NULL;
	m_pBBuffer		= NULL;
	m_LastFrameRate = 0;
	m_imgBackground = new CImageFile;
}

//-----------------------------------------------------------------------------
// Name : ~CGameApp () (Destructor)
// Desc : CGameApp Class Destructor
//-----------------------------------------------------------------------------
CGameApp::~CGameApp()
{
	// Shut the engine down
	ShutDown();
}

int CGameApp::getWeaponIDFromShopMenu() {
	for(int i = 1; i < MAX_WEAPONS; i++) {
		string output_message = "Buy weapon" + to_string(i);
		int option = MessageBox(m_hWnd, _T(output_message.c_str()), _T("SHOP"), MB_YESNO | MB_ICONQUESTION);
		if(option == IDYES) {
			return i;
		}
	}
	return -1;
}

//-----------------------------------------------------------------------------
// Name : InitInstance ()
// Desc : Initialises the entire Engine here.
//-----------------------------------------------------------------------------
bool CGameApp::InitInstance( LPCTSTR lpCmdLine, int iCmdShow )
{

	// Create the primary display device
	if (!CreateDisplay()) { ShutDown(); return false; }
	if (!BuildObjects()) 
	{ 
		MessageBox( 0, _T("Failed to initialize properly. Reinstalling the application may solve this problem.\nIf the problem persists, please contact technical support."), _T("Fatal Error"), MB_OK | MB_ICONSTOP);
		ShutDown(); 
		return false; 
	}

	// Set up all required game states

	// Success!
	return true;
}

//-----------------------------------------------------------------------------
// Name : CreateDisplay ()
// Desc : Create the display windows, devices etc, ready for rendering.
//-----------------------------------------------------------------------------
bool CGameApp::CreateDisplay()
{
	LPTSTR			WindowTitle		= _T("GameFramework");
	LPCSTR			WindowClass		= _T("GameFramework_Class");
	USHORT			Width			= 1366;
	USHORT			Height			= 730;
	RECT			rc;
	WNDCLASSEX		wcex			= {0};

	
	wcex.cbSize			= sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= CGameApp::StaticWndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= g_hInst;
	wcex.hIcon			= LoadIcon(g_hInst, MAKEINTRESOURCE(IDI_ICON));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= 0;
	wcex.lpszClassName	= WindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON));

	if(atom == 0) {
		atom = RegisterClassEx(&wcex);
	}
	
	if(atom == 0)
		return false;

	// Retrieve the final client size of the window
	::GetClientRect( m_hWnd, &rc );
	m_nViewX		= rc.left;
	m_nViewY		= rc.top;
	m_nViewWidth	= rc.right - rc.left;
	m_nViewHeight	= rc.bottom - rc.top;
	
	
	m_hWnd = CreateWindow(WindowClass, 0, WS_BORDER,
		0, 0, Width, Height, NULL, NULL, g_hInst, this);


	SetWindowLong(m_hWnd, GWL_STYLE, 0);


	if (!m_hWnd)
		return false;


	// Show the window
	// aici mazimizeaza SW_NORMAL
	ShowWindow(m_hWnd, SW_MAXIMIZE);

	// Success!!
	return true;
}

//-----------------------------------------------------------------------------
// Name : BeginGame ()
// Desc : Signals the beginning of the physical post-initialisation stage.
//		From here on, the game engine has control over processing.
//-----------------------------------------------------------------------------

//void CGameApp::SelectGameMode() {
//	string output_message = "Select mode\nSingle player = YES\nMultiplayer = NO";
//	int option = MessageBox(m_hWnd, _T(output_message.c_str()), _T("Game Over"), MB_YESNO | MB_ICONQUESTION);
//	if (option == IDYES) {
//		SINGLE_PLAYER = 1;
//	}
//	else  if (option == IDNO) {
//		SINGLE_PLAYER = 0;
//	}
//}

int CGameApp::BeginGame()
{
	MSG		msg;
	int		restart = IDCONTINUEGAME;
	// Start main loop
	
	while(restart == IDCONTINUEGAME)
	{
		// Did we recieve a message, or are we idling ?
		if ( PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) ) 
		{
			if (msg.message == WM_QUIT) break;
			TranslateMessage( &msg );
			DispatchMessage ( &msg );
		} 
		else 
		{
			// Advance Game Frame.
			restart = FrameAdvance();

		} // End If messages waiting
	
	} // Until quit message is receieved

	return restart;
}

//-----------------------------------------------------------------------------
// Name : ShutDown ()
// Desc : Shuts down the game engine, and frees up all resources.
//-----------------------------------------------------------------------------
bool CGameApp::ShutDown()
{
	// Release any previously built objects
	ReleaseObjects ( );
	
	// Destroy menu, it may not be attached
	if ( m_hMenu ) DestroyMenu( m_hMenu );
	m_hMenu		 = NULL;

	// Destroy the render window
	SetMenu( m_hWnd, NULL );
	if ( m_hWnd ) DestroyWindow( m_hWnd );
	m_hWnd		  = NULL;
	
	// Shutdown Success
	return true;
}

//-----------------------------------------------------------------------------
// Name : StaticWndProc () (Static Callback)
// Desc : This is the main messge pump for ALL display devices, it captures
//		the appropriate messages, and routes them through to the application
//		class for which it was intended, therefore giving full class access.
// Note : It is VITALLY important that you should pass your 'this' pointer to
//		the lpParam parameter of the CreateWindow function if you wish to be
//		able to pass messages back to that app object.
//-----------------------------------------------------------------------------
LRESULT CALLBACK CGameApp::StaticWndProc(HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	// If this is a create message, trap the 'this' pointer passed in and store it within the window.
	if ( Message == WM_CREATE ) SetWindowLong( hWnd, GWL_USERDATA, (LONG)((CREATESTRUCT FAR *)lParam)->lpCreateParams);

	// Obtain the correct destination for this message
	CGameApp *Destination = (CGameApp*)GetWindowLong( hWnd, GWL_USERDATA );
	
	// If the hWnd has a related class, pass it through
	if (Destination) return Destination->DisplayWndProc( hWnd, Message, wParam, lParam );
	
	// No destination found, defer to system...
	return DefWindowProc( hWnd, Message, wParam, lParam );
}

//-----------------------------------------------------------------------------
// Name : DisplayWndProc ()
// Desc : The display devices internal WndProc function. All messages being
//		passed to this function are relative to the window it owns.
//-----------------------------------------------------------------------------
LRESULT CGameApp::DisplayWndProc( HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam )
{
	static UINT			fTimer;	

	// Determine message type
	switch (Message)
	{
		case WM_CREATE:
			break;
		
		case WM_CLOSE:
			PostQuitMessage(0);
			break;
		
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		
		case WM_SIZE:
			if ( wParam == SIZE_MAXIMIZED)
			{
				// App is inactive
				m_bActive = true;

				m_nViewWidth = LOWORD(lParam);
				m_nViewHeight = HIWORD(lParam);
			} // App has been minimized
			else
			{
				// App is active
				m_bActive = true;

				// Store new viewport sizes
				m_nViewWidth  = LOWORD( lParam );
				m_nViewHeight = HIWORD( lParam );
				
			
			} // End if !Minimized

			break;

		case WM_LBUTTONDOWN:
			// Capture the mouse
			SetCapture( m_hWnd );
			GetCursorPos( &m_OldCursorPos );
			break;

		case WM_LBUTTONUP:
			// Release the mouse
			ReleaseCapture( );
			break;

		case WM_KEYDOWN:
			switch(wParam)
			{
			case VK_ESCAPE:
				PostQuitMessage(0);
				break;
			case 0x55:
				fTimer = SetTimer(m_hWnd, 1, 250, NULL);
				Save();
				break;
			case 0x49:
				fTimer = SetTimer(m_hWnd, 1, 250, NULL);
				Load();
				break;
			case 0x52: //r
				player->changeWeaponUp();
				break;
			case 0x46: //f
				player->changeWeaponDown();
				break;
			case 0x43: //c
				player->endDrive(cars);
				break;
			case 0x58: //x
				player->drive(cars);
				break;
			case 0x54: //t
				player->rotateAntiClockwise();
				break;
			case 0x59: //y
				player->rotateClockwise();
				break;
			case 0x42: //b
				if (player->isInShopZone()) {
					player->buyWeapon(getWeaponIDFromShopMenu());
				}
				break;
			case VK_SPACE:				
				player->shoot();
				fTimer = SetTimer(m_hWnd, 1, 50, NULL);
				break;
			}
			break;	

		case WM_TIMER:
			break;

		case WM_COMMAND:
			break;

		default:
			return DefWindowProc(hWnd, Message, wParam, lParam);

	} // End Message Switch
	
	return 0;
}

//-----------------------------------------------------------------------------
// Name : BuildObjects ()
// Desc : Build our demonstration meshes, and the objects that instance them
//-----------------------------------------------------------------------------
bool CGameApp::BuildObjects()
{
	if (!m_imgBackground->LoadBitmapFromFile(BACKGROUND_FILE.c_str(), GetDC(m_hWnd)))
		return false;
	
	
	
	m_pBBuffer = new BackBuffer(m_hWnd, m_nViewWidth, m_nViewHeight);
	builder = new Builders(m_pBBuffer, m_imgBackground);
	nature = builder->buildNature();
	collisionManager = new CollisionManagerTest(nature);
	player = builder->buildPlayer();
	player->Position() = Vec2(330, 330);


	hud = builder->buildHUD(player);
	hud->setPosition(Vec2(700, 50));
	


	humans = new GroupOfHumans(m_imgBackground, collisionManager, builder);
	cars = new GroupOfCars(m_imgBackground, collisionManager, builder);
	policeCars = new GroupOfPoliceCars(m_imgBackground, collisionManager, builder);
	players = new GroupOfPlayers(builder);

	Car* car = builder->buildCar();
	car->absoluteCenterPosition() = Vec2(1000, 200);
	vector<Vec2> collisionPoints;
	collisionPoints.push_back(Vec2(94, 40));
	car->setCollisionPoints(collisionPoints);
	cars->addNewCar(car);

	builder = new Builders(m_pBBuffer, m_imgBackground);

	policeCar = builder->buildPoliceCar();
	policeCar->setAbsolutePosition(Vec2(600, 600));
	policeCars->addNewCar(policeCar);
	policeCar->setCollisionPoints(collisionPoints);
	miniMap = new MiniMap(m_pBBuffer, MINI_MAP_FILE_NAME.c_str(), PLAYER_MINI_MAP_FILE_NAME.c_str(), m_imgBackground);
	miniMap->placement();

	Human* human = builder->buildHuman();
	human->setAbsolutePosition(Vec2(400, 500));
	humans->add(human);
	player->setPoliceCars(policeCars);
	players->add(player);

	collisionManager->add(players);
	collisionManager->add(humans);
	collisionManager->add(cars);
	collisionManager->add(policeCars);
	
	//// Success!
	return true;
}

//-----------------------------------------------------------------------------
// Name : SetupGameState ()
// Desc : Sets up all the initial states required by the game.
//-----------------------------------------------------------------------------
void CGameApp::SetupGameState()
{
}

//-----------------------------------------------------------------------------
// Name : ReleaseObjects ()
// Desc : Releases our objects and their associated memory so that we can
//		rebuild them, if required, during our applications life-time.
//-----------------------------------------------------------------------------
void CGameApp::ReleaseObjects( )
{
	if(player != NULL) {
		delete player;
		player = NULL;
	}
	if (humans != NULL) {
		delete humans;
		humans = NULL;
	}
	if (cars != NULL) {
		delete cars;
		cars = NULL;
	}
	if (policeCars != NULL) {
		delete policeCars;
		policeCars = NULL;
	}
	if (miniMap != NULL) {
		delete miniMap;
		miniMap = NULL;
	}
	if (hud != NULL) {
		delete hud;
		hud = NULL;
	}
	if(m_pBBuffer != NULL)
	{
		delete m_pBBuffer;
		m_pBBuffer = NULL;
	}
}

//-----------------------------------------------------------------------------
// Name : FrameAdvance () (Private)
// Desc : Called to signal that we are now rendering the next frame.
//-----------------------------------------------------------------------------
int CGameApp::FrameAdvance()
{
	static TCHAR FrameRate[ 50 ];
	static TCHAR TitleBuffer[ 255 ];

	// Advance the timer
	m_Timer.Tick( );

	// Skip if app is inactive
	if ( !m_bActive ) return IDCONTINUEGAME;
	
	// Get / Display the framerate
	if ( m_LastFrameRate != m_Timer.GetFrameRate() )
	{
		m_LastFrameRate = m_Timer.GetFrameRate( FrameRate, 50 );
		sprintf_s( TitleBuffer, _T("Game : %s"), FrameRate );
		SetWindowText( m_hWnd, TitleBuffer );

	} // End if Frame Rate Altered

	// Poll & Process input devices
	ProcessInput();

	// Animate the game objects
	AnimateObjects();
	CheckCollisions();
	// Drawing the game objects
	DrawObjects();
	srand(time(NULL));
	
		if(player->isGameOver()) {
			string output_message = "Your score is : " + to_string(player->getScore()) + "\nDo you want to play again?";

			int option = MessageBox(m_hWnd, _T(output_message.c_str()), _T("Game Over"), MB_YESNO | MB_ICONQUESTION);
			if (option == IDNO) {
				ShutDown();
			}
			return option;
			//ShutDown();
		}
	//}

	return IDCONTINUEGAME;
}

//-----------------------------------------------------------------------------
// Name : ProcessInput () (Private)
// Desc : Simply polls the input devices and performs basic input operations
//-----------------------------------------------------------------------------
void CGameApp::ProcessInput()
{
	static UCHAR pKeyBuffer[256];
	ULONG		Direction[2] = { 0, 0 };
	POINT		CursorPos;
	float		X = 0.0f, Y = 0.0f;

	// Retrieve keyboard state
	if (!GetKeyboardState(pKeyBuffer)) return;

	// Check the relevant keys
	if (pKeyBuffer[0x57] & 0xF0) Direction[0] |= CPlayer::DIR_FORWARD;
	if (pKeyBuffer[0x53] & 0xF0) Direction[0] |= CPlayer::DIR_BACKWARD;
	if (pKeyBuffer[0x41] & 0xF0) Direction[0] |= CPlayer::DIR_LEFT;
	if (pKeyBuffer[0x44] & 0xF0) Direction[0] |= CPlayer::DIR_RIGHT;


	// Move the player
	player->Move(Direction[0]);

	// Now process the mouse (if the button is pressed)
	if ( GetCapture() == m_hWnd )
	{
		// Hide the mouse pointer
		SetCursor( NULL );

		// Retrieve the cursor position
		GetCursorPos( &CursorPos );

		// Reset our cursor position so we can keep going forever :)
		SetCursorPos( m_OldCursorPos.x, m_OldCursorPos.y );

	} // End if Captured
}

//-----------------------------------------------------------------------------
// Name : AnimateObjects () (Private)
// Desc : Animates the objects we currently have loaded.
//-----------------------------------------------------------------------------
void CGameApp::AnimateObjects()
{
	player->Update(m_Timer.GetTimeElapsed());
	humans->Update(m_Timer.GetTimeElapsed());
	cars->Update(m_Timer.GetTimeElapsed());
	policeCars->Update(m_Timer.GetTimeElapsed());
	hud->Update();
	miniMap->Update(player->AbsoluteCenterPosition());

}

void CGameApp::CheckCollisions() {
	
	player->isCollisionWithNature();
	collisionManager->checkCollision();
}

//-----------------------------------------------------------------------------
// Name : DrawObjects () (Private)
// Desc : Draws the game objects
//-----------------------------------------------------------------------------
void CGameApp::DrawObjects()
{
	m_pBBuffer->reset();
	/*if (-scrolling_backgroung >= m_imgBackground->Height() - m_pBBuffer->height())
		scrolling_backgroung = 0;
	scrolling_backgroung -= SCROLLING_SPEED;*/

	//m_imgBackground->Paint(m_pBBuffer->getDC(), 0, scrolling_backgroung);
	player->Draw();
	humans->Draw();
	cars->Draw();
	policeCars->Draw();
	miniMap->Draw();
	hud->Draw();
	m_pBBuffer->present();
}


int CGameApp::RestartGame(LPCTSTR lpCmdLine, int iCmdShow) {

	ReleaseObjects();
	if (!BuildObjects()) return 0;
	SetupGameState();
	int retCode = BeginGame();
	
	return retCode;
}

void CGameApp::Save() {
	ofstream file;
	file.open(save_file);

	file << players->toString();

	file << humans->toString();
	
	file << cars->toString();
	
	file << policeCars->toString();
	
	file.close();
}


void CGameApp::Load() {

	//ReleaseObjects();
	int no_enemies;
	int no_players;


	ifstream file;
	file.open(save_file);

	//if (!BuildObjects()) return;
	file >> players;
	
	file >> humans;
	
	file >> cars;
	
	file >> policeCars;
	
	player = players->get(0);
	player->setPoliceCars(policeCars);
	hud->setPlayer(player);

	file.close();
}
