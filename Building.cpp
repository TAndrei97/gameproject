﻿#include "Building.h"

Building::Building(const BackBuffer* pBackBuffer, const char* fileName, CImageFile* imgBackground) : ICollidable() {
	this->imgBackground = imgBackground;
	backBuffer = pBackBuffer;

	m_pSprite = new Sprite(fileName, BACKGROUND_TRANSPARENT_COLOR);
	m_pSprite->setBackBuffer(backBuffer);
	m_pSprite->setBackgroundImage(imgBackground);
}

Building::~Building() {
	delete m_pSprite;
}

void Building::Draw() {
	m_pSprite->drawByAbsolutePosition();
}

Vec2& Building::Position() {
	return m_pSprite->mPosition;
}

Vec2& Building::absoluteCenterPosition() {
	return m_pSprite->mAbsoluteCenterPosition;
}

bool Building::isCollision(ICollidable* other) {
	return other->isCollision(m_pSprite);
}

bool Building::isCollision(Sprite* otherSprite) {
	//isHit();
	return m_pSprite->isCollisionWith(otherSprite);
}



void Building::isInDangerZone() {
}

void Building::goLastPostion() {
}

void Building::isCollisionWithNature() {
}
