﻿#pragma once

#include "Main.h"
#include "Sprite.h"
#include <iostream>
#include <vector>
#include "ImageFile.h"
#include "ICollidable.h"

class Building : ICollidable{
public:
	Building(const BackBuffer* pBackBuffer, const char* fileName, CImageFile* imgBackground);
	virtual ~Building();

	void					Draw();

	Vec2&					Position();
	Vec2&					absoluteCenterPosition();

	bool isCollision(ICollidable* other) override;
	bool isCollision(Sprite* otherSprite) override;
	
	void isInDangerZone() override;
	void goLastPostion() override;
	void isCollisionWithNature() override;
	void isHit(ICollidable* other) override{}
	void increaseMoney(int money) override{}
private:
	Sprite*	m_pSprite;
	COLORREF				BACKGROUND_TRANSPARENT_COLOR = RGB(255, 0, 255);
	CImageFile *imgBackground;
	const BackBuffer* backBuffer;
};
